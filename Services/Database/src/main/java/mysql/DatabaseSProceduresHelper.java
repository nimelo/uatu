package mysql;

import java.sql.*;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class DatabaseSProceduresHelper {
    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    public static final String MYSQL_URL = "jdbc:mysql://localhost/logindatabase?noAccessToProcedureBodies=true&"
            + "user=db_client&password=db_password2&";

    public static void invoke(String sql) {

        try {
            Class.forName(MYSQL_DRIVER);

        Connection connection = DriverManager.getConnection(MYSQL_URL);
        CallableStatement callableStatement = connection.prepareCall(sql);
        callableStatement.setString(1, "test");
            callableStatement.setString(2, "test");
            callableStatement.registerOutParameter(3, Types.BOOLEAN);


        ResultSet resultSet = callableStatement.executeQuery();

            String string = callableStatement.getString(3);
            boolean out_exist = resultSet.getBoolean("OUT_EXIST");
        System.out.println(out_exist);

        resultSet.close();
        callableStatement.close();
        connection.close();}
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
