package ServiceStaticVariables;

import Database.DatabaseConfiguration;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class Variables {
    private static String loginServiceAddress;
    private static String loginServicePort;
    private static String loginServicePath;
    private static DatabaseConfiguration statisticsDatabaseConfiguration;

    public static DatabaseConfiguration getHistoryDatabaseConfiguration() {
        return historyDatabaseConfiguration;
    }

    public static void setHistoryDatabaseConfiguration(DatabaseConfiguration historyDatabaseConfiguration) {
        Variables.historyDatabaseConfiguration = historyDatabaseConfiguration;
    }

    private static DatabaseConfiguration historyDatabaseConfiguration;

    public static DatabaseConfiguration getStatisticsDatabaseConfiguration() {
        return statisticsDatabaseConfiguration;
    }

    public static void setStatisticsDatabaseConfiguration(DatabaseConfiguration statisticsDatabaseConfiguration) {
        Variables.statisticsDatabaseConfiguration = statisticsDatabaseConfiguration;
    }

    public String getLoginServiceAddress() {
        return loginServiceAddress;
    }

    public void setLoginServiceAddress(String loginServiceAddress) {
        loginServiceAddress = loginServiceAddress;
    }

    public String getLoginServicePath() {
        return loginServicePath;
    }

    public void setLoginServicePath(String loginServicePath) {
        loginServicePath = loginServicePath;
    }

    public String getLoginServicePort() {
        return loginServicePort;
    }

    public void setLoginServicePort(String loginServicePort) {
        loginServicePort = loginServicePort;
    }
}
