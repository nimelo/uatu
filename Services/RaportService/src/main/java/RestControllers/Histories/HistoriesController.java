package RestControllers.Histories;

import LoginCheckers.LoginChecker;
import LoginCheckers.LoginResponse;
import Query.QueryManager;
import RestControllers.Histories.Context.HistoriesContext;
import RestControllers.Histories.Context.HistoriesHardwareContext;
import RestControllers.Histories.Results.HistoriesHardwareResponse;
import RestControllers.Histories.Results.HistoriesResponse;
import RestControllers.Statistics.BaseRequest;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
@EnableAutoConfiguration
@RequestMapping("/histories")
public class HistoriesController {

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public HistoriesResponse processSearch(@RequestBody HistoriesContext ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) (ctx));
        if (response.getSuccess()) {
            return QueryManager.getInstance().getHistoriesFor(response.getId(), ctx.getFrom(), ctx.getTo(), ctx.getPrecision());
        }

        return HistoriesResponse.FailResult("");
    }

    @RequestMapping(value = "/hardwares", method = RequestMethod.POST)
    public HistoriesHardwareResponse processHardwares(@RequestBody HistoriesHardwareContext ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) (ctx));
        if (response.getSuccess()) {
            return QueryManager.getInstance().getHardwareHistoriesFor(response.getId(), ctx.getFrom(), ctx.getTime());
        }

        return HistoriesHardwareResponse.FailResult("");
    }
}