package RestControllers.Histories.Context;

import RestControllers.Statistics.BaseRequest;

/**
 * Created by mrnim on 28-Jul-16.
 */
public class HistoriesHardwareContext extends BaseRequest{
    private Long from;
    private Long time;

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
