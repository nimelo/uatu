package RestControllers.Histories.Context;

import RestControllers.Histories.Data.HistoryDate;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.BaseRequest;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoriesContext extends BaseRequest {
    private HistoryDate from;
    private HistoryDate to;
    private Integer precision;

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public HistoryDate getFrom() {
        return from;
    }

    public void setFrom(HistoryDate from) {
        this.from = from;
    }

    public HistoryDate getTo() {
        return to;
    }

    public void setTo(HistoryDate to) {
        this.to = to;
    }
}
