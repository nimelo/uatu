package RestControllers.Histories.Results;

import RestControllers.Histories.Data.HistoryHardwareSeries;

import java.util.ArrayList;

/**
 * Created by mrnim on 28-Jul-16.
 */
public class HistoriesHardwareResponse {
    private Boolean success;
    private String errorMessage;
    private ArrayList<HistoryHardwareSeries> series;

    public ArrayList<HistoryHardwareSeries> getSeries() {
        return series;
    }

    public void setSeries(ArrayList<HistoryHardwareSeries> series) {
        this.series = series;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static HistoriesHardwareResponse FailResult(String s) {
        HistoriesHardwareResponse historiesHardwareResponse = new HistoriesHardwareResponse();
        historiesHardwareResponse.setErrorMessage(s);
        historiesHardwareResponse.setSuccess(false);
        return historiesHardwareResponse;
    }
}
