package RestControllers.Histories.Results;

import RestControllers.Histories.Data.HistoryYear;

import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoriesResponse {
    private Boolean success;
    private String errorMessage;
    private ArrayList<HistoryYear> years;

    public ArrayList<HistoryYear> getYears() {
        return years;
    }

    public void setYears(ArrayList<HistoryYear> years) {
        this.years = years;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static HistoriesResponse FailResult(String s) {
        HistoriesResponse historiesResponse = new HistoriesResponse();
        historiesResponse.setSuccess(false);
        historiesResponse.setErrorMessage(s);
        return historiesResponse;
    }
}
