package RestControllers.Histories.Data;

import RestControllers.Raporting.Data.Common.YearMonthDay;

import java.util.Calendar;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryDate {
    private YearMonthDay date;
    private HoursMinutes time;

    public HoursMinutes getTime() {
        return time;
    }

    public void setTime(HoursMinutes time) {
        this.time = time;
    }

    public YearMonthDay getDate() {
        return date;
    }

    public void setDate(YearMonthDay date) {
        this.date = date;
    }

    public Calendar toCalendar(){
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.DATE, this.getDate().getDay());
        instance.set(Calendar.MONTH, this.getDate().getMonth() - 1);
        instance.set(Calendar.YEAR, this.getDate().getYear());

        instance.set(Calendar.HOUR, this.getTime().getHours());
        instance.set(Calendar.MINUTE, this.getTime().getMinutes());

        return instance;
    }
}
