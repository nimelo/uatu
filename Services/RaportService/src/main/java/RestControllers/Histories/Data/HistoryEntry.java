package RestControllers.Histories.Data;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryEntry {
    private String name;
    private Long from;
    private Long time;
    private Long to;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }
}
