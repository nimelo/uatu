package RestControllers.Histories.Data;

import java.util.ArrayList;

/**
 * Created by mrnim on 28-Jul-16.
 */
public class HistoryHardwareSeries {
    private String name;
    private Long pointInterval;
    private Long pointStart;
    private ArrayList<Double> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPointInterval() {
        return pointInterval;
    }

    public void setPointInterval(Long pointInterval) {
        this.pointInterval = pointInterval;
    }

    public Long getPointStart() {
        return pointStart;
    }

    public void setPointStart(Long pointStart) {
        this.pointStart = pointStart;
    }

    public ArrayList<Double> getData() {
        return data;
    }

    public void setData(ArrayList<Double> data) {
        this.data = data;
    }
}
