package RestControllers.Histories.Data;

import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryDay {
    private String name;
    private ArrayList<HistoryEntry> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<HistoryEntry> getData() {
        return data;
    }

    public void setData(ArrayList<HistoryEntry> data) {
        this.data = data;
    }
}
