package RestControllers.Histories.Data;

import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryYear {
    private String name;
    private ArrayList<HistoryMonth> months;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<HistoryMonth> getMonths() {
        return months;
    }

    public void setMonths(ArrayList<HistoryMonth> months) {
        this.months = months;
    }
}
