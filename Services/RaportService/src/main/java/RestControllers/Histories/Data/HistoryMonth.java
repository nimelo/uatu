package RestControllers.Histories.Data;

import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryMonth {
    private String name;
    private ArrayList<HistoryDay> days;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<HistoryDay> getDays() {
        return days;
    }

    public void setDays(ArrayList<HistoryDay> days) {
        this.days = days;
    }
}
