package RestControllers.Histories.Data;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HoursMinutes {
    private Integer hours;
    private Integer minutes;

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }
}
