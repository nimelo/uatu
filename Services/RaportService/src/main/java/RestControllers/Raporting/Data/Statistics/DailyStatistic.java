package RestControllers.Raporting.Data.Statistics;

import RestControllers.Raporting.Data.Common.YearMonthDay;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class DailyStatistic {
    @JsonProperty("ApplicationStatistics")
    private HashMap<String, Long> applicationStatistics;

    @JsonProperty("WebsidesStatistics")
    private HashMap<String, Long> websiedesStatistics;

    @JsonProperty("Time")
    private YearMonthDay time;

    public HashMap<String, Long> getApplicationStatistics() {
        return applicationStatistics;
    }

    public void setApplicationStatistics(HashMap<String, Long> applicationStatistics) {
        this.applicationStatistics = applicationStatistics;
    }

    public YearMonthDay getTime() {
        return time;
    }

    public void setTime(YearMonthDay time) {
        this.time = time;
    }

    public HashMap<String, Long> getWebsiedesStatistics() {
        return websiedesStatistics;
    }

    public void setWebsiedesStatistics(HashMap<String, Long> websiedesStatistics) {
        this.websiedesStatistics = websiedesStatistics;
    }
}
