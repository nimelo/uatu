package RestControllers.Raporting.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mrnim on 15-Jul-16.
 */
public class ExampleData2 {
    private String name;

    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }
}
