package RestControllers.Raporting.Data.Common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mrnim on 15-Jul-16.
 */
public class YearMonthDay {
    @JsonProperty("Year")
    private Integer year;

    @JsonProperty("Month")
    private Integer month;

    @JsonProperty("Day")
    private Integer day;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public YearMonthDay(Integer day, Integer month, Integer year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public YearMonthDay() {
    }
}
