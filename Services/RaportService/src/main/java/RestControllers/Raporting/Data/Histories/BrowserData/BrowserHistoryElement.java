package RestControllers.Raporting.Data.Histories.BrowserData;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mrnim on 15-Jul-16.
 */
public class BrowserHistoryElement {
    @JsonProperty("Url")
    private String value;

    @JsonProperty("Time")
    private Long time;

    @JsonProperty("From")
    private Long from;

    public long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
