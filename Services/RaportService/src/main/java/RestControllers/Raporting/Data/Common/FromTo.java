package RestControllers.Raporting.Data.Common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mrnim on 15-Jul-16.
 */
public class FromTo {
    @JsonProperty("From")
    private Long from;

    @JsonProperty("Time")
    private Long time;

    public long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
