package RestControllers.Raporting.Data.Histories.HardwareData;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mrnim on 15-Jul-16.
 */
public class HardwareData {
    @JsonProperty("Values")
    private HashMap<String, ArrayList<Double>> values;

    @JsonProperty("QueryFrequency")
    private Long queryFrequency;

    @JsonProperty("From")
    private Long from;

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getQueryFrequency() {
        return queryFrequency;
    }

    public void setQueryFrequency(Long queryFrequency) {
        this.queryFrequency = queryFrequency;
    }

    public HashMap<String, ArrayList<Double>> getValues() {
        return values;
    }

    public void setValues(HashMap<String, ArrayList<Double>> values) {
        this.values = values;
    }
}
