package RestControllers.Raporting.Data.Histories;

import RestControllers.Raporting.Data.Common.FromTo;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class BrowserHistory {
    @JsonProperty("History")
    private ArrayList<FromTo> history;

    public ArrayList<FromTo> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<FromTo> history) {
        this.history = history;
    }
}
