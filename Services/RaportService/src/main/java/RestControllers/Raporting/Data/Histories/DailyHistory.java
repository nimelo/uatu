package RestControllers.Raporting.Data.Histories;

import RestControllers.Raporting.Data.Histories.ApplicationData.ApplicationHistory;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Raporting.Data.Histories.HardwareData.HardwareData;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class DailyHistory {
    @JsonProperty("ApplicationsHistory")
    private HashMap<String, ApplicationHistory> applicationHistory;

    @JsonProperty("WebsidesHistory")
    private HashMap<String, BrowserHistory> websidesHistory;

    @JsonProperty("HardwaresHistory")
    private HashMap<String, HardwareData> hardwaresData;

    @JsonProperty("Time")
    private YearMonthDay time;

    public HashMap<String, ApplicationHistory> getApplicationHistory() {
        return applicationHistory;
    }

    public void setApplicationHistory(HashMap<String, ApplicationHistory> applicationHistory) {
        this.applicationHistory = applicationHistory;
    }

    public HashMap<String, HardwareData> getHardwaresData() {
        return hardwaresData;
    }

    public void setHardwaresData(HashMap<String, HardwareData> hardwaresData) {
        this.hardwaresData = hardwaresData;
    }

    public YearMonthDay getTime() {
        return time;
    }

    public void setTime(YearMonthDay time) {
        this.time = time;
    }

    public HashMap<String, BrowserHistory> getWebsidesHistory() {
        return websidesHistory;
    }

    public void setWebsidesHistory(HashMap<String, BrowserHistory> websidesHistory) {
        this.websidesHistory = websidesHistory;
    }
}
