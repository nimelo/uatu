package RestControllers.Raporting.Data.Histories.ApplicationData;

import RestControllers.Raporting.Data.Common.FromTo;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by mrnim on 15-Jul-16.
 */
public class ApplicationHistory {
    @JsonProperty("History")
    private ArrayList<FromTo> history;

    public ArrayList<FromTo> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<FromTo> history) {
        this.history = history;
    }
}
