package RestControllers.Raporting;


import LoginCheckers.LoginResponse;
import Query.QueryManager;
import LoginCheckers.LoginChecker;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Raporting.Context.HistoryContext;
import RestControllers.Raporting.Context.StatisticsContext;
import RestControllers.Raporting.Responses.RaportResponse;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mrnim on 15-Jul-16.
 */
@RestController
@Controller
@EnableAutoConfiguration
@RequestMapping("/raport")
public class RaportController {

    @RequestMapping(path="/statistics", method = RequestMethod.POST)
    public RaportResponse processStatistics(@RequestBody StatisticsContext ctx){
        LoginResponse response = LoginChecker.getInstance().getId(ctx.getCredentials());
        if(response.getSuccess()){
            if(QueryManager.getInstance().handleStatistics(ctx.getStatistics(), response.getId())){
                return RaportResponse.SuccessResult();
            }
        }
        return RaportResponse.FailResult("");
    }

    @RequestMapping(path="/histories", method = RequestMethod.POST)
    public RaportResponse processHistories(@RequestBody HistoryContext ctx){
        LoginResponse response = LoginChecker.getInstance().getId(ctx.getCredentials());
        if(response.getSuccess()){
            if(QueryManager.getInstance().handleHistory(ctx.getHistories(), response.getId())){
                return RaportResponse.SuccessResult();
            }
        }
        return RaportResponse.FailResult("");
    }

    @RequestMapping(path="/test", method = RequestMethod.POST)
    public YearMonthDay processRaport2(@RequestBody YearMonthDay param){
        return param;
    }
}
