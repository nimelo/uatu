package RestControllers.Raporting.Context;

import RestControllers.Raporting.Data.Credentials.Credentials;
import RestControllers.Raporting.Data.Statistics.DailyStatistic;

import java.util.ArrayList;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class StatisticsContext {
    private Credentials credentials;
    private ArrayList<DailyStatistic> statistics;

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public ArrayList<DailyStatistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(ArrayList<DailyStatistic> statistics) {
        this.statistics = statistics;
    }
}
