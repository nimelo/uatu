package RestControllers.Raporting.Context;

import RestControllers.Raporting.Data.Credentials.Credentials;
import RestControllers.Raporting.Data.Histories.DailyHistory;

import java.util.ArrayList;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class HistoryContext {
    private Credentials credentials;
    private ArrayList<DailyHistory> histories;

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public ArrayList<DailyHistory> getHistories() {
        return histories;
    }

    public void setHistories(ArrayList<DailyHistory> histories) {
        this.histories = histories;
    }
}
