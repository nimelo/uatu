package RestControllers.Raporting.Responses;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class RaportResponse {
    private Boolean isSuccess;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public static RaportResponse FailResult(String msg){
        RaportResponse raportResponse = new RaportResponse();
        raportResponse.setErrorMessage(msg);
        raportResponse.setSuccess(false);

        return raportResponse;
    }

    public static RaportResponse SuccessResult(){
        RaportResponse raportResponse = new RaportResponse();
        raportResponse.setSuccess(true);
        return  raportResponse;
    }

}
