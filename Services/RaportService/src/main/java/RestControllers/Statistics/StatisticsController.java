package RestControllers.Statistics;

import LoginCheckers.LoginResponse;
import Query.QueryManager;
import LoginCheckers.LoginChecker;
import RestControllers.Statistics.Context.StatisticsContext;
import RestControllers.Statistics.Context.StatisticsDailyContext;
import RestControllers.Statistics.Context.StatisticsGroupContext;
import RestControllers.Statistics.Results.StatisticsGroupResponse;
import RestControllers.Statistics.Results.StatisticsResponse;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mrnim on 15-Jul-16.
 */
@RestController
@Controller
@EnableAutoConfiguration
@RequestMapping("/statistics")
public class StatisticsController {

    @RequestMapping(value="/get", method = RequestMethod.POST)
    public StatisticsResponse processStatistics(@RequestBody StatisticsContext ctx){
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) (ctx));
        if(response.getSuccess()){
            return QueryManager.getInstance().getStatisticsFor(response.getId(), ctx.getElements(), ctx.getFrom(), ctx.getRange());
        }

        return StatisticsResponse.FailResult("");
    }

    @RequestMapping(value="/group", method = RequestMethod.POST)
    public StatisticsGroupResponse processGroupStatistics(@RequestBody StatisticsGroupContext ctx){
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) (ctx));
        if(response.getSuccess()){
            return QueryManager.getInstance().getStatisticsGroupFor(response.getId(), ctx.getElements(), ctx.getFrom(), ctx.getRange(), ctx.getGroupBy());
        }

        return StatisticsGroupResponse.FailResult("");
    }

    @RequestMapping(value="/daily", method = RequestMethod.POST)
    public StatisticsResponse processDailyStatistics(@RequestBody StatisticsDailyContext ctx){
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) (ctx));
        if(response.getSuccess()){
            return QueryManager.getInstance().getStatisticsDailyFor(response.getId(), ctx.getDay(), ctx.getApp());
        }

        return StatisticsResponse.FailResult("");
    }
}
