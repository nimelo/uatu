package RestControllers.Statistics.Results;

import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class StatisticsResponse {
    private Boolean success;
    private String errorMessage;
    private ArrayList<StatisticCategoryResult> data;

    public ArrayList<StatisticCategoryResult> getData() {
        return data;
    }

    public void setData(ArrayList<StatisticCategoryResult> data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static StatisticsResponse FailResult(String s) {
        StatisticsResponse statisticsResponse = new StatisticsResponse();
        statisticsResponse.setSuccess(false);
        statisticsResponse.setErrorMessage(s);
        return statisticsResponse;
    }
}
