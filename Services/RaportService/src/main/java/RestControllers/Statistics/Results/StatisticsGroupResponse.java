package RestControllers.Statistics.Results;

import java.util.ArrayList;

/**
 * Created by mrnim on 26-Jul-16.
 */
public class StatisticsGroupResponse {
    private Boolean success;
    private String errorMessage;
    private ArrayList<StatisticsDailyResult> data;
    private ArrayList<String> fields;

    public ArrayList<String> getFields() {
        return fields;
    }

    public void setFields(ArrayList<String> fields) {
        this.fields = fields;
    }

    public ArrayList<StatisticsDailyResult> getData() {
        return data;
    }

    public void setData(ArrayList<StatisticsDailyResult> data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public static StatisticsGroupResponse FailResult(String s) {
        StatisticsGroupResponse statisticsGroupResponse = new StatisticsGroupResponse();
        statisticsGroupResponse.setSuccess(false);
        statisticsGroupResponse.setErrorMessage(s);
        return statisticsGroupResponse;
    }
}
