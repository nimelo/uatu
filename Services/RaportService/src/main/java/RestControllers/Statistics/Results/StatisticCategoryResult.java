package RestControllers.Statistics.Results;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class StatisticCategoryResult {
    private String name;
    private Long y;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getY() {
        return y;
    }

    public void setY(Long y) {
        this.y = y;
    }

    public StatisticCategoryResult(String name, Long y) {
        this.name = name;
        this.y = y;
    }

    public StatisticCategoryResult() {
    }
}
