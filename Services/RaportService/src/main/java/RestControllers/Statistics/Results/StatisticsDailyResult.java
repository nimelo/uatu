package RestControllers.Statistics.Results;

import java.util.ArrayList;

/**
 * Created by mrnim on 26-Jul-16.
 */
public class StatisticsDailyResult {
    private String name;
    private ArrayList<Long> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Long> getData() {
        return data;
    }

    public void setData(ArrayList<Long> data) {
        this.data = data;
    }
}
