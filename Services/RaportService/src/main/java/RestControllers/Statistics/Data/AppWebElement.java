package RestControllers.Statistics.Data;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class AppWebElement {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isApp(){
        return this.getType().equals(APP_TYPE);
    }

    public boolean isWeb(){
        return this.getType().equals(WEB_TYPE);
    }

    public static final String APP_TYPE = "App";
    public static final String WEB_TYPE = "Web";
}
