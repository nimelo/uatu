package RestControllers.Statistics.Context;

import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.BaseRequest;
import RestControllers.Statistics.Data.AppWebElement;

import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class StatisticsContext extends BaseRequest{
    private Integer range;

    public YearMonthDay getFrom() {
        return from;
    }

    public void setFrom(YearMonthDay from) {
        this.from = from;
    }

    public Integer getRange() {
        return range;
    }

    public void setRange(Integer range) {
        this.range = range;
    }

    private YearMonthDay from;
    private ArrayList<AppWebElement> elements;

    public ArrayList<AppWebElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<AppWebElement> elements) {
        this.elements = elements;
    }
}
