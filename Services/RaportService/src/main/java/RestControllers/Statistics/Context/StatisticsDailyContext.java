package RestControllers.Statistics.Context;

import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.BaseRequest;

/**
 * Created by mrnim on 29-Jul-16.
 */
public class StatisticsDailyContext extends BaseRequest{
    private YearMonthDay day;
    private Boolean app;

    public YearMonthDay getDay() {
        return day;
    }

    public void setDay(YearMonthDay day) {
        this.day = day;
    }

    public Boolean getApp() {
        return app;
    }

    public void setApp(Boolean app) {
        this.app = app;
    }
}
