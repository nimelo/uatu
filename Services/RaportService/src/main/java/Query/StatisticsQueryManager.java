package Query;

import Database.DatabaseManager;
import Database.Objects.SimplyDatabaseResult;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Raporting.Data.Statistics.DailyStatistic;
import ServiceStaticVariables.Variables;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class StatisticsQueryManager {
    public static boolean handle(ArrayList<DailyStatistic> statistics, Integer lgnId) {
        if(statistics != null){
            return internalHandle(statistics, lgnId);
        }
        return false;
    }

    private static boolean internalHandle(ArrayList<DailyStatistic> statistics, Integer lgnId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getStatisticsDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            for(DailyStatistic statistic : statistics){
                handleStatistic(connection, statistic, lgnId);
            }
            connection.commit();
            return true;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return false;
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    private static void handleStatistic(Connection connection, DailyStatistic statistic, Integer lgnId) throws Exception{
        Integer ymdId = DatabaseManager.getInstance().saveYearMonthDay(connection, statistic.getTime()).getId();
        handleApplicationStatistic(connection, statistic.getApplicationStatistics(), ymdId, lgnId);
        handleWebsideStatistic(connection, statistic.getWebsiedesStatistics(), ymdId, lgnId);
    }

    private static void handleWebsideStatistic(Connection connection, HashMap<String, Long> websiedesStatistics, Integer ymdId, Integer lgnId) throws Exception {
        for (HashMap.Entry<String, Long> entry : websiedesStatistics.entrySet()){
            Integer id = DatabaseManager.getInstance().saveDomain(connection, entry.getKey());
            DatabaseManager.getInstance().saveWebsideStatistic(connection, entry.getValue(), id, ymdId, lgnId);
        }
    }

    private static void handleApplicationStatistic(Connection connection, HashMap<String, Long> applicationStatistics, Integer ymdId, Integer lgnId) throws Exception {
        for (HashMap.Entry<String, Long> entry : applicationStatistics.entrySet()){
            Integer id = DatabaseManager.getInstance().saveProgram(connection, entry.getKey());
            DatabaseManager.getInstance().saveApplicationStatistic(connection, entry.getValue(), id, ymdId, lgnId);
        }
    }

}
