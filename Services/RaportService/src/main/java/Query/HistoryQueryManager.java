package Query;

import Database.DatabaseManager;
import Database.Objects.HistoryDAO;
import RestControllers.Histories.Data.*;
import RestControllers.Histories.Results.HistoriesResponse;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.Results.StatisticsResponse;
import ServiceStaticVariables.Variables;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryQueryManager {
    public static HistoriesResponse getHistoriesFor(Integer id, HistoryDate from, HistoryDate to, Integer precision) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getHistoryDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);

            ArrayList<HistoryYear> data = internalGetHistoriesFor(connection, id, from, to, precision);
            connection.commit();
            HistoriesResponse result = new HistoriesResponse();
            result.setSuccess(true);
            result.setYears(data);
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return HistoriesResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    private static ArrayList<HistoryYear> internalGetHistoriesFor(Connection connection, Integer id, HistoryDate from, HistoryDate to, Integer precision) throws Exception {
        ArrayList<HistoryYear> yearList = new ArrayList<HistoryYear>();
        Integer currentYear = from.getDate().getYear();
        Integer yearTo = to.getDate().getYear();

        while(currentYear <= yearTo){
            HistoryYear year = internalGetYearHistoryFor(connection, id, from, to, precision, currentYear);
            yearList.add(year);
            currentYear += 1;
        }

        return yearList;
    }

    private static HistoryYear internalGetYearHistoryFor(Connection connection, Integer id, HistoryDate from, HistoryDate to, Integer precision, Integer currentYear) throws Exception {
        HistoryYear historyYear = new HistoryYear();
        ArrayList<HistoryMonth> historyMonths = new ArrayList<HistoryMonth>();
        Integer currentMonth = getCurrentMonthFrom(from.getDate(), to.getDate(), currentYear);
        Integer monthTo = getCurrentMonthTo(from.getDate(), to.getDate(), currentYear);

        while(currentMonth <= monthTo){
            HistoryMonth month = internalGetMonthHistoryFor(connection, id, from, to, precision, currentYear, currentMonth);
            historyMonths.add(month);
            currentMonth += 1;
        }

        historyYear.setName(currentYear.toString());
        historyYear.setMonths(historyMonths);
        return historyYear;
    }

    private static Integer getCurrentMonthTo(YearMonthDay from, YearMonthDay to, Integer currentYear) {
        if(currentYear.equals(to.getYear())){
            return to.getMonth();
        }else if(currentYear.equals(from.getYear())){
            return 12;
        }else if(currentYear > from.getYear()){
            if(currentYear < to.getYear()){
                return 12;
            }else if(currentYear.equals(to.getYear())){
                return to.getMonth();
            }
        }
        return null;
    }

    private static Integer getCurrentMonthFrom(YearMonthDay from, YearMonthDay to, Integer currentYear) {
        if(currentYear.equals(from.getYear())){
            return from.getMonth();
        }else if(currentYear > from.getYear()){
            return 1;
        }
        return null;
    }

    private static HistoryMonth internalGetMonthHistoryFor(Connection connection, Integer id, HistoryDate from, HistoryDate to, Integer precision, Integer currentYear, Integer currentMonth) throws Exception {
        HistoryMonth historyMonth = new HistoryMonth();
        ArrayList<HistoryDay> historyDays = new ArrayList<HistoryDay>();
        Integer currentDay = getCurrentDayFrom(from.getDate(), to.getDate(), currentYear, currentMonth);
        Integer dayTo = getCurrentDayTo(from.getDate(), to.getDate(), currentYear, currentMonth);

        while(currentDay <= dayTo){
            HistoryDay day = internalGetDayHistoryFor(connection, id, from, to, precision, currentYear, currentMonth, currentDay);
            historyDays.add(day);
            currentDay += 1;
        }

        historyMonth.setName(getMonthName(currentMonth - 1));
        historyMonth.setDays(historyDays);

        return historyMonth;
    }

    private static Integer getCurrentDayFrom(YearMonthDay from, YearMonthDay to, Integer currentYear, Integer currentMonth) {
        if(currentYear.equals(from.getYear())){
            if(currentMonth.equals(from.getMonth())){
                return from.getDay();
            }else if(!currentMonth.equals(from.getMonth())){
                return 1;
            }
        }else if(currentYear > from.getYear()){
            return 1;
        }

        return null;
    }

    private static Integer getCurrentDayTo(YearMonthDay from, YearMonthDay to, Integer currentYear, Integer currentMonth) {
        if(currentYear.equals(to.getYear())){
            if(currentMonth.equals(to.getMonth())){
                return to.getDay();
            }else if(currentMonth < to.getMonth()){
                return getMaxDaysInMonth(currentMonth, currentYear);
            }
        }else if(currentYear < to.getYear()){
            return getMaxDaysInMonth(currentMonth, currentYear);
        }

        return null;
    }

    private static Integer getMaxDaysInMonth(Integer currentMonth, Integer currentYear) {
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.DATE, 1);
        instance.set(Calendar.MONTH, currentMonth - 1);
        instance.set(Calendar.YEAR, currentYear);
        return instance.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    private static HistoryDay internalGetDayHistoryFor(Connection connection, Integer id, HistoryDate from, HistoryDate to, Integer precision, Integer currentYear, Integer currentMonth, Integer currentDay) throws Exception {
        HistoryDay historyDay = new HistoryDay();
        Long unixFrom = getUnixFrom(currentDay, currentMonth, currentYear, from);
        Long unixTo = getUnixTo(currentDay, currentMonth, currentYear, to);
        ArrayList<HistoryEntry> historyForDay = internalGetHistoryForDay(connection, id, unixFrom, unixTo, precision);

        historyDay.setName(currentDay.toString());
        historyDay.setData(historyForDay);
        return historyDay;
    }

    private static ArrayList<HistoryEntry> internalGetHistoryForDay(Connection connection, Integer id, Long unixFrom, Long unixTo, Integer precision) throws Exception {
        return HistoryDAO.internalGetHistoryForDay(connection, id, unixFrom, unixTo, precision);
    }

    private static Long getUnixTo(Integer currentDay, Integer currentMonth, Integer currentYear, HistoryDate historyDate) {
        YearMonthDay to = historyDate.getDate();
        if(currentYear.equals(to.getYear())){
            if(currentMonth.equals(to.getMonth())){
                if(currentDay.equals(to.getDay())){
                    return getUnixTimeFrom(currentDay, currentMonth, currentYear, historyDate.getTime().getHours(), historyDate.getTime().getMinutes());
                }
            }
        }

        return  getUnixTimeFrom(currentDay, currentMonth, currentYear, 23, 59);
    }

    private static Long getUnixFrom(Integer currentDay, Integer currentMonth, Integer currentYear, HistoryDate historyDate) {
        YearMonthDay fromDate = historyDate.getDate();
        if(currentYear.equals(fromDate.getYear())){
            if(currentMonth.equals(fromDate.getMonth())){
                if(currentDay.equals(fromDate.getDay())){
                    return getUnixTimeFrom(currentDay, currentMonth, currentYear, historyDate.getTime().getHours(), historyDate.getTime().getMinutes());
                }
            }
        }
        return getUnixTimeFrom(currentDay, currentMonth, currentYear, 0, 0);
    }

    private static Long getUnixTimeFrom(Integer currentDay, Integer currentMonth, Integer currentYear, Integer hours, Integer minutes) {
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.DATE, currentDay);
        instance.set(Calendar.MONTH, currentMonth - 1);
        instance.set(Calendar.YEAR, currentYear);
        instance.set(Calendar.HOUR_OF_DAY, hours);
        instance.set(Calendar.MINUTE, minutes);
        instance.set(Calendar.SECOND, 0);
        instance.set(Calendar.MILLISECOND, 0);
        return instance.getTimeInMillis();
    }


    private static String getMonthName(int zeroBasedIndex) {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[zeroBasedIndex];
    }


}
