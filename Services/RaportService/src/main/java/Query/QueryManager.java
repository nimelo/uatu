package Query;

import RestControllers.Histories.Data.HistoryDate;
import RestControllers.Histories.Results.HistoriesHardwareResponse;
import RestControllers.Histories.Results.HistoriesResponse;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Raporting.Data.Histories.DailyHistory;
import RestControllers.Raporting.Data.Statistics.DailyStatistic;
import RestControllers.Statistics.Data.AppWebElement;
import RestControllers.Statistics.Results.StatisticsGroupResponse;
import RestControllers.Statistics.Results.StatisticsResponse;

import java.util.ArrayList;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class QueryManager {
    public static QueryManager instance;
    public static QueryManager getInstance() {
        if(instance == null){
            instance = new QueryManager();
        }

        return instance;
    }

    public boolean handleStatistics(ArrayList<DailyStatistic> statistics, Integer lgnId) {
        return StatisticsQueryManager.handle(statistics, lgnId);
    }

    public boolean handleHistory(ArrayList<DailyHistory> histories, Integer lgnId) {
        return HistoriesQueryManager.handle(histories, lgnId);
    }

    public StatisticsResponse getStatisticsFor(Integer id, ArrayList<AppWebElement> elements, YearMonthDay from, Integer range) {
        return ChartsQueryManager.getStatisticdFor(id, elements, from , range);
    }

    public StatisticsGroupResponse getStatisticsGroupFor(Integer id, ArrayList<AppWebElement> elements, YearMonthDay from, Integer range, String groupBy) {
        return ChartsQueryManager.getStatisticsGroupFor(id, elements, from , range, groupBy);
    }

    public HistoriesResponse getHistoriesFor(Integer id, HistoryDate from, HistoryDate to, Integer precision) {
        return HistoryQueryManager.getHistoriesFor(id, from, to, precision);
    }

    public HistoriesHardwareResponse getHardwareHistoriesFor(Integer id, Long from, Long time) {
        return ChartsQueryManager.getHardwareHistoriesFor(id, from, time);
    }

    public StatisticsResponse getStatisticsDailyFor(Integer id, YearMonthDay day, Boolean app) {
        return ChartsQueryManager.getStatisticsDailyFor(id, day, app);
    }
}
