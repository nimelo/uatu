package Query;

import Database.DatabaseManager;
import RestControllers.Histories.Results.HistoriesHardwareResponse;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.Data.AppWebElement;
import RestControllers.Statistics.Results.StatisticCategoryResult;
import RestControllers.Statistics.Results.StatisticsGroupResponse;
import RestControllers.Statistics.Results.StatisticsResponse;
import ServiceStaticVariables.Variables;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class ChartsQueryManager {

    public static StatisticsResponse getStatisticdFor(Integer id, ArrayList<AppWebElement> elements, YearMonthDay from, Integer range) {
        ArrayList<String> appElements = getAppElements(elements);
        ArrayList<String> webElements = getWebElements(elements);

        return internalgetStatisticFor(id, appElements, webElements, from, range);
    }

    private static StatisticsResponse internalgetStatisticFor(Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getStatisticsDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);

            ArrayList<StatisticCategoryResult> data = DatabaseManager.getInstance().getStatisticFor(connection, id, appElements, webElements, from, range);
            connection.commit();
            StatisticsResponse result = new StatisticsResponse();
            result.setSuccess(true);
            result.setData(data);
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return StatisticsResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    private static ArrayList<String> getWebElements(ArrayList<AppWebElement> elements) {
        ArrayList<String> list = new ArrayList<String>();
        for (AppWebElement element : elements) {
            if(element.isWeb()){
                list.add(element.getName());
            }
        }
        return list;
    }

    private static ArrayList<String> getAppElements(ArrayList<AppWebElement> elements) {
        ArrayList<String> list = new ArrayList<String>();
        for (AppWebElement element : elements) {
            if(element.isApp()){
                list.add(element.getName());
            }
        }
        return list;
    }

    public static StatisticsGroupResponse getStatisticsGroupFor(Integer id, ArrayList<AppWebElement> elements, YearMonthDay from, Integer range, String groupBy) {
        ArrayList<String> appElements = getAppElements(elements);
        ArrayList<String> webElements = getWebElements(elements);

        return internalGetStatisticsGroupFor(id, appElements, webElements, from, range, groupBy);
    }

    private static StatisticsGroupResponse internalGetStatisticsGroupFor(Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range, String groupBy) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getStatisticsDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);

            StatisticsGroupResponse data = DatabaseManager.getInstance().getStatisticsGroupFor(connection, id, appElements, webElements, from, range, groupBy);
            connection.commit();
            data.setSuccess(true);

            return data;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return StatisticsGroupResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }

    }

    public static HistoriesHardwareResponse getHardwareHistoriesFor(Integer id, Long from, Long time) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getHistoryDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);

            HistoriesHardwareResponse data = DatabaseManager.getInstance().getHardwareHistoriesFor(connection, id, from, time);
            connection.commit();
            data.setSuccess(true);

            return data;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return HistoriesHardwareResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public static StatisticsResponse getStatisticsDailyFor(Integer id, YearMonthDay day, Boolean app) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getStatisticsDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);

            StatisticsResponse data = DatabaseManager.getInstance().getStatisticsDailyFor(connection, id, day, app);
            connection.commit();
            data.setSuccess(true);

            return data;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return StatisticsResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }
}
