package Query;

import Database.DatabaseManager;
import RestControllers.Raporting.Data.Common.FromTo;
import RestControllers.Raporting.Data.Histories.ApplicationData.ApplicationHistory;
import RestControllers.Raporting.Data.Histories.BrowserHistory;
import RestControllers.Raporting.Data.Histories.DailyHistory;
import RestControllers.Raporting.Data.Histories.HardwareData.HardwareData;
import ServiceStaticVariables.Variables;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class HistoriesQueryManager {
    public static boolean handle(ArrayList<DailyHistory> histories, Integer lgnId) {
        if(histories != null){
            return internalHandle(histories, lgnId);
        }
        return false;
    }

    private static boolean internalHandle(ArrayList<DailyHistory> histories, Integer lgnId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getHistoryDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            for(DailyHistory history:histories){
                handleHistory(connection, history, lgnId);
            }
            connection.commit();
            return true;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return false;
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    private static void handleHistory(Connection connection, DailyHistory history, Integer lgnId) throws Exception {
        Integer ymdId = DatabaseManager.getInstance().saveYearMonthDay(connection, history.getTime()).getId();
        handleApplicationHistory(connection, history.getApplicationHistory(), ymdId, lgnId);
        handleWebHistory(connection, history.getWebsidesHistory(), ymdId, lgnId);
        handleHardwareHistory(connection, history.getHardwaresData(), ymdId, lgnId);
    }

    private static void handleHardwareHistory(Connection connection, HashMap<String, HardwareData> hardwaresData, Integer ymdId, Integer lgnId) throws Exception {
        for(HashMap.Entry<String, HardwareData> entry : hardwaresData.entrySet()){
            Integer hardwareId = DatabaseManager.getInstance().saveHardware(connection, entry.getKey());
            Long from = entry.getValue().getFrom();
            Long queryFrequency = entry.getValue().getQueryFrequency();
            Integer hardwareHistoryId = DatabaseManager.getInstance().saveHardwareHistory(connection, hardwareId, from, queryFrequency, ymdId, lgnId);
            for (Map.Entry<String, ArrayList<Double>> hardwareEntry : entry.getValue().getValues().entrySet()) {
                Integer hardwareElementId = DatabaseManager.getInstance().saveHardwareElement(connection, hardwareEntry.getKey());
                long counter = 0;
                for (Double value : hardwareEntry.getValue()) {
                    DatabaseManager.getInstance().saveHardwareElementHistory(connection, hardwareHistoryId, hardwareElementId, value, counter, from + counter * queryFrequency);
                    counter++;
                }
            }
        }
    }

    private static void handleWebHistory(Connection connection, HashMap<String, BrowserHistory> websidesHistory, Integer ymdId, Integer lgnId) throws Exception {
        for(HashMap.Entry<String, BrowserHistory> entry : websidesHistory.entrySet()){
            Integer domainId = DatabaseManager.getInstance().saveDomain(connection, getDomain(entry.getKey()));
            for (FromTo fromTo : entry.getValue().getHistory()) {
                DatabaseManager.getInstance().saveWebsideHistory(connection, fromTo, entry.getKey(), ymdId, lgnId, domainId);
            }
        }
    }

    private static String getDomain(String url) {
        String domain = "";

        if(url.indexOf("://") > -1){
            domain = url.split("/")[2];
        }else{
            domain = url.split("/")[0];
        }

        return  domain.split(":")[0];
    }

    private static void handleApplicationHistory(Connection connection, HashMap<String, ApplicationHistory> applicationHistory, Integer ymdId, Integer lgnId) throws Exception {
        for(HashMap.Entry<String, ApplicationHistory> entry : applicationHistory.entrySet()){
            Integer id = DatabaseManager.getInstance().saveProgram(connection, entry.getKey());
            for (FromTo fromTo : entry.getValue().getHistory()) {
                DatabaseManager.getInstance().saveApplicationHistory(connection, fromTo, id, ymdId, lgnId);
            }
        }
    }


}
