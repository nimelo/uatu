USE statisticdatabase;

DROP PROCEDURE IF EXISTS get_pie_statistics_day_app;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pie_statistics_day_app(IN IN_LGN_ID INT, IN IN_DAY_FROM INT, IN IN_MONTH_FROM INT, IN IN_YEAR_FROM INT)
BEGIN
    SELECT PRG_PATH AS NAME, STT_TOTAL AS TOTAL
    FROM statistics JOIN programs ON STT_PRG_ID = PRG_ID
    WHERE STT_DYS_ID IN (SELECT DYS_ID
                             FROM days
                             WHERE DYS_YEAR = IN_YEAR_FROM
                             AND DYS_MONTH = IN_MONTH_FROM
                             AND DYS_DAY = IN_DAY_FROM)
    AND STT_LGN_ID = IN_LGN_ID;
END //
DELIMITER ;