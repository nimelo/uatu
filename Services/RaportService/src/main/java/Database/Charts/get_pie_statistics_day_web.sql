USE statisticdatabase;

DROP PROCEDURE IF EXISTS get_pie_statistics_day_web;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pie_statistics_day_web(IN IN_LGN_ID INT, IN IN_DAY_FROM INT, IN IN_MONTH_FROM INT, IN IN_YEAR_FROM INT)
BEGIN
    SELECT DMN_NAME AS NAME, STT_TOTAL AS TOTAL
    FROM statistics JOIN domains ON STT_DMN_ID = DMN_ID
    WHERE STT_DYS_ID IN (SELECT DYS_ID
                             FROM days
                             WHERE DYS_YEAR = IN_YEAR_FROM
                             AND DYS_MONTH = IN_MONTH_FROM
                             AND DYS_DAY = IN_DAY_FROM)
    AND STT_LGN_ID = IN_LGN_ID;
END //
DELIMITER ;