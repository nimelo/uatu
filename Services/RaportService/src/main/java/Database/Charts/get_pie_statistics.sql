USE statisticdatabase;

DROP PROCEDURE IF EXISTS get_pie_statistics;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pie_statistics(IN IN_LGN_ID INT, IN IN_DAY_FROM INT, IN IN_MONTH_FROM INT, IN IN_YEAR_FROM INT, IN IN_RANGE INT,
                                                      IN IN_APPS VARCHAR(10000), IN IN_DMNS VARCHAR(10000))
BEGIN
	DECLARE EXIST_ID INT DEFAULT 0;
	SET @FROM_DATE = STR_TO_DATE(CONCAT(IN_YEAR_FROM,'-',LPAD(IN_MONTH_FROM,2,'00'),'-',LPAD(IN_DAY_FROM,2,'00')), '%Y-%m-%d');
    SET @TO_DATE = DATE_ADD(@FROM_DATE,INTERVAL IN_RANGE DAY);

    SET @SQL = CONCAT("
    SELECT PRG_PATH AS NAME, SUM(STT_TOTAL) AS TOTAL
    FROM statistics JOIN programs ON STT_PRG_ID = PRG_ID
    WHERE STT_DYS_ID IN (SELECT DYS_ID
                             FROM days
                             WHERE DYS_VAL >= @FROM_DATE
                             AND DYS_VAL <= @TO_DATE)
    AND STT_PRG_ID IN (SELECT PRG_ID
                            FROM programs
                            WHERE PRG_PATH IN(", IN_APPS, "))
	GROUP BY PRG_PATH
    UNION ALL
    SELECT DMN_NAME AS NAME, SUM(STT_TOTAL) AS TOTAL
    FROM statistics JOIN domains ON STT_DMN_ID = DMN_ID
    WHERE STT_DYS_ID IN (SELECT DYS_ID
                             FROM days
                             WHERE DYS_VAL >= @FROM_DATE
                             AND DYS_VAL <= @TO_DATE)
    AND STT_DMN_ID IN (SELECT DMN_ID
                            FROM domains
                            WHERE DMN_NAME IN (", IN_DMNS ,"))
	GROUP BY DMN_NAME");

	PREPARE stmt FROM @SQL;
    EXECUTE stmt;

END //
DELIMITER ;