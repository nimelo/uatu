USE historydatabase;

DROP PROCEDURE IF EXISTS get_history_for_day;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_history_for_day(IN IN_LGN_ID INT, IN IN_FROM BIGINT, IN IN_TO BIGINT, IN IN_PRECISION INT)
BEGIN

    SELECT *
    FROM
        ((SELECT PRG_PATH AS NAME, APH_FROM AS FROM2, APH_TIME AS 'TIME'
            FROM application_histories JOIN programs ON APH_PRG_ID = PRG_ID
            WHERE APH_LGN_ID = IN_LGN_ID
            AND APH_FROM >= IN_FROM
            AND APH_FROM <= IN_TO
            AND APH_TIME >= IN_PRECISION
            ORDER BY APH_FROM ASC)
        UNION ALL
        (SELECT WSH_URL AS NAME, WSH_FROM AS FROM2, WSH_TIME AS 'TIME'
            FROM webside_histories
            WHERE WSH_LGN_ID = IN_LGN_ID
            AND WSH_FROM >= IN_FROM
            AND WSH_FROM <= IN_TO
            AND WSH_TIME >= IN_PRECISION
            ORDER BY WSH_FROM ASC)) A
    ORDER BY FROM2 ASC;

END //
DELIMITER ;