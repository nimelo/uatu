
USE historydatabase;

DROP PROCEDURE IF EXISTS get_hardware_element_ids_for;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_hardware_element_ids_for(IN IN_LGN_ID INT, IN IN_FROM BIGINT, IN IN_TO BIGINT)
BEGIN
	DECLARE EXIST_ID INT DEFAULT 0;

    SELECT DISTINCT HDE_ID, HDE_NAME
    FROM hardware_histories JOIN hardwares ON HDH_HRD_ID = HRD_ID
    JOIN hardware_element_histories ON HEDH_HDH_ID = HDH_ID
    JOIN hardware_elements ON HEDH_HDE_ID = HDE_ID
    WHERE HEDH_TIME >= IN_FROM
    AND HEDH_TIME <= IN_TO
    AND HDH_LGN_ID = IN_LGN_ID
    ORDER BY HDE_NAME;

END //
DELIMITER ;
