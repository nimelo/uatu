USE historydatabase;

DROP PROCEDURE IF EXISTS insert_hardware_element_histories;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insert_hardware_element_histories(IN IN_HEDH_HDH_ID INT, IN IN_HEDH_HDE_ID INT, IN IN_VALUE DOUBLE, IN IN_OFFSET INT, IN IN_TIME BIGINT,
								    OUT OUT_ID INT)
BEGIN
    INSERT INTO hardware_element_histories(HEDH_HDH_ID, HEDH_HDE_ID, HEDH_VALUE, HEDH_OFFSET, HEDH_TIME)
	                        VALUES(IN_HEDH_HDH_ID, IN_HEDH_HDE_ID, IN_VALUE, IN_OFFSET, IN_TIME);
	SET OUT_ID := LAST_INSERT_ID();

END //
DELIMITER ;