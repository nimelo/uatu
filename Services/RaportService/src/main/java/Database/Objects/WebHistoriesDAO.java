package Database.Objects;

import RestControllers.Raporting.Data.Common.FromTo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

/**
 * Created by mrnim on 20-Jul-16.
 */
public class WebHistoriesDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String INSERT_WEB_HISTORIES_SQL = "{CALL insert_webside_histories(?,?,?,?,?,?,?)}";

    public static void insert(Connection connection, FromTo fromTo, String url, Integer ymdId, Integer lgnId, Integer domainId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(INSERT_WEB_HISTORIES_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, ymdId);
            callableStatement.setInt(3, domainId);
            callableStatement.setString(4, url);
            callableStatement.setLong(5, fromTo.getFrom());
            callableStatement.setInt(6, (int)(long)fromTo.getTime());
            callableStatement.registerOutParameter(7, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }
}
