package Database.Objects;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class WebStatisticsDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String INSERT_WEB_STATISTICS_SQL = "{CALL insert_web_statistics(?,?,?,?,?)}";

    public static void insert(Connection connection, Integer lgnId, Integer ymdId, Integer dmnId, Long value) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(INSERT_WEB_STATISTICS_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, ymdId);
            callableStatement.setInt(3, dmnId);
            callableStatement.setInt(4, (int)(long)value);
            callableStatement.registerOutParameter(5, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }
}
