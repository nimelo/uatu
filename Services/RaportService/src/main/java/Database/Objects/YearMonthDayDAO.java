package Database.Objects;

import RestControllers.Raporting.Data.Common.YearMonthDay;

import java.sql.*;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class YearMonthDayDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String INSERT_DAYS_SQL = "{CALL insert_days(?,?,?,?)}";

    public static SimplyDatabaseResult insert(Connection connection, YearMonthDay ymd) throws Exception {
            CallableStatement callableStatement = null;
            ResultSet resultSet = null;
            try{
                callableStatement = connection.prepareCall(INSERT_DAYS_SQL);
                callableStatement.setInt(1, ymd.getYear());
                callableStatement.setInt(2, ymd.getMonth());
                callableStatement.setInt(3, ymd.getDay());
                callableStatement.registerOutParameter(4, Types.INTEGER);
                resultSet = callableStatement.executeQuery();

                int id = callableStatement.getInt(4);

                return SimplyDatabaseResult.SuccessResult(id);
            }catch (Exception e){
                throw e;
            }
            finally {
                if(resultSet != null){
                    resultSet.close();
                }

                if(callableStatement != null){
                    callableStatement.close();
                }
            }
    }
}
