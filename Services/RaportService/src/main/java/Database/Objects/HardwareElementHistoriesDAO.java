package Database.Objects;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

/**
 * Created by mrnim on 20-Jul-16.
 */
public class HardwareElementHistoriesDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String INSERT_HARDWARE_ELEMENT_HISTORIES_SQL = "{CALL insert_hardware_element_histories(?,?,?,?,?,?)}";

    public static void insert(Connection connection, Integer hardwareHistoryId, Integer hardwareElementId, Double value, Long counter, Long when) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(INSERT_HARDWARE_ELEMENT_HISTORIES_SQL);
            callableStatement.setInt(1, hardwareHistoryId);
            callableStatement.setInt(2, hardwareElementId);
            callableStatement.setDouble(3, value);
            callableStatement.setInt(4, (int)(long)counter);
            callableStatement.setLong(5, when);
            callableStatement.registerOutParameter(6, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }
}
