package Database.Objects;

import RestControllers.Raporting.Data.Common.FromTo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

/**
 * Created by mrnim on 20-Jul-16.
 */
public class AppHistoriesDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String INSERT_APP_HISTORIES_SQL = "{CALL insert_application_histories(?,?,?,?,?,?)}";

    public static void insert(Connection connection, FromTo value, Integer appId, Integer ymdId, Integer lgnId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(INSERT_APP_HISTORIES_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, ymdId);
            callableStatement.setInt(3, appId);
            callableStatement.setLong(4, value.getFrom());
            callableStatement.setInt(5, (int)(long)value.getTime());
            callableStatement.registerOutParameter(6, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }
}
