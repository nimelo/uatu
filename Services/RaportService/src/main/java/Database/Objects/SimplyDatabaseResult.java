package Database.Objects;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class SimplyDatabaseResult {
    private Boolean success;
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public static SimplyDatabaseResult FailResult(){
        return new SimplyDatabaseResult(false);
    }

    public static SimplyDatabaseResult SuccessResult(Integer id){
        return new SimplyDatabaseResult(id, true);
    }

    public Boolean getSuccess() {
        return success;
    }

    public SimplyDatabaseResult(Integer id, Boolean success) {
        this.id = id;
        this.success = success;
    }

    public SimplyDatabaseResult(Boolean success) {
        this.success = success;
    }

    public SimplyDatabaseResult() {
    }
}
