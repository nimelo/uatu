package Database.Objects;

import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.Results.StatisticCategoryResult;
import RestControllers.Statistics.Results.StatisticsDailyResult;
import RestControllers.Statistics.Results.StatisticsResponse;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class PieStatisticsDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String GET_SQL = "{CALL get_pie_statistics(?,?,?,?,?,?,?)}";
    private static final String GET_APP_DAILY_SQL = "{CALL get_pie_statistics_day_app(?,?,?,?)}";
    private static final String GET_WEB_DAILY_SQL = "{CALL get_pie_statistics_day_web(?,?,?,?)}";

    public static ArrayList<StatisticCategoryResult> getStatisticFor(Connection connection, Integer lgnId, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, from.getDay());
            callableStatement.setInt(3, from.getMonth());
            callableStatement.setInt(4, from.getYear());
            callableStatement.setInt(5, range);
            callableStatement.setString(6, ListToString(appElements));
            callableStatement.setString(7, ListToString(webElements));

            resultSet = callableStatement.executeQuery();
            ArrayList<StatisticCategoryResult> list = getStatisticCategoryResult(resultSet, appElements, webElements);

            return list;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<StatisticCategoryResult> getStatisticCategoryResult(ResultSet resultSet, ArrayList<String> appElements, ArrayList<String> webElements) throws SQLException {

        ArrayList<StatisticCategoryResult> array  = new ArrayList<StatisticCategoryResult>();
        HashMap<String, Long> map = new HashMap<String, Long>();

        for (String appElement : appElements) {
            map.put(appElement, 0L);
        }

        for (String webElement : webElements) {
            map.put(webElement, 0L);
        }

        while(resultSet.next()){
            String name = resultSet.getString("NAME");
            Long total = resultSet.getLong("TOTAL");
            map.put(name, map.getOrDefault(name, 0L) + total);
        }

        for(Map.Entry<String, Long> entry : map.entrySet()){
            StatisticCategoryResult statisticCategoryResult = new StatisticCategoryResult();
            statisticCategoryResult.setName(entry.getKey());
            statisticCategoryResult.setY(entry.getValue());
            array.add(statisticCategoryResult);
        }

        return  array;
    }

    public static String ListToString(ArrayList<String> list){
        StringBuilder stringBuilder = new StringBuilder("'',");

        for (String string : list) {
            stringBuilder.append(String.format("'%s',", string.replace("\\","\\\\")));
        }

        stringBuilder.replace(stringBuilder.lastIndexOf(",") ,stringBuilder.lastIndexOf(",") + 1, "");

        return stringBuilder.toString();
    }

    public static StatisticsResponse getStatisticsDailyFor(Connection connection, Integer lgnId, YearMonthDay day, Boolean app) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            String sql = app ? GET_APP_DAILY_SQL : GET_WEB_DAILY_SQL;
            callableStatement = connection.prepareCall(sql);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, day.getDay());
            callableStatement.setInt(3, day.getMonth());
            callableStatement.setInt(4, day.getYear());

            resultSet = callableStatement.executeQuery();
            return getResultsDaily(resultSet);
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static StatisticsResponse getResultsDaily(ResultSet resultSet) throws SQLException {
        StatisticsResponse statisticsResponse = new StatisticsResponse();
        ArrayList<StatisticCategoryResult> array = new ArrayList<StatisticCategoryResult>();

        while(resultSet.next()){
            String name = resultSet.getString("NAME");
            Long total = resultSet.getLong("TOTAL");
            StatisticCategoryResult statisticCategoryResult = new StatisticCategoryResult();
            statisticCategoryResult.setName(name);
            statisticCategoryResult.setY(total);
            array.add(statisticCategoryResult);
        }

        statisticsResponse.setData(array);
        statisticsResponse.setSuccess(true);
        return statisticsResponse;
    }
}
