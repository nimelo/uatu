package Database.Objects;

import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.Results.StatisticCategoryResult;
import RestControllers.Statistics.Results.StatisticsGroupResponse;
import RestControllers.Statistics.Results.StatisticsDailyResult;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mrnim on 26-Jul-16.
 */
public class BarStatisticsDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String GET_SQL = "{CALL get_pie_statistics(?,?,?,?,?,?,?)}";

    private static final String MONTHLY = "Monthly";
    private static final String YEARLY = "Yearly";
    private static final String DAILY = "Daily";

    public static ArrayList<StatisticCategoryResult> getStatisticFor(Connection connection, Integer lgnId, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, from.getDay());
            callableStatement.setInt(3, from.getMonth());
            callableStatement.setInt(4, from.getYear());
            callableStatement.setInt(5, range);
            callableStatement.setString(6, ListToString(appElements));
            callableStatement.setString(7, ListToString(webElements));

            resultSet = callableStatement.executeQuery();
            ArrayList<StatisticCategoryResult> list = getStatisticCategoryResult(resultSet, appElements, webElements);

            return list;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<StatisticCategoryResult> getStatisticCategoryResult(ResultSet resultSet, ArrayList<String> appElements, ArrayList<String> webElements) throws SQLException {

        ArrayList<StatisticCategoryResult> array  = new ArrayList<StatisticCategoryResult>();
        HashMap<String, Long> map = new HashMap<String, Long>();

        for (String appElement : appElements) {
            map.put(appElement, 0L);
        }

        for (String webElement : webElements) {
            map.put(webElement, 0L);
        }

        while(resultSet.next()){
            String name = resultSet.getString("NAME");
            Long total = resultSet.getLong("TOTAL");
            map.put(name, map.getOrDefault(name, 0L) + total);
        }

        for(Map.Entry<String, Long> entry : map.entrySet()){
            StatisticCategoryResult statisticCategoryResult = new StatisticCategoryResult();
            statisticCategoryResult.setName(entry.getKey());
            statisticCategoryResult.setY(entry.getValue());
            array.add(statisticCategoryResult);
        }

        return  array;
    }

    public static String ListToString(ArrayList<String> list){
        StringBuilder stringBuilder = new StringBuilder("'',");

        for (String string : list) {
            stringBuilder.append(String.format("'%s',", string.replace("\\","\\\\")));
        }

        stringBuilder.replace(stringBuilder.lastIndexOf(",") ,stringBuilder.lastIndexOf(",") + 1, "");

        return stringBuilder.toString();
    }

    public static StatisticsGroupResponse getStatisticsGroupFor(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range, String groupBy) throws Exception {
        if(DAILY.equals(groupBy)){
            return internalGetDaily(connection, id, appElements, webElements, from, range);
        } else if (MONTHLY.equals(groupBy)) {
            return internalGetMonthly(connection, id, appElements, webElements, from, range);
        }else if(YEARLY.equals(groupBy)){
            return internalGetYearly(connection, id, appElements, webElements, from, range);
        }

        throw new Exception();
    }

    private static StatisticsGroupResponse internalGetYearly(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) {
        return null;
    }

    private static StatisticsGroupResponse internalGetMonthly(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) throws Exception {
        HashMap<String, ArrayList<Long>> map = getMapFrom(appElements, webElements);
        ArrayList<String> fields = new ArrayList<String>();

        Calendar start = getStartingDate(from);
        Calendar endDate = getStartingDate(from);
        endDate.add(Calendar.DATE, range);
        do{
            fields.add(theMonth(start.get(Calendar.MONTH)));
            YearMonthDay newFrom = getYMDFromCalendar(start);
            ArrayList<StatisticCategoryResult> statisticFor = getStatisticFor(connection, id, appElements, webElements, newFrom, start.getActualMaximum(Calendar.DAY_OF_MONTH) - newFrom.getDay());
            addToMap(map, statisticFor);
            start.set(Calendar.DATE, 1);
            start.add(Calendar.MONTH, 1);
        }while(endDate.after(start));

        StatisticsGroupResponse result = new StatisticsGroupResponse();
        result.setData(mapToList(map));
        result.setFields(fields);
        return result;
    }

    public static String theMonth(int month){
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    private static StatisticsGroupResponse internalGetDaily(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) throws Exception {
        HashMap<String, ArrayList<Long>> map = getMapFrom(appElements, webElements);
        ArrayList<String> fields = new ArrayList<String>();

        Calendar start = getStartingDate(from);
        for(int i = 0; i <= range; i++){
            fields.add(new SimpleDateFormat("yyyy-MM-dd").format(start.getTime()));
            YearMonthDay newFrom = getYMDFromCalendar(start);
            ArrayList<StatisticCategoryResult> statisticFor = getStatisticFor(connection, id, appElements, webElements, newFrom, 0);
            addToMap(map, statisticFor);
            start.add(Calendar.DATE, 1);
        }

        StatisticsGroupResponse result = new StatisticsGroupResponse();
        result.setData(mapToList(map));
        result.setFields(fields);
        return result;
    }

    private static ArrayList<StatisticsDailyResult> mapToList(HashMap<String, ArrayList<Long>> map) {
        ArrayList<StatisticsDailyResult> list = new ArrayList<StatisticsDailyResult>();

        for (Map.Entry<String, ArrayList<Long>> entry : map.entrySet()) {
            StatisticsDailyResult obj = new StatisticsDailyResult();
            obj.setName(entry.getKey());
            obj.setData(entry.getValue());
            list.add(obj);
        }

        return list;
    }

    private static void addToMap(HashMap<String, ArrayList<Long>> map, ArrayList<StatisticCategoryResult> statisticFor) {
        for (StatisticCategoryResult result : statisticFor) {
            map.get(result.getName()).add(result.getY());
        }
    }

    private static HashMap<String, ArrayList<Long>> getMapFrom(ArrayList<String> appElements, ArrayList<String> webElements) {
        HashMap<String, ArrayList<Long>> map = new HashMap<String, ArrayList<Long>>();

        for (String appElement : appElements) {
            map.put(appElement, new ArrayList<Long>());
        }

        for (String webElement : webElements) {
            map.put(webElement, new ArrayList<Long>());
        }

        return map;
    }

    private static YearMonthDay getYMDFromCalendar(Calendar start) {
        YearMonthDay yearMonthDay = new YearMonthDay();
        yearMonthDay.setDay(start.get(Calendar.DATE));
        yearMonthDay.setMonth(start.get(Calendar.MONTH) + 1);
        yearMonthDay.setYear(start.get(Calendar.YEAR));
        return yearMonthDay;
    }

    private static Calendar getStartingDate(YearMonthDay from) {
        Calendar c = Calendar.getInstance();
        c.set(from.getYear(), from.getMonth() - 1, from.getDay(), 0, 0);
        return c;
    }


}
