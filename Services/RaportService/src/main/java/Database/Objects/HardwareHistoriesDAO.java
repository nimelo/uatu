package Database.Objects;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

/**
 * Created by mrnim on 20-Jul-16.
 */
public class HardwareHistoriesDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String INSERT_HARDWARE_HISTORIES_SQL = "{CALL insert_hardware_histories(?,?,?,?,?,?)}";

    public static Integer insert(Connection connection, Integer hardwareId, Long from, Long queryFrequency, Integer ymdId, Integer lgnId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(INSERT_HARDWARE_HISTORIES_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, ymdId);
            callableStatement.setInt(3, hardwareId);
            callableStatement.setLong(4, from);
            callableStatement.setInt(5, (int)(long)queryFrequency);
            callableStatement.registerOutParameter(6, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

            return callableStatement.getInt(6);
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

}
