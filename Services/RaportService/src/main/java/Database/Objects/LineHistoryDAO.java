package Database.Objects;

import RestControllers.Histories.Data.HistoryHardwareSeries;
import RestControllers.Histories.Results.HistoriesHardwareResponse;
import RestControllers.Statistics.Results.StatisticCategoryResult;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by mrnim on 28-Jul-16.
 */
public class LineHistoryDAO {

    private static final String GET_HARDWARE_ELEMENT_HISTORY_FOR_SQL = "{CALL get_hardware_element_history_for(?,?,?,?,?,?)}";
    private static final String GET_HARDWARE_ELEMENT_IDS_SQL = "{CALL get_hardware_element_ids_for(?,?,?)}";

    public static HistoriesHardwareResponse getHardwareHistoriesFor(Connection connection, Integer id, Long from, Long time) throws Exception {
        Long to = from + time;
        ArrayList<HistoryHardwareSeries> series = new ArrayList<HistoryHardwareSeries>();
        ArrayList<StrinInt> hardwareElementIds = getHardwareElementIds(connection, id, from, to);

        for (StrinInt he : hardwareElementIds) {
            HistoryHardwareSeries s = getSeries(connection, id, from, to, he.getId(), he.getName());
            series.add(s);
        }

        HistoriesHardwareResponse historiesHardwareResponse = new HistoriesHardwareResponse();
        historiesHardwareResponse.setSuccess(true);
        historiesHardwareResponse.setSeries(series);
        return historiesHardwareResponse;
    }

    private static HistoryHardwareSeries getSeries(Connection connection, Integer lgnId, Long from, Long to, Integer hardwareElementId, String name) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_HARDWARE_ELEMENT_HISTORY_FOR_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setLong(2, from);
            callableStatement.setLong(3, to);
            callableStatement.setInt(4, hardwareElementId);
            callableStatement.registerOutParameter(5, Types.BIGINT);
            callableStatement.registerOutParameter(6, Types.INTEGER);

            resultSet = callableStatement.executeQuery();

            Integer freq = callableStatement.getInt(6);
            Long fromReal = callableStatement.getLong(5);

            ArrayList<Double> values = getValues(resultSet);
            HistoryHardwareSeries result = new HistoryHardwareSeries();
            result.setData(values);
            result.setName(name);
            result.setPointInterval(Long.valueOf(freq));
            result.setPointStart(fromReal);
            return result;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<Double> getValues(ResultSet resultSet) throws SQLException {
        ArrayList<Double> doubles = new ArrayList<Double>();

        while(resultSet.next()){
            resultSet.getInt(1);
            doubles.add(resultSet.getDouble(2));
            resultSet.getLong(3);
        }

        return  doubles;
    }

    private static ArrayList<StrinInt> getHardwareElementIds(Connection connection, Integer lgnId, Long from, Long to) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_HARDWARE_ELEMENT_IDS_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setLong(2, from);
            callableStatement.setLong(3, to);

            resultSet = callableStatement.executeQuery();
            ArrayList<StrinInt> list = getHardwareElementIdsResult(resultSet);

            return list;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<StrinInt> getHardwareElementIdsResult(ResultSet resultSet) throws SQLException {
        ArrayList<StrinInt> strinInts = new ArrayList<StrinInt>();
        while(resultSet.next()){
            StrinInt strinInt = new StrinInt();
            strinInt.setId(resultSet.getInt("HDE_ID"));
            strinInt.setName(resultSet.getString("HDE_NAME"));
            strinInts.add(strinInt);
        }

        return strinInts;
    }

    private static class StrinInt{
        private String name;
        private Integer id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
