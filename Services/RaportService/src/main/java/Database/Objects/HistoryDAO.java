package Database.Objects;

import RestControllers.Histories.Data.HistoryEntry;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class HistoryDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static final String GET_SQL = "{CALL get_history_for_day(?,?,?,?)}";

    public static ArrayList<HistoryEntry> internalGetHistoryForDay(Connection connection, Integer lgnId, Long unixFrom, Long unixTo, Integer precision) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setLong(2, unixFrom);
            callableStatement.setLong(3, unixTo);
            callableStatement.setInt(4, precision);

            resultSet = callableStatement.executeQuery();
            ArrayList<HistoryEntry> list = getHistoryEntryResult(resultSet);

            return list;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<HistoryEntry> getHistoryEntryResult(ResultSet resultSet) throws Exception {
        ArrayList<HistoryEntry> historyEntries = new ArrayList<HistoryEntry>();

        while(resultSet.next()){
            HistoryEntry entry = getHistoryEntry(resultSet);
            historyEntries.add(entry);
        }

        return historyEntries;
    }

    private static HistoryEntry getHistoryEntry(ResultSet resultSet) throws SQLException {
        HistoryEntry entry = new HistoryEntry();
        String name = resultSet.getString("NAME");
        Long from = resultSet.getLong("FROM2");
        Long time = resultSet.getLong("TIME");
        Long to = from + time;

        entry.setName(name);
        entry.setFrom(from);
        entry.setTime(time);
        entry.setTo(to);

        return entry;

    }
}
