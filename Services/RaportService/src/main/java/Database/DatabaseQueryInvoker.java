package Database;

import Database.Objects.PieStatisticsDAO;
import Database.Objects.*;
import RestControllers.Histories.Results.HistoriesHardwareResponse;
import RestControllers.Raporting.Data.Common.FromTo;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.Results.StatisticCategoryResult;
import RestControllers.Statistics.Results.StatisticsGroupResponse;
import RestControllers.Statistics.Results.StatisticsResponse;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class DatabaseQueryInvoker {
    private static final String INSERT_DOMAINS_SQL = "{CALL insert_domains(?,?)}";
    private static final String INSERT_PROGRAMS_SQL = "{CALL insert_programs(?,?)}";
    private static final String INSERT_HARDWARES_SQL = "{CALL insert_hardwares(?,?)}";
    private static final String INSERT_HARDWARE_ELEMENTS_SQL = "{CALL insert_hardware_elements(?,?)}";

    public static SimplyDatabaseResult insert_day(Connection db, YearMonthDay ymd) throws Exception {
        return YearMonthDayDAO.insert(db, ymd);
    }

    public static Integer insert_domains(Connection connection, String domain) throws Exception {
        return execute1ArgStatement(connection, domain, INSERT_DOMAINS_SQL).getId();
    }

    public static Integer insert_programs(Connection connection, String path) throws Exception {
        return execute1ArgStatement(connection, path, INSERT_PROGRAMS_SQL).getId();
    }

    public static Integer insert_hardwares(Connection connection, String name) throws Exception {
        return execute1ArgStatement(connection, name, INSERT_HARDWARES_SQL).getId();
    }

    public static Integer insert_hardware_elements(Connection connection, String name) throws Exception {
        return execute1ArgStatement(connection, name, INSERT_HARDWARE_ELEMENTS_SQL).getId();
    }

    private static SimplyDatabaseResult execute1ArgStatement(Connection connection, String arg1, String sql) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, arg1);
            callableStatement.registerOutParameter(2, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

            int id = callableStatement.getInt(2);

            return SimplyDatabaseResult.SuccessResult(id);
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null) {
                callableStatement.close();
            }
        }
    }

    public static void insert_web_statistics(Connection connection, Integer lgnId, Integer ymdId, Integer dmnId, Long value) throws Exception {
        WebStatisticsDAO.insert(connection, lgnId, ymdId, dmnId, value);
    }

    public static void insert_app_statistic(Connection connection, Integer lgnId, Integer ymdId, Integer pathId, Long value) throws Exception {
        AppStatisticsDAO.insert(connection, lgnId, ymdId, pathId, value);
    }

    public static Integer insert_hardware_histories(Connection connection, Integer hardwareId, Long from, Long queryFrequency, Integer ymdId, Integer lgnId) throws Exception {
        return HardwareHistoriesDAO.insert(connection, hardwareId, from, queryFrequency, ymdId, lgnId);
    }

    public static void insert_hardware_element_histories(Connection connection, Integer hardwareHistoryId, Integer hardwareElementId, Double value, Long counter, Long when) throws Exception {
        HardwareElementHistoriesDAO.insert(connection, hardwareHistoryId, hardwareElementId, value, counter, when);
    }

    public static void insert_webside_histories(Connection connection, FromTo fromTo, String url, Integer ymdId, Integer lgnId, Integer domainId) throws Exception {
        WebHistoriesDAO.insert(connection, fromTo, url, ymdId, lgnId, domainId);
    }

    public static void insert_application_histories(Connection connection, FromTo value, Integer id, Integer ymdId, Integer lgnId) throws Exception {
        AppHistoriesDAO.insert(connection, value, id, ymdId, lgnId);
    }

    public static ArrayList<StatisticCategoryResult> get_statistic_for(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) throws Exception {
        return PieStatisticsDAO.getStatisticFor(connection, id, appElements, webElements, from, range);
    }

    public static StatisticsGroupResponse get_statistic_group_for(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range, String groupBy) throws Exception {
        return BarStatisticsDAO.getStatisticsGroupFor(connection, id, appElements, webElements, from, range, groupBy);
    }

    public static HistoriesHardwareResponse getHardwareHistoriesFor(Connection connection, Integer id, Long from, Long time) throws Exception {
        return LineHistoryDAO.getHardwareHistoriesFor(connection, id, from, time);
    }

    public static StatisticsResponse getStatisticsDailyFor(Connection connection, Integer id, YearMonthDay day, Boolean app) throws Exception {
        return PieStatisticsDAO.getStatisticsDailyFor(connection, id, day, app);
    }
}
