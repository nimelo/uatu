package Database;

import Database.Objects.SimplyDatabaseResult;
import RestControllers.Histories.Results.HistoriesHardwareResponse;
import RestControllers.Raporting.Data.Common.FromTo;
import RestControllers.Raporting.Data.Common.YearMonthDay;
import RestControllers.Statistics.Results.StatisticCategoryResult;
import RestControllers.Statistics.Results.StatisticsGroupResponse;
import RestControllers.Statistics.Results.StatisticsResponse;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class DatabaseManager {
    private static DatabaseManager instance;

    public static DatabaseManager getInstance(){
        if(instance == null){
            instance = new DatabaseManager();
        }

        return instance;
    }

    public SimplyDatabaseResult saveYearMonthDay(Connection connection, YearMonthDay ymd) throws Exception {
        return DatabaseQueryInvoker.insert_day(connection, ymd);
    }

    public Integer saveDomain(Connection connection, String dmn) throws Exception {
        return DatabaseQueryInvoker.insert_domains(connection, dmn);
    }

    public void saveWebsideStatistic(Connection connection, Long value, Integer dmnId, Integer ymdId, Integer lgnId) throws Exception {
        DatabaseQueryInvoker.insert_web_statistics(connection, lgnId, ymdId, dmnId, value);
    }

    public Integer saveProgram(Connection connection, String path) throws Exception {
        return DatabaseQueryInvoker.insert_programs(connection, path);
    }

    public void saveApplicationStatistic(Connection connection, Long value, Integer pathId, Integer ymdId, Integer lgnId) throws Exception {
        DatabaseQueryInvoker.insert_app_statistic(connection, lgnId, ymdId, pathId, value);
    }

    public void saveApplicationHistory(Connection connection, FromTo value, Integer id, Integer ymdId, Integer lgnId) throws Exception {
        DatabaseQueryInvoker.insert_application_histories(connection, value, id, ymdId, lgnId);
    }

    public void saveWebsideHistory(Connection connection, FromTo fromTo, String url, Integer ymdId, Integer lgnId, Integer domainId) throws Exception {
        DatabaseQueryInvoker.insert_webside_histories(connection, fromTo, url, ymdId, lgnId, domainId);
    }

    public Integer saveHardware(Connection connection, String name) throws Exception {
        return DatabaseQueryInvoker.insert_hardwares(connection, name);
    }

    public Integer saveHardwareElement(Connection connection, String name) throws Exception {
        return DatabaseQueryInvoker.insert_hardware_elements(connection, name);
    }


    public Integer saveHardwareHistory(Connection connection, Integer hardwareId, Long from, Long queryFrequency, Integer ymdId, Integer lgnId) throws Exception {
        return DatabaseQueryInvoker.insert_hardware_histories(connection, hardwareId, from, queryFrequency, ymdId, lgnId);
    }

    public void saveHardwareElementHistory(Connection connection, Integer hardwareHistoryId, Integer hardwareElementId, Double value, Long counter, Long when) throws Exception {
        DatabaseQueryInvoker.insert_hardware_element_histories(connection, hardwareHistoryId, hardwareElementId, value, counter, when);
    }

    public ArrayList<StatisticCategoryResult> getStatisticFor(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range) throws Exception {
        return DatabaseQueryInvoker.get_statistic_for(connection, id, appElements, webElements, from, range);
    }

    public StatisticsGroupResponse getStatisticsGroupFor(Connection connection, Integer id, ArrayList<String> appElements, ArrayList<String> webElements, YearMonthDay from, Integer range, String groupBy) throws Exception {
        return DatabaseQueryInvoker.get_statistic_group_for(connection, id, appElements, webElements, from, range, groupBy);
    }

    public HistoriesHardwareResponse getHardwareHistoriesFor(Connection connection, Integer id, Long from, Long time) throws Exception {
        return DatabaseQueryInvoker.getHardwareHistoriesFor(connection, id, from, time);
    }

    public StatisticsResponse getStatisticsDailyFor(Connection connection, Integer id, YearMonthDay day, Boolean app) throws Exception {
        return DatabaseQueryInvoker.getStatisticsDailyFor(connection, id, day, app);
    }
}
