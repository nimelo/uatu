package ApplicationContext;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class CurrentContext {
    private static ServiceConfiguration resolvingServiceConfiguration;
    private static ServiceConfiguration loginServiceConfiguration;
    private static ServiceConfiguration authServiceConfiguration;

    public static ServiceConfiguration getLoginServiceConfiguration() {
        return loginServiceConfiguration;
    }

    public static void setLoginServiceConfiguration(ServiceConfiguration loginServiceConfiguration) {
        CurrentContext.loginServiceConfiguration = loginServiceConfiguration;
    }

    public static ServiceConfiguration getResolvingServiceConfiguration() {
        return resolvingServiceConfiguration;
    }

    public static void setResolvingServiceConfiguration(ServiceConfiguration resolvingServiceConfiguration) {
        CurrentContext.resolvingServiceConfiguration = resolvingServiceConfiguration;
    }

    public static void setAuthServiceConfiguration(ServiceConfiguration authServiceConfiguration) {
        CurrentContext.authServiceConfiguration = authServiceConfiguration;
    }

    public static ServiceConfiguration getAuthServiceConfiguration() {
        return authServiceConfiguration;
    }
}
