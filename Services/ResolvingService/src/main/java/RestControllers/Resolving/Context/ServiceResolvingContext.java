package RestControllers.Resolving.Context;

import RestControllers.Resolving.Data.ServiceQuery;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class ServiceResolvingContext {
    private String secretString;
    private ServiceQuery query;

    public ServiceQuery getQuery() {
        return query;
    }

    public void setQuery(ServiceQuery query) {
        this.query = query;
    }

    public String getSecretString() {
        return secretString;
    }

    public void setSecretString(String secretString) {
        this.secretString = secretString;
    }
}
