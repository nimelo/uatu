package RestControllers.Resolving;

import Database.ServiceConfiguration;
import Database.ServiceStaticVariables;
import Database.ServicesConfigurationDatabase;
import RestControllers.Resolving.Context.ServiceResolvingContext;
import RestControllers.Resolving.Results.ResolvingResult;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mrnim on 17-Jul-16.
 */
@RestController
@Controller
@EnableAutoConfiguration
@RequestMapping("/resolve")
public class ResolvingController {

    @RequestMapping(method = RequestMethod.POST)
    public ResolvingResult resolveService(@RequestBody ServiceResolvingContext ctx){
        if(ServiceStaticVariables.getSecretKey().equals(ctx.getSecretString())){
            ServicesConfigurationDatabase database = ServiceStaticVariables.getDatabase();
            ServiceConfiguration service = database.getServices().getOrDefault(ctx.getQuery().getServiceName(), null);
            if(service != null){
                return new ResolvingResult(service.getAddress(), true, service.getPath(), service.getPort());
            }
        }
        return ResolvingResult.FailResult();
    }
}
