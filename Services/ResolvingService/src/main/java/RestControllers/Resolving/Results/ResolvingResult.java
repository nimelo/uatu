package RestControllers.Resolving.Results;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class ResolvingResult {
    private Boolean isSuccess;
    private String ip;
    private String port;
    private String path;

    public ResolvingResult(String ip, Boolean isSuccess, String path, String port) {
        this.ip = ip;
        this.isSuccess = isSuccess;
        this.path = path;
        this.port = port;
    }

    public ResolvingResult() {
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public static ResolvingResult FailResult(){
        ResolvingResult resolvingResult = new ResolvingResult();
        resolvingResult.setSuccess(false);
        return resolvingResult;
    }
}
