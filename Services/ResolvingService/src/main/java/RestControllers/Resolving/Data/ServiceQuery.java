package RestControllers.Resolving.Data;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class ServiceQuery {
    private String serviceName;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
