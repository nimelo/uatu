import Database.ServiceStaticVariables;
import Database.ServicesConfigurationDatabase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mrnim on 15-Jul-16.
 */
@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages = "RestControllers")
public class ResolvingMain {

    @RequestMapping("/")
    public String home(){
        return "Hello World!";
    }

    public static void main(String[] args){
        SetUpBeans();
        SpringApplication.run(ResolvingMain.class, args);
    }

    public static void SetUpBeans(){
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

        ServicesConfigurationDatabase db = (ServicesConfigurationDatabase) context.getBean("db");
        String secretKey = (String)context.getBean("secretKey");

        ServiceStaticVariables.setDatabase(db);
        ServiceStaticVariables.setSecretKey(secretKey);
    }
}
