package Database;

import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class ServicesConfigurationDatabase {
    @Resource
    @Qualifier(value="serviceConfigurations")
    private HashMap<String, ServiceConfiguration> services;

    public HashMap<String, ServiceConfiguration> getServices() {
        return services;
    }

    public void setServices(HashMap<String, ServiceConfiguration> services) {
        this.services = services;
    }

    public ServicesConfigurationDatabase() {
        this.services = new HashMap<String, ServiceConfiguration>();
    }

    public void LoadConfigurationFromResources(){

    }
}
