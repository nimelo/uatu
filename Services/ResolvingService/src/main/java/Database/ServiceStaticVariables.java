package Database;

/**
 * Created by mrnim on 18-Jul-16.
 */
public final class ServiceStaticVariables {
    private static ServicesConfigurationDatabase database;
    private static String secretKey;

    public static String getSecretKey() {
        return secretKey;
    }

    public static void setSecretKey(String secretKey) {
        ServiceStaticVariables.secretKey = secretKey;
    }

    public static ServicesConfigurationDatabase getDatabase() {
        return database;
    }

    public static void setDatabase(ServicesConfigurationDatabase database) {
        ServiceStaticVariables.database = database;
    }
}
