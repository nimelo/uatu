package LoginCheckers;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class LoginResponse {
    private Boolean success;
    private Integer id;

    public LoginResponse(Integer id, Boolean success) {
        this.id = id;
        this.success = success;
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public LoginResponse() {
    }

    public static LoginResponse FailResponse(){
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setSuccess(false);
        return loginResponse;
    }
}
