package LoginCheckers;

import ApplicationContext.CurrentContext;
import Requests.HttpPostHelper;
import RestControllers.Configuration.Requests.BaseRequest;
import RestControllers.Configuration.Requests.ConfigurationRequest;
import com.google.gson.Gson;

import java.io.StringReader;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class LoginChecker {
    private static LoginChecker instance;
    public static LoginChecker getInstance() {
        if(instance == null){
            instance = new LoginChecker();
        }

        return instance;
    }

    private LoginChecker() {
    }

    public LoginResponse getId(ConfigurationRequest credentials){
        String url = CurrentContext.getLoginServiceConfiguration().getURL();
        String json = new Gson().toJson(credentials);

        try {
            String result = HttpPostHelper.Post(url, json);
            LoginResponse loginResponse = new Gson().fromJson(new StringReader(result), LoginResponse.class);
            return loginResponse;
        } catch (Exception e) {

        }
        return LoginResponse.FailResponse();
    }

    public LoginResponse getId(BaseRequest credentials){
        String url = CurrentContext.getAuthServiceConfiguration().getURL();
        String json = new Gson().toJson(credentials);

        try {
            String result = HttpPostHelper.Post(url, json);
            LoginResponse loginResponse = new Gson().fromJson(new StringReader(result), LoginResponse.class);
            return loginResponse;
        } catch (Exception e) {

        }
        return LoginResponse.FailResponse();
    }

}
