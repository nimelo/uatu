package Database;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class DatabaseConfiguration {
    private String address;
    private String port;
    private String user;
    private String password;
    private String dbName;

    public DatabaseConfiguration() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public DatabaseConfiguration(String address, String dbName, String password, String port, String user) {
        this.address = address;
        this.dbName = dbName;
        this.password = password;
        this.port = port;
        this.user = user;
    }

    public String getMySqlUrl(){
        final String jdbc = "jdbc:mysql";
        final String noAccess = "noAccessToProcedureBodies=true";
        return String.format("%s://%s:%s/%s?%s&user=%s&password=%s", jdbc, this.address, this.port, this.dbName, noAccess, this.user, this.password);
    }
}
