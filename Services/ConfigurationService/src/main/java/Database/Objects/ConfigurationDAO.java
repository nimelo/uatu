package Database.Objects;

import ApplicationContext.CurrentContext;
import RestControllers.Configuration.Responses.ConfigurationResponse;
import ServiceStaticVariables.Variables;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mrnim on 22-Jul-16.
 */
public class ConfigurationDAO {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    private static final String GET_APP_CONFIGURATIONS_SQL = "{CALL get_app_configurations(?)}";
    private static final String GET_WEB_CONFIGURATIONS_SQL = "{CALL get_web_configurations(?)}";
    private static final String GET_BROWSERS_PATHS_SQL = "{CALL get_browser_paths(?)}";

    private static final String BRW_NAME = "BRW_NAME";
    private static final String BRP_PATH = "BRP_PATH";
    private static final String APC_PATH = "APC_PATH";
    private static final String WBC_URL = "WBC_URL";

    public static ConfigurationResponse handleConfiguration(Integer id) throws Exception {
        Connection connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
        ConfigurationResponse configurationResponse = new ConfigurationResponse();
        configurationResponse.setSuccess(true);
        configurationResponse.setAppExceptions(getAppExceptions(connection, id));
        configurationResponse.setWebExceptions(getWebExceptions(connection, id));
        configurationResponse.setBrowsersExePaths(getBrowsersExePaths(connection, id));
        return configurationResponse;
    }

    private static HashMap<String, String> getBrowsersExePaths(Connection connection, Integer id) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_BROWSERS_PATHS_SQL);
            callableStatement.setInt(1, id);
            resultSet = callableStatement.executeQuery();
            HashMap<String, String> map = new HashMap<String, String>();

            while(resultSet.next()){
                map.put(resultSet.getString(BRW_NAME), resultSet.getString(BRP_PATH));
            }

            return map;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<String> getWebExceptions(Connection connection, Integer id) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_WEB_CONFIGURATIONS_SQL);
            callableStatement.setInt(1, id);
            resultSet = callableStatement.executeQuery();
            ArrayList<String> list = new ArrayList<String>();

            while(resultSet.next()){
                list.add(resultSet.getString(WBC_URL));
            }

            return list;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<String> getAppExceptions(Connection connection, Integer id) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_APP_CONFIGURATIONS_SQL);
            callableStatement.setInt(1, id);
            resultSet = callableStatement.executeQuery();
            ArrayList<String> list = new ArrayList<String>();

            while(resultSet.next()){
                list.add(resultSet.getString(APC_PATH));
            }

            return list;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }


    public static void insert(Connection connection,  Integer appId, Integer ymdId, Integer lgnId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_APP_CONFIGURATIONS_SQL);
            callableStatement.setInt(1, lgnId);
            callableStatement.setInt(2, ymdId);
            callableStatement.setInt(3, appId);
            //callableStatement.setLong(4, value.getFrom());
            //callableStatement.setInt(5, (int)(long)value.getTime());
            callableStatement.registerOutParameter(6, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }
}
