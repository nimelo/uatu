package Database.Objects;

import RestControllers.Configuration.Data.AppNaming.AppNamingInfo;
import RestControllers.Configuration.Data.Category.CategoryInfo;
import RestControllers.Configuration.Responses.AppNaming.GetAppNamingListResponse;
import RestControllers.Configuration.Responses.BaseResponse;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class AppNamingDAO {

    private static final String DELETE_APPNAMINGS_SQL = "{CALL delete_appNaming(?,?,?)}";
    private static final String ADD_APPNAMINGS_SQL = "{CALL add_appNaming(?,?,?)}";

    private static final String GET_APPNAMINGS_LIST_FOR_USER = "{CALL get_appNaming_list_for_user(?)}";


    public static BaseResponse addNaming(Connection connection, Integer id, AppNamingInfo appNaming) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(ADD_APPNAMINGS_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setString(2, appNaming.getName());
            callableStatement.setString(3, appNaming.getPath());
            resultSet = callableStatement.executeQuery();

            return BaseResponse.SuccessResponse();
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static GetAppNamingListResponse getAppNamingListForUser(Connection connection, Integer id) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_APPNAMINGS_LIST_FOR_USER);
            callableStatement.setInt(1, id);
            resultSet = callableStatement.executeQuery();

            ArrayList<AppNamingInfo> appNamingInfos = new ArrayList<AppNamingInfo>();

            while(resultSet.next()){
                AppNamingInfo info = new AppNamingInfo();
                Integer anId = resultSet.getInt("APN_ID");
                String anName = resultSet.getString("APN_NAME");
                String anPath = resultSet.getString("APN_PATH");
                info.setId(anId);
                info.setName(anName);
                info.setPath(anPath);
                appNamingInfos.add(info);
            }
            GetAppNamingListResponse getAppNamingListResponse = new GetAppNamingListResponse();
            getAppNamingListResponse.setSuccess(true);
            getAppNamingListResponse.setAppNamings(appNamingInfos);

            return getAppNamingListResponse;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static BaseResponse deleteAppNaming(Connection connection, Integer id, Integer appNamingId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(DELETE_APPNAMINGS_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, appNamingId);
            callableStatement.registerOutParameter(3, Types.BOOLEAN);
            resultSet = callableStatement.executeQuery();

            boolean success = callableStatement.getBoolean(3);

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(success);

            return baseResponse;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }
}
