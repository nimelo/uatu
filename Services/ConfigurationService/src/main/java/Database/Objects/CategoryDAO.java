package Database.Objects;

import RestControllers.Configuration.Data.Category.Category;
import RestControllers.Configuration.Data.Category.CategoryElement;
import RestControllers.Configuration.Data.Category.CategoryInfo;
import RestControllers.Configuration.Responses.BaseResponse;
import RestControllers.Configuration.Responses.Category.GetCategoryListResponse;
import RestControllers.Configuration.Responses.Category.GetCategoryResponse;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class CategoryDAO {
    private static final String DELETE_CATEGORY_SQL = "{CALL delete_category(?,?,?)}";
    private static final String CHANGE_VISIBILITY_SQL = "{CALL change_visibility(?,?,?)}";
    private static final String ADD_CATEGORY_ELEMENT_SQL = "{CALL add_category_element(?,?,?,?)}";
    private static final String ADD_CATEGORY_SQL = "{CALL add_category(?,?,?,?,?)}";
    private static final String GET_CATEGORIES_LIST_FOR_USER = "{CALL get_categories_list_for_user(?)}";
    private static final String GET_CATEGORY_SQL = "{CALL get_category(?,?,?,?,?,?)}";

    public static BaseResponse deleteCategory(Connection connection, Integer id, Integer categoryId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(DELETE_CATEGORY_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, categoryId);
            callableStatement.registerOutParameter(3, Types.BOOLEAN);
            resultSet = callableStatement.executeQuery();

            boolean success = callableStatement.getBoolean(3);

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(success);

            return baseResponse;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static BaseResponse changeVisibilityCategory(Connection connection, Integer id, Integer categoryId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(CHANGE_VISIBILITY_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, categoryId);
            callableStatement.registerOutParameter(3, Types.BOOLEAN);
            resultSet = callableStatement.executeQuery();

            boolean success = callableStatement.getBoolean(3);

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(success);

            return baseResponse;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static BaseResponse modifyCategory(Connection connection, Integer id, Category category) throws Exception {
        BaseResponse baseResponse = deleteCategory(connection, id, category.getInfo().getId());
        if(baseResponse.getSuccess()){
            return addCategory(connection, id, category);
        }else{
            return BaseResponse.FailResponse("");
        }
    }

    public static BaseResponse addCategory(Connection connection, Integer id, Category category) throws Exception {
        try{
            Integer categoryId = internalCategoryAdd(connection, id, category.getInfo());

            if(category.getElements() == null){
                category.setElements(new ArrayList<CategoryElement>());
            }

            for (CategoryElement categoryElement : category.getElements()) {
                addCategoryElement(connection, id, categoryElement, categoryId);
            }

            return BaseResponse.SuccessResponse();
        }catch(Exception e){
            throw e;
        }
    }

    private static void addCategoryElement(Connection connection, Integer id, CategoryElement categoryElement, Integer categoryId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(ADD_CATEGORY_ELEMENT_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, categoryId);
            callableStatement.setString(3, categoryElement.getName());
            callableStatement.setString(4, categoryElement.getType());
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static Integer internalCategoryAdd(Connection connection, Integer id, CategoryInfo info) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(ADD_CATEGORY_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, info.getRange());
            callableStatement.setString(3, info.getName());
            callableStatement.setBoolean(4, info.getVisibility());
            callableStatement.registerOutParameter(5, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

            Integer resultId = callableStatement.getInt(5);

            return resultId;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static GetCategoryListResponse getCategoryListForUser(Connection connection, Integer id) throws Exception {
        GetCategoryListResponse response = new GetCategoryListResponse();
        ArrayList<CategoryInfo> categories = internalGetCategoryListForUser(connection, id);
        response.setSuccess(true);
        response.setCategories(categories);

        return response;
    }

    private static ArrayList<CategoryInfo> internalGetCategoryListForUser(Connection connection, Integer id) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_CATEGORIES_LIST_FOR_USER);
            callableStatement.setInt(1, id);
            resultSet = callableStatement.executeQuery();

            ArrayList<CategoryInfo> categoryInfos = new ArrayList<CategoryInfo>();

            while(resultSet.next()){
                CategoryInfo info = new CategoryInfo();
                Integer ctgId = resultSet.getInt("CTG_ID");
                String ctgName = resultSet.getString("CTG_NAME");
                Integer ctgVis = resultSet.getInt("CTG_VISIBILITY");
                Integer ctgRange = resultSet.getInt("CTG_RANGE");
                info.setId(ctgId);
                info.setName(ctgName);
                info.setRange(ctgRange);
                info.setVisibility(ctgVis == 1);
                categoryInfos.add(info);
            }

            return categoryInfos;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static GetCategoryResponse getCategory(Connection connection, Integer id, Integer categoryId) throws Exception {
        GetCategoryResponse response = new GetCategoryResponse();
        Category category = internalGetCategory(connection, id, categoryId);
        response.setSuccess(true);
        response.setCategory(category);

        return response;
    }

    private static Category internalGetCategory(Connection connection, Integer id, Integer categoryId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_CATEGORY_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, categoryId);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.INTEGER);
            callableStatement.registerOutParameter(5, Types.INTEGER);
            callableStatement.registerOutParameter(6, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

            Category category = new Category();
            CategoryInfo info = getInfo(callableStatement);
            ArrayList<CategoryElement> elements = getElements(resultSet);

            category.setInfo(info);
            category.setElements(elements);

            return category;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<CategoryElement> getElements(ResultSet resultSet) throws SQLException {
        ArrayList<CategoryElement> elements = new ArrayList<CategoryElement>();

        while (resultSet.next()){
            CategoryElement element = new CategoryElement();

            Integer id = resultSet.getInt("CTE_ID");
            String name = resultSet.getString("CTE_NAME");
            String type = resultSet.getString("CTE_TYPE");

            element.setId(id);
            element.setName(name);
            element.setType(type);

            elements.add(element);
        }

        return elements;
    }

    private static CategoryInfo getInfo(CallableStatement callableStatement) throws SQLException {
        String name = callableStatement.getString(3);
        Integer id = callableStatement.getInt(4);
        Integer vis = callableStatement.getInt(5);
        Integer rng = callableStatement.getInt(6);

        CategoryInfo categoryInfo = new CategoryInfo();
        categoryInfo.setVisibility(vis == 1);
        categoryInfo.setRange(rng);
        categoryInfo.setId(id);
        categoryInfo.setName(name);

        return categoryInfo;
    }
}
