package Database.Objects;

import RestControllers.Configuration.Data.Spectrum.Spectrum;
import RestControllers.Configuration.Data.Spectrum.SpectrumElement;
import RestControllers.Configuration.Data.Spectrum.SpectrumInfo;
import RestControllers.Configuration.Responses.BaseResponse;
import RestControllers.Configuration.Responses.Spectrum.GetSpectrumListResponse;
import RestControllers.Configuration.Responses.Spectrum.GetSpectrumResponse;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class SpectrumDAO {
    private static final String DELETE_SPECTRUM_SQL = "{CALL delete_spectrum(?,?,?)}";
    private static final String CHANGE_VISIBILITY_SQL = "{CALL change_visibility_spectrum(?,?,?)}";
    private static final String ADD_SPECTRUM_ELEMENT_SQL = "{CALL add_spectrum_element(?,?,?,?)}";
    private static final String ADD_SPECTRUM_SQL = "{CALL add_spectrum(?,?,?,?,?,?)}";
    private static final String GET_SPECTRUMS_LIST_FOR_USER = "{CALL get_spectrums_list_for_user(?)}";
    private static final String GET_SPECTRUM_SQL = "{CALL get_spectrum(?,?,?,?,?,?,?)}";

    public static BaseResponse deleteSpectrum(Connection connection, Integer id, Integer spectrumId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(DELETE_SPECTRUM_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, spectrumId);
            callableStatement.registerOutParameter(3, Types.BOOLEAN);
            resultSet = callableStatement.executeQuery();

            boolean success = callableStatement.getBoolean(3);

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(success);

            return baseResponse;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static BaseResponse changeVisibilitySpectrum(Connection connection, Integer id, Integer spectrumId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(CHANGE_VISIBILITY_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, spectrumId);
            callableStatement.registerOutParameter(3, Types.BOOLEAN);
            resultSet = callableStatement.executeQuery();

            boolean success = callableStatement.getBoolean(3);

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(success);

            return baseResponse;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static BaseResponse modifySpectrum(Connection connection, Integer id, Spectrum spectrum) throws Exception {
        BaseResponse baseResponse = deleteSpectrum(connection, id, spectrum.getInfo().getId());
        if(baseResponse.getSuccess()){
            return addSpectrum(connection, id, spectrum);
        }else{
            return BaseResponse.FailResponse("");
        }
    }

    public static BaseResponse addSpectrum(Connection connection, Integer id, Spectrum spectrum) throws Exception {
        try{
            Integer spectrumId = internalSpectrumAdd(connection, id, spectrum.getInfo());

            if(spectrum.getElements() == null){
                spectrum.setElements(new ArrayList<SpectrumElement>());
            }

            for (SpectrumElement spectrumElement : spectrum.getElements()) {
                addSpectrumElement(connection, id, spectrumElement, spectrumId);
            }

            return BaseResponse.SuccessResponse();
        }catch(Exception e){
            throw e;
        }
    }

    private static void addSpectrumElement(Connection connection, Integer id, SpectrumElement spectrumElement, Integer spectrumId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(ADD_SPECTRUM_ELEMENT_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, spectrumId);
            callableStatement.setString(3, spectrumElement.getName());
            callableStatement.setString(4, spectrumElement.getType());
            resultSet = callableStatement.executeQuery();

        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static Integer internalSpectrumAdd(Connection connection, Integer id, SpectrumInfo info) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(ADD_SPECTRUM_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, info.getRange());
            callableStatement.setString(3, info.getName());
            callableStatement.setBoolean(4, info.getVisibility());
            callableStatement.setString(5, info.getGroupBy());
            callableStatement.registerOutParameter(6, Types.INTEGER);
            resultSet = callableStatement.executeQuery();

            Integer resultId = callableStatement.getInt(6);

            return resultId;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static GetSpectrumListResponse getSpectrumListForUser(Connection connection, Integer id) throws Exception {
        GetSpectrumListResponse response = new GetSpectrumListResponse();
        ArrayList<SpectrumInfo> categories = internalGetSpectrumListForUser(connection, id);
        response.setSuccess(true);
        response.setSpectrums(categories);

        return response;
    }

    private static ArrayList<SpectrumInfo> internalGetSpectrumListForUser(Connection connection, Integer id) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_SPECTRUMS_LIST_FOR_USER);
            callableStatement.setInt(1, id);
            resultSet = callableStatement.executeQuery();

            ArrayList<SpectrumInfo> spectrumInfos = new ArrayList<SpectrumInfo>();

            while(resultSet.next()){
                SpectrumInfo info = new SpectrumInfo();
                Integer ctgId = resultSet.getInt("STG_ID");
                String ctgName = resultSet.getString("STG_NAME");
                Integer ctgVis = resultSet.getInt("STG_VISIBILITY");
                Integer ctgRange = resultSet.getInt("STG_RANGE");
                String groupBy = resultSet.getString("STG_GROUP_BY");
                info.setId(ctgId);
                info.setName(ctgName);
                info.setRange(ctgRange);
                info.setVisibility(ctgVis == 1);
                info.setGroupBy(groupBy);
                spectrumInfos.add(info);
            }

            return spectrumInfos;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    public static GetSpectrumResponse getSpectrum(Connection connection, Integer id, Integer spectrumId) throws Exception {
        GetSpectrumResponse response = new GetSpectrumResponse();
        Spectrum spectrum = internalGetSpectrum(connection, id, spectrumId);
        response.setSuccess(true);
        response.setSpectrum(spectrum);

        return response;
    }

    private static Spectrum internalGetSpectrum(Connection connection, Integer id, Integer spectrumId) throws Exception {
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            callableStatement = connection.prepareCall(GET_SPECTRUM_SQL);
            callableStatement.setInt(1, id);
            callableStatement.setInt(2, spectrumId);
            callableStatement.registerOutParameter(3, Types.VARCHAR);
            callableStatement.registerOutParameter(4, Types.INTEGER);
            callableStatement.registerOutParameter(5, Types.INTEGER);
            callableStatement.registerOutParameter(6, Types.INTEGER);
            callableStatement.registerOutParameter(7, Types.VARCHAR);
            resultSet = callableStatement.executeQuery();

            Spectrum spectrum = new Spectrum();
            SpectrumInfo info = getInfo(callableStatement);
            ArrayList<SpectrumElement> elements = getElements(resultSet);

            spectrum.setInfo(info);
            spectrum.setElements(elements);

            return spectrum;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }
        }
    }

    private static ArrayList<SpectrumElement> getElements(ResultSet resultSet) throws SQLException {
        ArrayList<SpectrumElement> elements = new ArrayList<SpectrumElement>();

        while (resultSet.next()){
            SpectrumElement element = new SpectrumElement();

            Integer id = resultSet.getInt("STE_ID");
            String name = resultSet.getString("STE_NAME");
            String type = resultSet.getString("STE_TYPE");

            element.setId(id);
            element.setName(name);
            element.setType(type);

            elements.add(element);
        }

        return elements;
    }

    private static SpectrumInfo getInfo(CallableStatement callableStatement) throws SQLException {
        String name = callableStatement.getString(3);
        Integer id = callableStatement.getInt(4);
        Integer vis = callableStatement.getInt(5);
        Integer rng = callableStatement.getInt(6);
        String groupBy = callableStatement.getString(7);

        SpectrumInfo spectrumInfo = new SpectrumInfo();
        spectrumInfo.setVisibility(vis == 1);
        spectrumInfo.setRange(rng);
        spectrumInfo.setId(id);
        spectrumInfo.setName(name);
        spectrumInfo.setGroupBy(groupBy);

        return spectrumInfo;
    }
}
