CREATE DATABASE IF NOT EXISTS `configurationdatabase` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE configurationdatabase;

CREATE USER IF NOT EXISTS 'db_client' IDENTIFIED BY 'db_password';
grant usage on *.* to db_client identified by 'db_password';
grant all privileges on configurationdatabase.* to db_client;

USE configurationdatabase;
CREATE TABLE IF NOT EXISTS `browsers` (
  `BRW_ID` INT NOT NULL AUTO_INCREMENT,
  `BRW_NAME` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`BRW_ID`));

USE configurationdatabase;
CREATE TABLE IF NOT EXISTS `application_configurations` (
  `APC_ID` INT NOT NULL AUTO_INCREMENT,
  `APC_LGN_ID` INT NOT NULL,
  `APC_PATH` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`APC_ID`));

USE configurationdatabase;
CREATE TABLE IF NOT EXISTS `webside_configurations` (
  `WBC_ID` INT NOT NULL AUTO_INCREMENT,
  `WBC_LGN_ID` INT NOT NULL,
  `WBC_URL` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`WBC_ID`));

USE configurationdatabase;
CREATE TABLE IF NOT EXISTS `browser_paths` (
  `BRP_ID` INT NOT NULL AUTO_INCREMENT,
  `BRP_LGN_ID` INT NOT NULL,
  `BRP_BRW_ID` INT NOT NULL,
  `BRP_PATH` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`BRP_ID`),
  FOREIGN KEY (`BRP_BRW_ID`) REFERENCES browsers(`BRW_ID`));

USE configurationdatabase;
CREATE TABLE IF NOT EXISTS `categories` (
  `CTG_ID` INT NOT NULL AUTO_INCREMENT,
  `CTG_LGN_ID` INT NOT NULL,
  `CTG_NAME` VARCHAR(1000) NOT NULL,
  `CTG_VISIBILITY` INT NOT NULL,
  `CTG_RANGE` INT NOT NULL,
  PRIMARY KEY (`CTG_ID`));

USE configurationdatabase;
CREATE TABLE IF NOT EXISTS `category_elements` (
  `CTE_ID` INT NOT NULL AUTO_INCREMENT,
  `CTE_LGN_ID` INT NOT NULL,
  `CTE_NAME` VARCHAR(1000) NOT NULL,
  `CTE_TYPE` VARCHAR(100) NOT NULL,
  `CTE_CTG_ID` INT NOT NULL,
  PRIMARY KEY (`CTE_ID`),
  FOREIGN KEY (`CTE_CTG_ID`) REFERENCES categories(`CTG_ID`));

  USE configurationdatabase;
  CREATE TABLE IF NOT EXISTS `spectrums` (
    `STG_ID` INT NOT NULL AUTO_INCREMENT,
    `STG_LGN_ID` INT NOT NULL,
    `STG_NAME` VARCHAR(1000) NOT NULL,
    `STG_VISIBILITY` INT NOT NULL,
    `STG_RANGE` INT NOT NULL,
    `STG_GROUP_BY` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`STG_ID`));

  USE configurationdatabase;
  CREATE TABLE IF NOT EXISTS `spectrum_elements` (
    `STE_ID` INT NOT NULL AUTO_INCREMENT,
    `STE_LGN_ID` INT NOT NULL,
    `STE_NAME` VARCHAR(1000) NOT NULL,
    `STE_TYPE` VARCHAR(100) NOT NULL,
    `STE_STG_ID` INT NOT NULL,
    PRIMARY KEY (`STE_ID`),
    FOREIGN KEY (`STE_STG_ID`) REFERENCES spectrums(`STG_ID`));

    USE configurationdatabase;
    CREATE TABLE IF NOT EXISTS `app_namings` (
      `APN_ID` INT NOT NULL AUTO_INCREMENT,
      `APN_LGN_ID` INT NOT NULL,
      `APN_NAME` VARCHAR(1000) NOT NULL,
      `APN_PATH` VARCHAR(1000) NOT NULL,
      PRIMARY KEY (`APN_ID`));