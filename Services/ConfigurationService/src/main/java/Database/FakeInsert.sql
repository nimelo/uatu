USE configurationdatabase;
INSERT INTO browsers(BRW_NAME) VALUES('iexplore');
INSERT INTO browsers(BRW_NAME) VALUES('chrome');
INSERT INTO browsers(BRW_NAME) VALUES('firefox');

INSERT INTO application_configurations(APC_LGN_ID, APC_PATH) VALUES(1, 'C:\\TEST.exe');
INSERT INTO application_configurations(APC_LGN_ID, APC_PATH) VALUES(1, 'C:\\hello.exe');

INSERT INTO webside_configurations(WBC_LGN_ID, WBC_URL) VALUES(1, 'stackoverflow.com');
INSERT INTO webside_configurations(WBC_LGN_ID, WBC_URL) VALUES(1, 'http://onet.pl');

INSERT INTO browser_paths(BRP_LGN_ID, BRP_BRW_ID, BRP_PATH) VALUES(1, 2, 'sciezka do iexplore');