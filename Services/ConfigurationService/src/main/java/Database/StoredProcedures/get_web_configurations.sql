USE configurationdatabase;

DROP PROCEDURE IF EXISTS get_web_configurations;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_web_configurations(IN IN_LGN_ID INT)
BEGIN
	DECLARE EXIST_ID INT DEFAULT 0;

    SELECT WBC_URL
    FROM webside_configurations
    WHERE WBC_LGN_ID = IN_LGN_ID;

END //
DELIMITER ;