USE configurationdatabase;

DROP PROCEDURE IF EXISTS get_app_configurations;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_app_configurations(IN IN_LGN_ID INT)
BEGIN
	DECLARE EXIST_ID INT DEFAULT 0;

    SELECT APC_PATH
    FROM application_configurations
    WHERE APC_LGN_ID = IN_LGN_ID;

END //
DELIMITER ;