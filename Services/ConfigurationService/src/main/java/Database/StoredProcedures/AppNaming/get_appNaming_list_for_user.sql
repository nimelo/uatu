USE configurationdatabase;

DROP PROCEDURE IF EXISTS get_appNaming_list_for_user;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_appNaming_list_for_user(IN IN_LGN_ID INT)
BEGIN

    SELECT APN_ID, APN_NAME, APN_PATH
    FROM app_namings
    WHERE APN_LGN_ID = IN_LGN_ID;

END //
DELIMITER ;