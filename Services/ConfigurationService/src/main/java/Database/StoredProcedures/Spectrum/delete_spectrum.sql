USE configurationdatabase;

DROP PROCEDURE IF EXISTS delete_spectrum;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE delete_spectrum(IN IN_LGN_ID INT, IN IN_STG_ID INT, OUT OUT_PAR BOOLEAN)
BEGIN

    DECLARE EXIST INT DEFAULT 0;

    SELECT COUNT(*) INTO EXIST FROM spectrums WHERE STG_ID = IN_STG_ID AND STG_LGN_ID = IN_LGN_ID LIMIT 1;

    DELETE FROM spectrum_elements
    WHERE STE_STG_ID = IN_STG_ID;

    DELETE FROM spectrums
    WHERE STG_ID = IN_STG_ID
    AND STG_LGN_ID = IN_LGN_ID;

    SELECT COUNT(*) INTO EXIST FROM spectrums WHERE STG_ID = IN_STG_ID AND STG_LGN_ID = IN_LGN_ID LIMIT 1;

    SET OUT_PAR := EXIST = 0;

END //
DELIMITER ;