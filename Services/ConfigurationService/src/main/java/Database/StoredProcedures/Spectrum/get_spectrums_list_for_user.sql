USE configurationdatabase;

DROP PROCEDURE IF EXISTS get_spectrums_list_for_user;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_spectrums_list_for_user(IN IN_LGN_ID INT)
BEGIN

    SELECT STG_ID, STG_NAME, STG_VISIBILITY, STG_RANGE, STG_GROUP_BY
    FROM spectrums
    WHERE STG_LGN_ID = IN_LGN_ID;

END //
DELIMITER ;