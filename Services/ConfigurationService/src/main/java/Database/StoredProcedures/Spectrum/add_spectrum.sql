USE configurationdatabase;

DROP PROCEDURE IF EXISTS add_spectrum;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE add_spectrum(IN IN_LGN_ID INT, IN IN_STG_RANGE INT, IN IN_STG_NAME VARCHAR(1000), IN IN_STG_VISIBILITY BOOLEAN, IN_STG_GB VARCHAR(100), OUT OUT_ID INT)
BEGIN

    INSERT INTO spectrums(STG_LGN_ID, STG_NAME, STG_VISIBILITY, STG_RANGE, STG_GROUP_BY)
    VALUES(IN_LGN_ID, IN_STG_NAME, IN_STG_VISIBILITY, IN_STG_RANGE, IN_STG_GB);

    SET OUT_ID := LAST_INSERT_ID();

END //
DELIMITER ;