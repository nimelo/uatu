USE configurationdatabase;

DROP PROCEDURE IF EXISTS get_categories_list_for_user;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_categories_list_for_user(IN IN_LGN_ID INT)
BEGIN

    SELECT CTG_ID, CTG_NAME, CTG_VISIBILITY, CTG_RANGE
    FROM categories
    WHERE CTG_LGN_ID = IN_LGN_ID;

END //
DELIMITER ;