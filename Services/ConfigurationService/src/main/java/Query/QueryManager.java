package Query;

import Database.Objects.AppNamingDAO;
import Database.Objects.CategoryDAO;
import Database.Objects.ConfigurationDAO;
import Database.Objects.SpectrumDAO;
import RestControllers.Configuration.Data.AppNaming.AppNamingInfo;
import RestControllers.Configuration.Data.Category.Category;
import RestControllers.Configuration.Data.Spectrum.Spectrum;
import RestControllers.Configuration.Responses.AppNaming.GetAppNamingListResponse;
import RestControllers.Configuration.Responses.BaseResponse;
import RestControllers.Configuration.Responses.Category.GetCategoryListResponse;
import RestControllers.Configuration.Responses.Category.GetCategoryResponse;
import RestControllers.Configuration.Responses.ConfigurationResponse;
import RestControllers.Configuration.Responses.Spectrum.GetSpectrumListResponse;
import RestControllers.Configuration.Responses.Spectrum.GetSpectrumResponse;
import ServiceStaticVariables.Variables;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by mrnim on 22-Jul-16.
 */
public class QueryManager {
    private static QueryManager instance;
    private QueryManager(){

    }

    public static QueryManager getInstance() {
        if(instance == null){
            instance = new QueryManager();
        }
        return instance;
    }

    public ConfigurationResponse handleConfiguration(Integer id) {
        try {
            return ConfigurationDAO.handleConfiguration(id);
        }catch(Exception e){
            return ConfigurationResponse.FailResult("");
        }
    }

    public BaseResponse deleteCategory(Integer id, Integer categoryId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = CategoryDAO.deleteCategory(connection, id, categoryId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse changeVisibilityCategory(Integer id, Integer categoryId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = CategoryDAO.changeVisibilityCategory(connection, id, categoryId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse modifyCategory(Integer id, Category category) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = CategoryDAO.modifyCategory(connection, id, category);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse addCategory(Integer id, Category category) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = CategoryDAO.addCategory(connection, id, category);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public GetCategoryListResponse getCategoryListForUser(Integer id) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            GetCategoryListResponse result = CategoryDAO.getCategoryListForUser(connection, id);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return GetCategoryListResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public GetCategoryResponse getCategory(Integer id, Integer categoryId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            GetCategoryResponse result = CategoryDAO.getCategory(connection, id, categoryId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return GetCategoryResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse deleteSpectrum(Integer id, Integer categoryId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = SpectrumDAO.deleteSpectrum(connection, id, categoryId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse changeVisibilitySpectrum(Integer id, Integer categoryId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = SpectrumDAO.changeVisibilitySpectrum(connection, id, categoryId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse modifySpectrum(Integer id, Spectrum category) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = SpectrumDAO.modifySpectrum(connection, id, category);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse addSpectrum(Integer id, Spectrum category) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = SpectrumDAO.addSpectrum(connection, id, category);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public GetSpectrumListResponse getSpectrumListForUser(Integer id) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            GetSpectrumListResponse result = SpectrumDAO.getSpectrumListForUser(connection, id);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return GetSpectrumListResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public GetSpectrumResponse getSpectrum(Integer id, Integer spectrumId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            GetSpectrumResponse result = SpectrumDAO.getSpectrum(connection, id, spectrumId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return GetSpectrumResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public GetAppNamingListResponse getAppNamingListForUser(Integer id) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            GetAppNamingListResponse result = AppNamingDAO.getAppNamingListForUser(connection, id);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return GetAppNamingListResponse.FailResult("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse addAppNaming(Integer id, AppNamingInfo appNaming) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = AppNamingDAO.addNaming(connection, id, appNaming);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }

    public BaseResponse deleteAppNaming(Integer id, Integer appNamingId) {
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(Variables.getConfigurationDatabaseConfiguration().getMySqlUrl());
            connection.setAutoCommit(false);
            BaseResponse result = AppNamingDAO.deleteAppNaming(connection, id, appNamingId);
            connection.commit();
            return result;
        }catch (Exception e){
            if(connection != null){
                try {
                    connection.rollback();
                } catch (SQLException e1) {

                }
            }
            return BaseResponse.FailResponse("");
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {

            }
        }
    }
}
