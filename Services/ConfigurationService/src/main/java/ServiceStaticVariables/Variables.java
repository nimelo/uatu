package ServiceStaticVariables;

import Database.DatabaseConfiguration;

/**
 * Created by mrnim on 22-Jul-16.
 */
public class Variables {
    private static DatabaseConfiguration configurationDatabaseConfiguration;

    public static void setConfigurationDatabaseConfiguration(DatabaseConfiguration configurationDatabaseConfiguration) {
        Variables.configurationDatabaseConfiguration = configurationDatabaseConfiguration;
    }

    public static DatabaseConfiguration getConfigurationDatabaseConfiguration() {
        return configurationDatabaseConfiguration;
    }
}
