import Database.DatabaseConfiguration;
import ServiceStaticVariables.Variables;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RestController;

import ApplicationContext.ServiceConfiguration;
import ApplicationContext.CurrentContext;

/**
 * Created by mrnim on 21-Jul-16.
 */
@RestController
@ComponentScan(basePackages = "RestControllers")
@EnableAutoConfiguration
public class ConfigurationMain {
    private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName(MYSQL_DRIVER);

        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

        ServiceConfiguration loginConf = (ServiceConfiguration) context.getBean("loginConf");
        ServiceConfiguration authConf = (ServiceConfiguration) context.getBean("authConf");
        Variables.setConfigurationDatabaseConfiguration((DatabaseConfiguration)context.getBean("confDB"));

        CurrentContext.setLoginServiceConfiguration(loginConf);
        CurrentContext.setAuthServiceConfiguration(authConf);

        ApplicationContext ctx = SpringApplication.run(ConfigurationMain.class, args);
    }
}