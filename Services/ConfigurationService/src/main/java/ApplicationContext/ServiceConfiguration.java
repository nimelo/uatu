package ApplicationContext;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class ServiceConfiguration {
    protected String address;
    protected String port;
    protected String path;

    public ServiceConfiguration(String address, String path, String port) {
        this.address = address;
        this.path = path;
        this.port = port;
    }

    public ServiceConfiguration() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getURL(){
        return String.format("%s:%s/%s", this.address, this.port, this.path);
    }

}
