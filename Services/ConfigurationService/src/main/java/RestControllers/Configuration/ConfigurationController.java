package RestControllers.Configuration;

import LoginCheckers.LoginChecker;
import LoginCheckers.LoginResponse;
import Query.QueryManager;
import RestControllers.Configuration.Requests.AppNaming.AddAppNamingRequest;
import RestControllers.Configuration.Requests.AppNaming.DeleteAppNamingRequest;
import RestControllers.Configuration.Requests.BaseRequest;
import RestControllers.Configuration.Requests.Category.*;
import RestControllers.Configuration.Requests.Spectrum.*;
import RestControllers.Configuration.Responses.AppNaming.GetAppNamingListResponse;
import RestControllers.Configuration.Responses.BaseResponse;
import RestControllers.Configuration.Responses.Category.*;
import RestControllers.Configuration.Responses.Spectrum.GetSpectrumListResponse;
import RestControllers.Configuration.Responses.Spectrum.GetSpectrumResponse;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import RestControllers.Configuration.Responses.ConfigurationResponse;
import RestControllers.Configuration.Requests.ConfigurationRequest;
/**
 * Created by mrnim on 22-Jul-16.
 */
@RestController
@Controller
@EnableAutoConfiguration
@RequestMapping("/configuration")
public class ConfigurationController {

    @RequestMapping(method = RequestMethod.POST)
    public ConfigurationResponse processConfiguration(@RequestBody ConfigurationRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId(ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().handleConfiguration(response.getId());
        }
        return ConfigurationResponse.FailResult("");
    }

    @RequestMapping(value = "/category/getList", method = RequestMethod.POST)
    public GetCategoryListResponse getCategoryListForUser(@RequestBody BaseRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().getCategoryListForUser(response.getId());
        }

        return GetCategoryListResponse.FailResult("");
    }

    @RequestMapping(value = "/category/get", method = RequestMethod.POST)
    public GetCategoryResponse getCategory(@RequestBody GetConfigurationRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().getCategory(response.getId(), ctx.getCategoryId());
        }

        return GetCategoryResponse.FailResult("");
    }

    @RequestMapping(value = "/category/add", method = RequestMethod.POST)
    public BaseResponse addCategory(@RequestBody AddCategoryRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().addCategory(response.getId(), ctx.getCategory());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/category/delete", method = RequestMethod.POST)
    public BaseResponse deleteCategory(@RequestBody DeleteCategoryRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().deleteCategory(response.getId(), ctx.getCategoryId());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/category/modify", method = RequestMethod.POST)
    public BaseResponse modifyCategory(@RequestBody ModifyCategoryRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().modifyCategory(response.getId(), ctx.getCategory());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/category/changeVisibility", method = RequestMethod.POST)
    public BaseResponse changeCategoryVisibility(@RequestBody ChangeVisibilityRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().changeVisibilityCategory(response.getId(), ctx.getCategoryId());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/spectrum/getList", method = RequestMethod.POST)
    public GetSpectrumListResponse getSpectrumListForUser(@RequestBody BaseRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().getSpectrumListForUser(response.getId());
        }

        return GetSpectrumListResponse.FailResult("");
    }

    @RequestMapping(value = "/spectrum/get", method = RequestMethod.POST)
    public GetSpectrumResponse getSpectrum(@RequestBody GetConfigurationSpectrumRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().getSpectrum(response.getId(), ctx.getSpectrumId());
        }

        return GetSpectrumResponse.FailResult("");
    }

    @RequestMapping(value = "/spectrum/add", method = RequestMethod.POST)
    public BaseResponse addSpectrum(@RequestBody AddSpectrumRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().addSpectrum(response.getId(), ctx.getSpectrum());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/spectrum/delete", method = RequestMethod.POST)
    public BaseResponse deleteSpectrum(@RequestBody DeleteSpectrumRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().deleteSpectrum(response.getId(), ctx.getSpectrumId());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/spectrum/modify", method = RequestMethod.POST)
    public BaseResponse modifySpectrum(@RequestBody ModifySpectrumRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().modifySpectrum(response.getId(), ctx.getSpectrum());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/spectrum/changeVisibility", method = RequestMethod.POST)
    public BaseResponse changeSpectrumVisibility(@RequestBody ChangeVisibilitySpectrumRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().changeVisibilitySpectrum(response.getId(), ctx.getSpectrumId());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/appNaming/getList", method = RequestMethod.POST)
    public GetAppNamingListResponse getAppNamingListForUser(@RequestBody BaseRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().getAppNamingListForUser(response.getId());
        }

        return GetAppNamingListResponse.FailResult("");
    }

    @RequestMapping(value = "/appNaming/add", method = RequestMethod.POST)
    public BaseResponse addAppNaming(@RequestBody AddAppNamingRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().addAppNaming(response.getId(), ctx.getAppNaming());
        }
        return BaseResponse.FailResponse("");
    }

    @RequestMapping(value = "/appNaming/delete", method = RequestMethod.POST)
    public BaseResponse deleteAppNaming(@RequestBody DeleteAppNamingRequest ctx) {
        LoginResponse response = LoginChecker.getInstance().getId((BaseRequest) ctx);
        if (response.getSuccess()) {
            return QueryManager.getInstance().deleteAppNaming(response.getId(), ctx.getAppNamingId());
        }
        return BaseResponse.FailResponse("");
    }
}
