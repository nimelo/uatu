package RestControllers.Configuration.Responses.Category;

import RestControllers.Configuration.Data.Category.Category;
import RestControllers.Configuration.Responses.BaseResponse;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class GetCategoryResponse extends BaseResponse{
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static GetCategoryResponse FailResult(String msg){
        GetCategoryResponse getCategoryResponse = new GetCategoryResponse();
        getCategoryResponse.setSuccess(false);
        getCategoryResponse.setErrorMessage(msg);
        return getCategoryResponse;
    }
}
