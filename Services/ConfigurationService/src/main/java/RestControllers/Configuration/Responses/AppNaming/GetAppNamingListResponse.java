package RestControllers.Configuration.Responses.AppNaming;

import RestControllers.Configuration.Data.AppNaming.AppNamingInfo;
import RestControllers.Configuration.Responses.BaseResponse;

import java.util.ArrayList;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class GetAppNamingListResponse extends BaseResponse {
    private ArrayList<AppNamingInfo> appNamings;

    public ArrayList<AppNamingInfo> getAppNamings() {
        return appNamings;
    }

    public void setAppNamings(ArrayList<AppNamingInfo> appNamings) {
        this.appNamings = appNamings;
    }

    public static GetAppNamingListResponse FailResult(String msg){
        GetAppNamingListResponse getCategoryListResponse = new GetAppNamingListResponse();
        getCategoryListResponse.setSuccess(false);
        getCategoryListResponse.setErrorMessage(msg);
        return getCategoryListResponse;
    }
}
