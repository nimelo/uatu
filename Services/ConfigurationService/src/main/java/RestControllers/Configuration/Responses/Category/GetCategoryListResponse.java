package RestControllers.Configuration.Responses.Category;

import RestControllers.Configuration.Data.Category.CategoryInfo;
import RestControllers.Configuration.Responses.BaseResponse;

import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class GetCategoryListResponse extends BaseResponse{
    private ArrayList<CategoryInfo> categories;

    public ArrayList<CategoryInfo> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryInfo> categories) {
        this.categories = categories;
    }

    public static GetCategoryListResponse FailResult(String msg){
        GetCategoryListResponse getCategoryListResponse = new GetCategoryListResponse();
        getCategoryListResponse.setSuccess(false);
        getCategoryListResponse.setErrorMessage(msg);
        return getCategoryListResponse;
    }
}
