package RestControllers.Configuration.Responses.Spectrum;

import RestControllers.Configuration.Data.Spectrum.SpectrumInfo;
import RestControllers.Configuration.Responses.BaseResponse;

import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class GetSpectrumListResponse extends BaseResponse{
    private ArrayList<SpectrumInfo> spectrums;

    public ArrayList<SpectrumInfo> getSpectrums() {
        return spectrums;
    }

    public void setSpectrums(ArrayList<SpectrumInfo> spectrums) {
        this.spectrums = spectrums;
    }

    public static GetSpectrumListResponse FailResult(String msg){
        GetSpectrumListResponse getSpectrumListResponse = new GetSpectrumListResponse();
        getSpectrumListResponse.setSuccess(false);
        getSpectrumListResponse.setErrorMessage(msg);
        return getSpectrumListResponse;
    }
}
