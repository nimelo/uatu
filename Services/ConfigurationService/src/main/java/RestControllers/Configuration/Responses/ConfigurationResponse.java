package RestControllers.Configuration.Responses;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mrnim on 22-Jul-16.
 */
public class ConfigurationResponse {
    private Boolean success;
    private String errorMessage;
    private ArrayList<String> appExceptions;
    private ArrayList<String> webExceptions;
    private HashMap<String, String> browsersExePaths;

    public ArrayList<String> getAppExceptions() {
        return appExceptions;
    }

    public void setAppExceptions(ArrayList<String> appExceptions) {
        this.appExceptions = appExceptions;
    }

    public HashMap<String, String> getBrowsersExePaths() {
        return browsersExePaths;
    }

    public void setBrowsersExePaths(HashMap<String, String> browsersExePaths) {
        this.browsersExePaths = browsersExePaths;
    }

    public ArrayList<String> getWebExceptions() {
        return webExceptions;
    }

    public void setWebExceptions(ArrayList<String> webExceptions) {
        this.webExceptions = webExceptions;
    }

    public ConfigurationResponse() {
        this.setSuccess(false);
    }

    public ConfigurationResponse(String errorMessage, Boolean success) {
        this.errorMessage = errorMessage;
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static ConfigurationResponse FailResult(String s) {
        return new ConfigurationResponse(s, false);
    }

    public static ConfigurationResponse SuccessResult() {
        return new ConfigurationResponse("", true);
    }
}
