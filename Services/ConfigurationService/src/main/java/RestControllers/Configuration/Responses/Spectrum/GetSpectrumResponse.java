package RestControllers.Configuration.Responses.Spectrum;

import RestControllers.Configuration.Data.Spectrum.Spectrum;
import RestControllers.Configuration.Responses.BaseResponse;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class GetSpectrumResponse extends BaseResponse{
    private Spectrum spectrum;

    public Spectrum getSpectrum() {
        return spectrum;
    }

    public void setSpectrum(Spectrum spectrum) {
        this.spectrum = spectrum;
    }

    public static GetSpectrumResponse FailResult(String msg){
        GetSpectrumResponse getSpectrumResponse = new GetSpectrumResponse();
        getSpectrumResponse.setSuccess(false);
        getSpectrumResponse.setErrorMessage(msg);
        return getSpectrumResponse;
    }
}
