package RestControllers.Configuration.Responses;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class BaseResponse {
    private Boolean success;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static BaseResponse FailResponse(String msg){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setSuccess(false);
        baseResponse.setErrorMessage(msg);
        return baseResponse;
    }

    public static BaseResponse SuccessResponse(){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setSuccess(true);
        return baseResponse;
    }
}
