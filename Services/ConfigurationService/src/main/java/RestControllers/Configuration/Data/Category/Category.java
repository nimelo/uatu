package RestControllers.Configuration.Data.Category;

import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class Category {

    private CategoryInfo info;

    public CategoryInfo getInfo() {
        return info;
    }

    public void setInfo(CategoryInfo info) {
        this.info = info;
    }

    private ArrayList<CategoryElement> elements;

    public ArrayList<CategoryElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<CategoryElement> elements) {
        this.elements = elements;
    }
}
