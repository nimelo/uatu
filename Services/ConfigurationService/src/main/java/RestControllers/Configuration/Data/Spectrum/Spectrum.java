package RestControllers.Configuration.Data.Spectrum;

import java.util.ArrayList;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class Spectrum {

    private SpectrumInfo info;

    public SpectrumInfo getInfo() {
        return info;
    }

    public void setInfo(SpectrumInfo info) {
        this.info = info;
    }

    private ArrayList<SpectrumElement> elements;

    public ArrayList<SpectrumElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<SpectrumElement> elements) {
        this.elements = elements;
    }
}
