package RestControllers.Configuration.Requests.Category;

import RestControllers.Configuration.Data.Category.Category;
import RestControllers.Configuration.Requests.BaseRequest;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class ModifyCategoryRequest extends BaseRequest{
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
