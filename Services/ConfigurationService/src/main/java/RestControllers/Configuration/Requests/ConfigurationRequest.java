package RestControllers.Configuration.Requests;

/**
 * Created by mrnim on 22-Jul-16.
 */
public class ConfigurationRequest {
    private String login;
    private String appKey;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
