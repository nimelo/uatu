package RestControllers.Configuration.Requests.Spectrum;

import RestControllers.Configuration.Requests.BaseRequest;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class ChangeVisibilitySpectrumRequest extends BaseRequest {
    private Integer spectrumId;

    public Integer getSpectrumId() {
        return spectrumId;
    }

    public void setSpectrumId(Integer spectrumId) {
        this.spectrumId = spectrumId;
    }
}
