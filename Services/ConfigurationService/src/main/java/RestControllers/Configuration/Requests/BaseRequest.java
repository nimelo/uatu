package RestControllers.Configuration.Requests;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class BaseRequest {
    private String key;
    private Long date;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
