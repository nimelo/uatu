package RestControllers.Configuration.Requests.Spectrum;

import RestControllers.Configuration.Data.Category.Category;
import RestControllers.Configuration.Data.Spectrum.Spectrum;
import RestControllers.Configuration.Requests.BaseRequest;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class AddSpectrumRequest extends BaseRequest{
    private Spectrum spectrum;

    public Spectrum getSpectrum() {
        return spectrum;
    }

    public void setSpectrum(Spectrum spectrum) {
        this.spectrum = spectrum;
    }
}
