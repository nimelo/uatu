package RestControllers.Configuration.Requests.Category;

import RestControllers.Configuration.Requests.BaseRequest;

/**
 * Created by mrnim on 25-Jul-16.
 */
public class ChangeVisibilityRequest extends BaseRequest {
    private Integer categoryId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
