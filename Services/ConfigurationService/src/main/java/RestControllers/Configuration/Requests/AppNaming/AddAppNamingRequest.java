package RestControllers.Configuration.Requests.AppNaming;

import RestControllers.Configuration.Data.AppNaming.AppNamingInfo;
import RestControllers.Configuration.Requests.BaseRequest;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class AddAppNamingRequest extends BaseRequest {
    private AppNamingInfo appNaming;

    public AppNamingInfo getAppNaming() {
        return appNaming;
    }

    public void setAppNaming(AppNamingInfo appNaming) {
        this.appNaming = appNaming;
    }
}
