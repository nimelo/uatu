package RestControllers.Configuration.Requests.AppNaming;

import RestControllers.Configuration.Requests.BaseRequest;

/**
 * Created by mrnim on 27-Jul-16.
 */
public class DeleteAppNamingRequest extends BaseRequest {
    private Integer appNamingId;

    public Integer getAppNamingId() {
        return appNamingId;
    }

    public void setAppNamingId(Integer appNamingId) {
        this.appNamingId = appNamingId;
    }
}
