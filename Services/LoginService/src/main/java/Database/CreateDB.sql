CREATE DATABASE IF NOT EXISTS `logindatabase` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE logindatabase;
CREATE USER IF NOT EXISTS'db_client'IDENTIFIED BY 'db_password';
grant usage on *.* to db_client identified by 'db_password';
grant all privileges on logindatabase.* to db_client;

USE logindatabase;
CREATE TABLE IF NOT EXISTS `logins` (
  `LGN_ID` INT NOT NULL AUTO_INCREMENT,
  `LGN_LOGIN` VARCHAR(100) NOT NULL,
  `LGN_PASSWORD` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`LGN_ID`));

USE logindatabase;
CREATE TABLE IF NOT EXISTS `appkeys` (
  `APK_ID` INT NOT NULL AUTO_INCREMENT,
  `APK_KEY` VARCHAR(100) NOT NULL,
  `APK_NAME` VARCHAR(100) NOT NULL,
  `APK_LGN_ID` INT NOT NULL,
  PRIMARY KEY (`APK_ID`),
  FOREIGN KEY (`APK_LGN_ID`) REFERENCES logins(`LGN_ID`));