package Database;

import Database.Objects.LoginResult;
import Database.Objects.LoginResultId;
import Helpers.HashUtils;
import Helpers.WebLoginKey;
import RestControllers.Authentication.Responses.CheckCookiesResult;
import RestControllers.Authentication.Responses.LoginWebResult;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class DatabaseManager {
    private static DatabaseManager instance;

    public static DatabaseManager getInstance(){
        if(instance == null){
            instance = new DatabaseManager();
        }

        return instance;
    }

    public LoginResult LoginByPassword(String login, String password){
        try {
            String hash = HashUtils.SHA256(password);
            return DatabaseQueryInvoker.check_login_by_password(login, hash);
        } catch (Exception e) {
            return LoginResult.FailResult();
        }

    }

    public LoginResult LoginByAppKey(String login, String appKey){
        try {
            return DatabaseQueryInvoker.check_login_by_appkey(login, appKey);
        } catch (Exception e) {
            return LoginResult.FailResult();
        }
    }

    public LoginResultId LoginByAppKeyId(String login, String appKey){
        try {
            return DatabaseQueryInvoker.check_login_by_appkey_id(login, appKey);
        } catch (Exception e) {
            return LoginResultId.FailResult();
        }
    }

    public CheckCookiesResult checkCookies(Long date, String key) {
        try{
            String decrypted = HashUtils.decrypt(key);
            WebLoginKey cookieKey = WebLoginKey.fromString(decrypted);
            if(date.equals(cookieKey.getDate())){
                CheckCookiesResult result = new CheckCookiesResult();
                boolean success = DatabaseQueryInvoker.check_id_pass(cookieKey.getId(), cookieKey.getPassword());

                result.setSuccess(success);
                return result;
            }
        }catch (Exception e){

        }

        return CheckCookiesResult.FailResult();
    }

    public LoginResultId checkCookiesGetId(Long date, String key) {
        try{
            String decrypted = HashUtils.decrypt(key);
            WebLoginKey cookieKey = WebLoginKey.fromString(decrypted);
            if(date.equals(cookieKey.getDate())){
                LoginResultId result = new LoginResultId();
                boolean success = DatabaseQueryInvoker.check_id_pass(cookieKey.getId(), cookieKey.getPassword());

                result.setSuccess(success);
                result.setId(cookieKey.getId());
                return result;
            }
        }catch (Exception e){

        }

        return LoginResultId.FailResult();
    }

    public LoginWebResult webLogin(String login, String password) {
        LoginWebResult result = new LoginWebResult();
        try{
            String hash = HashUtils.SHA256(password);
            LoginResult loginResult = LoginByPassword(login, password);
            if(loginResult.getSuccess()){
                LoginResultId loginResultId = LoginByAppKeyId(loginResult.getLogin(), loginResult.getAppKey());
                if(loginResultId.getSuccess()){
                    Long date = Calendar.getInstance().getTimeInMillis();
                    String key = HashUtils.encrypt(new WebLoginKey(loginResultId.getId(), date, hash).toString());

                    result.setSuccess(true);
                    result.setDate(String.valueOf(date));
                    result.setKey(key);
                    result.setName(loginResult.getLogin());
                    return result;
                }
            }
        }catch(Exception e){

        }
        return LoginWebResult.FailResult();
    }
}
