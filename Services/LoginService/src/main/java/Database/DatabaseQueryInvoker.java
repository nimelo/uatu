package Database;

import Database.Objects.LoginResult;
import Database.Objects.LoginResultId;
import Service.StaticServiceVariables;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.*;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class DatabaseQueryInvoker {

    private static String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
    private static String CHECK_LOGIN_BY_APPKEY_ST = "{CALL check_login_by_appkey(?,?)}";
    private static String CHECK_LOGIN_BY_PASSWORD_ST = "{CALL check_login_by_password(?,?)}";
    private static String CHECK_LOGIN_BY_APPKEY_ID_ST = "{CALL check_login_by_appkey_id(?,?)}";
    private static String CHECK_ID_PASS = "{CALL check_id_pass(?,?,?)}";

    private static String LGN_LOGIN = "LGN_LOGIN";
    private static String APK_KEY = "APK_KEY";
    private static String APK_ID = "APK_ID";

    public static LoginResult check_login_by_appkey(String login, String appkey) throws Exception {
        return executeStatement(login, appkey, CHECK_LOGIN_BY_APPKEY_ST);
    }

    public static LoginResult check_login_by_password(String login, String password) throws Exception {
        return executeStatement(login, password, CHECK_LOGIN_BY_PASSWORD_ST);
    }

    private static LoginResult executeStatement(String arg1, String arg2, String sql) throws Exception {
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            Class.forName(MYSQL_DRIVER);

            connection = DriverManager.getConnection(StaticServiceVariables.getDatabaseConfiguration().getMySqlUrl());
            callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, arg1);
            callableStatement.setString(2, arg2);
            resultSet = callableStatement.executeQuery();

            LoginResult loginResult = new LoginResult();
            while(resultSet.next()){
                String login = resultSet.getString(LGN_LOGIN);
                String appkey = resultSet.getString(APK_KEY);
                loginResult.setSuccess(true);
                loginResult.setAppKey(appkey);
                loginResult.setLogin(login);
            }

            return loginResult;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }

            if(connection != null){
                connection.close();
            }
        }
    }

    public static LoginResultId check_login_by_appkey_id(String login, String appKey) throws Exception {
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            Class.forName(MYSQL_DRIVER);

            connection = DriverManager.getConnection(StaticServiceVariables.getDatabaseConfiguration().getMySqlUrl());
            callableStatement = connection.prepareCall(CHECK_LOGIN_BY_APPKEY_ID_ST);
            callableStatement.setString(1, login);
            callableStatement.setString(2, appKey);
            resultSet = callableStatement.executeQuery();

            LoginResultId loginResult = new LoginResultId();
            while(resultSet.next()){
                Integer id = resultSet.getInt(APK_ID);
                if(id != null){
                    loginResult.setSuccess(true);
                    loginResult.setId(id);
                }
            }

            return loginResult;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }

            if(connection != null){
                connection.close();
            }
        }
    }

    public static boolean check_id_pass(Integer id, String password) throws Exception {
        Connection connection = null;
        CallableStatement callableStatement = null;
        ResultSet resultSet = null;
        try{
            Class.forName(MYSQL_DRIVER);

            connection = DriverManager.getConnection(StaticServiceVariables.getDatabaseConfiguration().getMySqlUrl());
            callableStatement = connection.prepareCall(CHECK_ID_PASS);
            callableStatement.setInt(1, id);
            callableStatement.setString(2, password);
            callableStatement.registerOutParameter(3, Types.BOOLEAN);
            resultSet = callableStatement.executeQuery();

            boolean success = callableStatement.getBoolean(3);

            return success;
        }catch (Exception e){
            throw e;
        }
        finally {
            if(resultSet != null){
                resultSet.close();
            }

            if(callableStatement != null){
                callableStatement.close();
            }

            if(connection != null){
                connection.close();
            }
        }
    }
}
