USE logindatabase;

DROP PROCEDURE IF EXISTS check_login_by_password;
DELIMITER ;

DELIMITER //
CREATE PROCEDURE check_login_by_password(IN IN_LOGIN CHAR(100),
											IN IN_PASS CHAR(100))
BEGIN

	SELECT LGN_LOGIN, APK_KEY
    FROM logins JOIN appkeys
    WHERE LGN_LOGIN = IN_LOGIN
    AND LGN_PASSWORD = IN_PASS
    LIMIT 1;

END //
DELIMITER ;