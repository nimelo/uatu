package Database.Objects;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class LoginResultId {
    private Integer id;
    private Boolean success;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public LoginResultId(Integer id, Boolean success) {

        this.id = id;
        this.success = success;
    }

    public LoginResultId() {
        this.setSuccess(false);
    }

    public static LoginResultId FailResult(){
        LoginResultId loginResultId = new LoginResultId();
        loginResultId.setSuccess(false);
        return loginResultId;
    }
}
