package Database.Objects;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class LoginResult {
    private Boolean isSuccess;
    private String appKey;
    private String login;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public static LoginResult FailResult() {
        LoginResult loginResult = new LoginResult();
        loginResult.setSuccess(false);
        return loginResult;
    }

    public LoginResult() {
        this.isSuccess = false;
    }
}
