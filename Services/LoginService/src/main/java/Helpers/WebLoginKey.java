package Helpers;

/**
 * Created by mrnim on 24-Jul-16.
 */
public class WebLoginKey {
    private Integer id;
    private Long date;
    private String password;

    private static String SEPARATOR = "_";

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public WebLoginKey(Integer id, Long date, String password) {
        this.date = date;
        this.id = id;
        this.password = password;
    }

    @Override
    public String toString() {
        return String.format("%d_%d_%s", this.id, this.date, this.password);
    }

    public static WebLoginKey fromString(String value){
        String[] strings = value.split(SEPARATOR, 3);
        Integer id = Integer.valueOf(strings[0]);
        Long date = Long.valueOf(strings[1]);
        String password = strings[2];

        return new WebLoginKey(id, date, password);
    }
}
