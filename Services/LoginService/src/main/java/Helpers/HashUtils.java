package Helpers;

import java.security.MessageDigest;

/**
 * Created by mrnim on 24-Jul-16.
 */
public class HashUtils {
    private static String encryptionKey = "!@#$%^&*()_+1234";

    public static String SHA256(String value) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        String text = "This is some text";

        md.update(text.getBytes("UTF-8"));
        byte[] digest = md.digest();

        return String.format("%064x", new java.math.BigInteger(1, digest));
    }

    public static String SHA256(String lhs, String middle, Long rhs) throws Exception {
        return SHA256(lhs + middle + rhs);
    }

    public static String encrypt(String value) throws Exception{
        return new AdvancedEncryptionStandard(encryptionKey).encrypt(value);
    }

    public static String decrypt(String value) throws Exception{
        return new AdvancedEncryptionStandard(encryptionKey).decrypt(value);
    }

}
