import Database.DatabaseConfiguration;
import Service.StaticServiceVariables;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by mrnim on 15-Jul-16.
 */
@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages = "RestControllers")
public class LoginMain {

    @RequestMapping("/")
    public String home(){
        return "Hello World!";
    }

    public static void main(String[] args){
        Properties properties = new Properties();
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            properties.load(loader.getResourceAsStream("application.properties"));
            String address = properties.getProperty("db.address");
            String port = properties.getProperty("db.port");
            String user = properties.getProperty("db.user");
            String password = properties.getProperty("db.password");
            String dbName = properties.getProperty("db.dbName");

            StaticServiceVariables.setDatabaseConfiguration(new DatabaseConfiguration(address, dbName, password, port, user));
        } catch (IOException e) {
           System.out.println("DB Configuration not found!");
        }

        SpringApplication.run(LoginMain.class, args);
    }
}
