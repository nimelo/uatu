package RestControllers.Logining.Responses;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class LoginResponse {
    private Boolean isSuccess;
    private String login;
    private String appKey;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public LoginResponse(String appKey, Boolean isSuccess, String login) {
        this.appKey = appKey;
        this.isSuccess = isSuccess;
        this.login = login;
    }

    public LoginResponse() {
    }

    public static LoginResponse FailResponse(){
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setSuccess(false);
        return loginResponse;
    }
}
