package RestControllers.Logining.Responses;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class LoginResponseId {
    private Boolean success;
    private Integer id;

    public LoginResponseId(Boolean success, Integer id) {
        this.success = success;
        this.id = id;
    }

    public LoginResponseId() {
        this.success = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static LoginResponseId FailResponse() {
        return new LoginResponseId();
    }
}
