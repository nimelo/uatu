package RestControllers.Logining;

import Database.DatabaseManager;
import Database.Objects.LoginResult;
import Database.Objects.LoginResultId;
import RestControllers.Logining.Data.LoginCredentials;
import RestControllers.Logining.Responses.LoginResponse;
import RestControllers.Logining.Responses.LoginResponseId;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mrnim on 18-Jul-16.
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @RequestMapping(path = "",method = RequestMethod.POST)
    public LoginResponse processLogin(@RequestBody LoginCredentials credentials){
        if(credentials != null){
            LoginResult result = LoginResult.FailResult();

            if(credentials.isAppKey()){
                result = DatabaseManager.getInstance().LoginByAppKey(credentials.getLogin(), credentials.getAppKey());
            }else if(credentials.isPassword()){
                result = DatabaseManager.getInstance().LoginByPassword(credentials.getLogin(), credentials.getPassword());
            }

            if(result.getSuccess()){
                return new LoginResponse(result.getAppKey(), true, result.getLogin());
            }
        }

        return LoginResponse.FailResponse();
    }

    @RequestMapping(path = "/id",method = RequestMethod.POST)
    public LoginResponseId processLoginId(@RequestBody LoginCredentials credentials){
        if(credentials != null){
            LoginResultId result = LoginResultId.FailResult();

            if(credentials.isAppKey()){
                result = DatabaseManager.getInstance().LoginByAppKeyId(credentials.getLogin(), credentials.getAppKey());
            }

            if(result.getSuccess()){
                return new LoginResponseId(true, result.getId());
            }
        }

        return LoginResponseId.FailResponse();
    }

}

