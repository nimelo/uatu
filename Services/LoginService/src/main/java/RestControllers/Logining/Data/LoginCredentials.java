package RestControllers.Logining.Data;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class LoginCredentials {
    private String login;
    private String appKey;
    private String password;

    public Boolean isPassword(){
        return this.password != null;
    }

    public Boolean isAppKey(){
        return this.appKey != null;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
