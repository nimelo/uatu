package RestControllers.Authentication;

import Database.DatabaseManager;

import Database.DatabaseManager;
import Database.Objects.LoginResult;
import Database.Objects.LoginResultId;
import RestControllers.Authentication.Requests.CheckCookiesRequest;
import RestControllers.Authentication.Responses.CheckCookiesResult;
import RestControllers.Authentication.Responses.LoginWebResult;
import RestControllers.Logining.Data.LoginCredentials;
import RestControllers.Logining.Responses.LoginResponse;
import RestControllers.Logining.Responses.LoginResponseId;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by mrnim on 18-Jul-16.
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public CheckCookiesResult checkCookies(@RequestBody CheckCookiesRequest req){
        if(req != null){
            CheckCookiesResult result = CheckCookiesResult.FailResult();

            if(!req.isRequestNull()) {
                result = DatabaseManager.getInstance().checkCookies(req.getDate(), req.getKey());
            }

            if(result.getSuccess()){
                return result;
            }
        }

        return CheckCookiesResult.FailResult();
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginWebResult login(@RequestBody LoginCredentials credentials){
        if(credentials != null){
            LoginWebResult result = LoginWebResult.FailResult();

            if(credentials.isPassword()){
                result = DatabaseManager.getInstance().webLogin(credentials.getLogin(), credentials.getPassword());
            }

            if(result.getSuccess()){
                return result;
            }
        }

        return LoginWebResult.FailResult();
    }

    @RequestMapping(value = "/id", method = RequestMethod.POST)
    public LoginResponseId id(@RequestBody CheckCookiesRequest req){
        if(req != null){
            LoginResponseId result = LoginResponseId.FailResponse();

            if(!req.isRequestNull()) {
                LoginResultId loginResultId = DatabaseManager.getInstance().checkCookiesGetId(req.getDate(), req.getKey());
                if(loginResultId.getSuccess()){
                    result.setSuccess(true);
                    result.setId(loginResultId.getId());
                    return result;
                }
            }

        }

        return LoginResponseId.FailResponse();
    }

}

