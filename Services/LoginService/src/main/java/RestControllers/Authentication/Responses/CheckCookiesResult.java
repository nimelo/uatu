package RestControllers.Authentication.Responses;

/**
 * Created by mrnim on 24-Jul-16.
 */
public class CheckCookiesResult {
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static CheckCookiesResult FailResult() {
        CheckCookiesResult result = new CheckCookiesResult();
        result.setSuccess(false);
        return result;
    }
}
