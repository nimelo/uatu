package RestControllers.Authentication.Responses;

/**
 * Created by mrnim on 24-Jul-16.
 */
public class LoginWebResult {
    private String name;
    private Boolean success;
    private String key;
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public static LoginWebResult FailResult() {
        LoginWebResult result = new LoginWebResult();
        result.setSuccess(false);
        return result;
    }
}
