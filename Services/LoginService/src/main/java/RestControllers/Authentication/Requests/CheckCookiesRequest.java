package RestControllers.Authentication.Requests;

/**
 * Created by mrnim on 24-Jul-16.
 */
public class CheckCookiesRequest {
    private Long date;
    private String key;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean isRequestNull(){
        return this.date == null || this.key == null;
    }
}
