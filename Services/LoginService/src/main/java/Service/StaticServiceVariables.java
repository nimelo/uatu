package Service;

import Database.DatabaseConfiguration;

/**
 * Created by mrnim on 19-Jul-16.
 */
public class StaticServiceVariables {
    private static DatabaseConfiguration databaseConfiguration;

    public static DatabaseConfiguration getDatabaseConfiguration() {
        return databaseConfiguration;
    }

    public static void setDatabaseConfiguration(DatabaseConfiguration databaseConfiguration) {
        StaticServiceVariables.databaseConfiguration = databaseConfiguration;
    }
}
