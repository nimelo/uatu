package Data.Credentials;

/**
 * Created by mrnim on 17-Jul-16.
 */
public class Credentials {
    private String login;
    private String appKey;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Credentials() {
    }

    public Credentials(String appKey, String login) {

        this.appKey = appKey;
        this.login = login;
    }
}
