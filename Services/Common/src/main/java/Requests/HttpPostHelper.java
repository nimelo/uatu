package Requests;



import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class HttpPostHelper {
    public static String Post(String url, String json) throws Exception {
        return Post(url, json, "UTF-8");
    }

    public static String Post(String url, String json, String charset) throws Exception{
        final String http = "http://";
        URLConnection connection = new URL(http + url).openConnection();
        connection.setDoOutput(true);
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", "application/json;charset=" + charset);

        OutputStream output = connection.getOutputStream();
        output.write(json.getBytes(charset));

        InputStream response = connection.getInputStream();
        String toReturn = IOUtils.toString(response, charset);
        output.close();
        response.close();

        return toReturn;
    }
}
