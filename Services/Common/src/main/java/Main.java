import Data.Credentials.Credentials;
import Requests.HttpPostHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.StringReader;

/**
 * Created by mrnim on 18-Jul-16.
 */
public class Main {
    public static void main(String [] args){
        try {
            String json = "{\n" +
                    "\t\"secretString\": \"!@#$%\",\n" +
                    "      \"query\" :{\"serviceName\" : \"logi2n\"}\n" +
                    "\t\n" +
                    "}";
            String test = HttpPostHelper.Post("192.168.1.100:9000/resolve", json);


            Gson gson = new Gson();
            String crdStr = "{'login':'l','appKey':'ak'}";
            Credentials crd = gson.fromJson(new StringReader(crdStr), Credentials.class);

            System.out.print(test);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
