chrome.tabs.onCreated.addListener(OnTabCreated);
chrome.tabs.onUpdated.addListener(OnTabUpdated);
chrome.tabs.onRemoved.addListener(OnTabRemoved);
chrome.tabs.onActivated.addListener(OnTabActivated);

chrome.windows.onFocusChanged.addListener(OnWindowFocusChanged);
chrome.windows.onCreated.addListener(OnWindowCreated);
chrome.windows.onRemoved.addListener(OnWindowRemoved);

/*OBJECTS*/
db = undefined;
currentObject = new ObjectWithSetMemory();
var db = openDB();
createTablesIfNotExist();

/*FUNCTIONS*/
function getFocuesWindowId(callback){
   chrome.windows.getAll({}, function(windows){
      var focusedWindowId = undefined;
      for (var i = 0, length = windows.length; i < length; i++) {
          if(windows[i].focused === true){
              focusedWindowId = windows[i].id;
              break;
          }
      }
      callback(focusedWindowId);
   });
}

function getActiveTabByWindowId(id, callback){
  chrome.tabs.query({active:true, windowId:id}, function(tabs){
      callback(tabs[0]);
  });
}

function getTabId(tab){
  return tab.id;
}

function getTabUrl(tab){
  return tab.url;
}

function getDomainFromUrl(url){
  var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    }
    else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];

    return domain;   
}

function logCurrentUrlOrSeyNothing(){
  getFocuesWindowId(function(id){
    var time = new Date().getTime();
	  if(typeof id === 'undefined'){
	    if(currentObject.hasValue()){
	        var endedEvent = currentObject.clearObject(time);
	        insert(endedEvent.url, endedEvent.domain, endedEvent.from, endedEvent.to);
	        console.log(endedEvent)
	    }
	  }else{
	    getActiveTabByWindowId(id, function(tab){
	      var tabId = getTabId(tab);
	      var tabUrl = getTabUrl(tab);
	      var tabDomain = getDomainFromUrl(tabUrl);
	      
	      if(!currentObject.hasValue()){
	        currentObject.setValue(tabUrl, tabDomain, time);
	      }else{
	        if(!currentObject.compare(tabUrl)){
	          var endedEvent = currentObject.registerEndEvent(tabUrl, tabDomain, time);
	          insert(endedEvent.url, endedEvent.domain, endedEvent.from, endedEvent.to);
	          console.log(endedEvent)
	        }
	      }
	      //console.log("Id: " + tabId + " domain: "+ tabDomain + " url: " + tabUrl);
	    });
	  }
	})
}



/*CALLBACKS*/
function OnTabCreated(tab){
  logCurrentUrlOrSeyNothing();
}

function OnTabUpdated(tabId, changeInfo, tab)
{
  logCurrentUrlOrSeyNothing();
}


function OnTabActivated(info)
{
  logCurrentUrlOrSeyNothing();
}


function OnTabRemoved(tabId, removeInfo){
  logCurrentUrlOrSeyNothing();
}

function OnWindowFocusChanged(windowId){
	logCurrentUrlOrSeyNothing();
}

/**/
function OnWindowCreated(window){
  logCurrentUrlOrSeyNothing();
}

/**/
function OnWindowRemoved(windowId){
  logCurrentUrlOrSeyNothing();
}


