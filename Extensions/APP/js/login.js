$(function(){
  $('.logged').hide();
  
  function setView(){
   getValueByKeyFromDB("LOGIN").then(l =>{
     if(l !== ''){
       $('.login').hide();
       $('.logged').show();
       $('#asWho span').html(l);
     }
   });
  }
  
  $('#relog').click(function(){
    $('.login').show();
    $('.logged').hide();
  });
  
  
  $('#loginBtn').click(function(){
    var uLogin = $('#login').val();
    var uPass = $('#password').val();
    getValueByKeyFromDB("LOGINURL").then(url =>{
            $.ajax({
              type: 'POST',
              url: url + "/login",
              data: JSON.stringify ({login : uLogin, password : uPass }),
              success: function(data){ 
                console.log(data);
                  if(data.success){
                    Promise.all([setValueByKey("APPKEY", data.appKey), setValueByKey("LOGIN", data.login)])
                    .then(values =>{
                      console.log('Login ok');
                      setView();
                    });
                  }else{
                      alert('Incorrect credentials!');
                  }
              },
              contentType: "application/json",
              dataType: 'json'
        }).fail(function(){
          alert("ERROR");
        });
    });   
  });
  
  setView();
});

