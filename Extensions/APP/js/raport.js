
function getRaport(year, month, day){
  var statistics = getStatistics(year, month, day);
}

function createTablesIfNotExist(){
  var daysTableSQL = "CREATE TABLE IF NOT EXISTS DAYS(DYS_ID INTEGER PRIMARY KEY, DYS_YEAR INTEGER, DYS_MONTH INTEGER, DYS_DAY INTEGER)";
  var statisticsTableSQL = "CREATE TABLE IF NOT EXISTS STATISTICS(ST_ID INTEGER PRIMARY KEY, ST_DYS_ID INTEGER, ST_DMN VARCHAR(255), ST_TOTAL INTEGER)";
  var historyTableSQL = "CREATE TABLE IF NOT EXISTS HISTORIES(HT_ID INTEGER PRIMARY KEY, HT_DYS_ID INTEGER, HT_URL VARCHAR(255), HT_FROM INTEGER, HT_TIME INTEGER)";
  db.transaction(function(tx){
    tx.executeSql(daysTableSQL);
    tx.executeSql(statisticsTableSQL);
    tx.executeSql(historyTableSQL);
  });
}

function getStatistics(year, month, day){
  return new Promise(function(resolve, reject){
    getDayIdByYMD(year, month, day).then(dayId =>{
      var sql = "SELECT ST_DMN, ST_TOTAL FROM STATISTICS WHERE ST_DYS_ID = ?;";
      executeTransaction(sql, [dayId]).then(results => {
        var array = {};
        $.each(results.rows, function(index, element){
          array[element['ST_DMN']] = element['ST_TOTAL'];
        });
        resolve(array);
      });
    });
  });
}

function getHistory(year, month, day){
  return new Promise(function(resolve, reject){
    getDayIdByYMD(year, month, day).then(dayId =>{
      var sql = "SELECT DISTINCT HT_URL FROM HISTORIES WHERE HT_DYS_ID = ?;";
      executeTransaction(sql, [dayId]).then(results => {
        var promises = [];
        var array = [];
        $.each(results.rows, function(index, element){
          var url = element['HT_URL'];
          promises.push(getHistoryArrayFor(dayId, url));
          array.push(url);
        });
        Promise.all(promises).then(values =>{
          var array2 = {};
          $.each(values, function(index, element){
            array2[array[index]] = {};
            array2[array[index]].History = element[array[index]].History;
          });
          resolve(array2);
        });
      });
    });
  });
}

function getHistoryArrayFor(dysId, url){
  var sql = "SELECT HT_FROM, HT_TIME FROM HISTORIES WHERE HT_DYS_ID = ? AND HT_URL = ?;";
  return new Promise(function(resolve, reject){
    executeTransaction(sql, [dysId, url]).then(results => {
      var array = [];
      $.each(results.rows, function(index, element){
        var obj = {
          From : element['HT_FROM'],
          Time : element['HT_TIME']
        };
        array.push(obj);
      });
      var obj = {};
      obj[url] = {
        History : array
      };
      resolve(obj);
    });
  });
}

function createStatisticRaport(year, month, day, login, key){
  return new Promise(function(resolve, reject){
    var time = {
      Year : year,
      Month : month,
      Day : day
    };
    
    var credentials = {
      login : login,
      appKey : key
    };
    
    getStatistics(year, month, day).then(value => {
      resolve({credentials : credentials, statistics :[{Time : time, ApplicationStatistics : {}, WebsidesStatistics : value}]});
    });
  });
}

function createHistoryRaport(year, month, day, login, key){
   return new Promise(function(resolve, reject){
    var time = {
      Year : year,
      Month : month,
      Day : day
    };
    
    var credentials = {
      login : login,
      appKey : key
    };
    
    getHistory(year, month, day).then(value => {
      resolve({credentials : credentials, histories : [{Time : time, ApplicationsHistory : {}, WebsidesHistory : value, HardwaresHistory : {}}]});
    });
  });
}






























