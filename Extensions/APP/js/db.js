/*Constants*/
databaseName = 'db';
databaseVersion = '1.0';
databaseDescription = 'Description of db';
databaseSize = 10 * 1024 * 1024;
var db = openDB();
createTablesIfNotExist();
setUpConfig();

/*Functions*/
function openDB(){
    return openDatabase(databaseName, databaseVersion, databaseDescription, databaseSize);
}

function executeTransaction(query, param) {
  if(typeof param === 'undefined'){
    param = [];
  }
  
  console.log("Transaction Query : " + query);
  console.log("Transaction Parameters : " + param);
  
  return new Promise(function (resolve, reject) {
      db.transaction(function (transaction) {
          transaction.executeSql(query, param, function (transaction, result) {
              resolve(result);
          }, null, null);
      });
  });
}

function createTablesIfNotExist(){
  var daysTableSQL = "CREATE TABLE IF NOT EXISTS DAYS(DYS_ID INTEGER PRIMARY KEY, DYS_YEAR INTEGER, DYS_MONTH INTEGER, DYS_DAY INTEGER)";
  var statisticsTableSQL = "CREATE TABLE IF NOT EXISTS STATISTICS(ST_ID INTEGER PRIMARY KEY, ST_DYS_ID INTEGER, ST_DMN VARCHAR(255), ST_TOTAL INTEGER)";
  var historyTableSQL = "CREATE TABLE IF NOT EXISTS HISTORIES(HT_ID INTEGER PRIMARY KEY, HT_DYS_ID INTEGER, HT_URL VARCHAR(255), HT_FROM INTEGER, HT_TIME INTEGER)";
  var confDB = "CREATE TABLE IF NOT EXISTS CONF(CNF_ID INTEGER PRIMARY KEY, CNF_KEY VARCHAR(255), CNF_VALUE VARCHAR(255))";
  Promise.all([executeTransaction(daysTableSQL), executeTransaction(statisticsTableSQL), executeTransaction(historyTableSQL), executeTransaction(confDB)])
  .then(values => 
    {
      console.log("Table created!");
    });
}

function setUpConfig(){
  Promise.all([setValueByKey("LOGINURL", "http://127.0.0.1:10000"), setValueByKey("URL", "http://127.0.0.1:10001/raport")])
  .then(values => {
    console.log("Inserted configuration!");
  });
}

function dropAll(){
    Promise.all([executeTransaction("DROP TABLE HISTORIES"), executeTransaction("DROP TABLE DAYS"), executeTransaction("DROP TABLE STATISTICS")])
    .then(values => 
    {
      console.log("Table drop!");
    });
}

function insert(url, domain, from, to){
  var insertHistorySql = "INSERT INTO HISTORIES(HT_URL, HT_FROM, HT_TIME, HT_DYS_ID) VALUES(?,?,?,?);";
  getDayId(from).then(dayId => {
    console.log(dayId);
    Promise.all([executeTransaction(insertHistorySql, [url, from, to - from, dayId]), getChangeStatisticsSQL(url, domain, from, to, dayId)]).then(values =>{
      console.log("Successfully inserted data!");
    });
  });
}

function getChangeStatisticsSQL(url, domain, from, to, dayId){
  var sqlInsert = "INSERT INTO STATISTICS(ST_TOTAL, ST_DYS_ID, ST_DMN) VALUES (?,?,?);";
  var sqlUpdate = "UPDATE STATISTICS SET ST_TOTAL = ST_TOTAL + ? WHERE ST_DYS_ID = ? AND ST_DMN = ?;";
  var findSql = "SELECT COUNT(*) FROM STATISTICS WHERE ST_DYS_ID = ? AND ST_DMN = ? LIMIT 1;";
  
  return new Promise(function(resolve, reject){
    executeTransaction(findSql, [dayId, domain]).then(results => {
      if(results.rows[0]['COUNT(*)'] === 0){
        executeTransaction(sqlInsert, [to - from, dayId, domain]).then(r1 => {
          resolve();
        });
      }else{
        executeTransaction(sqlUpdate, [to - from, dayId, domain]).then(r1 => {
          resolve();
        });
      }
    });
  });
}


function getDayId(from){
  var date = new Date(from);
  var year = date.getFullYear();
  var month = date.getMonth();
  var day = date.getDate();
  
  var insertSQL = "INSERT INTO DAYS(DYS_YEAR, DYS_MONTH, DYS_DAY) VALUES (?,?,?);";
  var findSQL = "SELECT DYS_ID FROM DAYS WHERE DYS_YEAR = " + year + " AND DYS_MONTH = " + (month + 1).toString() + " AND DYS_DAY = " + day + " LIMIT 1;";
  
  return new Promise(function(resolve, reject){
    executeTransaction(findSQL).then(results => {
      if(results.rows.length === 0){
        executeTransaction(insertSQL, [year, month + 1, day]).then(resultsInsert => {
          resolve(resultsInsert.insertId);
        });
      }else{
        resolve(results.rows[0]['DYS_ID']);
      }
    });
  });
}

function getDayIdByYMD(year, month, day){
  var insertSQL = "INSERT INTO DAYS(DYS_YEAR, DYS_MONTH, DYS_DAY) VALUES (?,?,?);";
  var findSQL = "SELECT DYS_ID FROM DAYS WHERE DYS_YEAR = " + year + " AND DYS_MONTH = " + month + " AND DYS_DAY = " + day + " LIMIT 1;";
  
  return new Promise(function(resolve, reject){
    executeTransaction(findSQL).then(results => {
      if(results.rows.length === 0){
        executeTransaction(insertSQL, [year, month, day]).then(resultsInsert => {
          resolve(resultsInsert.insertId);
        });
      }else{
        resolve(results.rows[0]['DYS_ID']);
      }
    });
  });
}

function getLoginAppKeyUrlFromDB(){
  return new Promise(function(resolve, reject){
    Promise.all([getValueByKeyFromDB("LOGIN"), getValueByKeyFromDB("APPKEY"), getValueByKeyFromDB("URL")])
    .then(values =>{
      resolve({login : values[0], appKey : values[1], url : values[2]});
    });
  });
}

function getValueByKeyFromDB(key){
  return new Promise(function(resolve, reject){
    var sql = "SELECT CNF_VALUE FROM CONF WHERE CNF_KEY = ? LIMIT 1;";
    executeTransaction(sql, [key]).then(results => {
      if(results.rows.length === 0){
        resolve("");
      }else{
        resolve(results.rows[0]['CNF_VALUE']);
      }
    });
  });
}

function setValueByKey(key, value){
  return new Promise(function(resolve, reject){
    var sql = "DELETE FROM CONF WHERE CNF_KEY = ?;";
    executeTransaction(sql, [key]).then(x =>{
      var insert = "INSERT INTO CONF(CNF_KEY, CNF_VALUE) VALUES (?,?);";
      executeTransaction(insert, [key, value]).then(y =>{
        console.log("Inserted: " + [key, value]);
        resolve(value);
      });
    });
  });
}
























