function doJob(){
  console.log("Performing raporting job!");
  getLoginAppKeyUrlFromDB().then(result => {
    var url = result.url;
    var appKey = result.appKey;
    var login = result.login;
    
    var sql = "SELECT DISTINCT ST_DYS_ID, DYS_YEAR, DYS_MONTH, DYS_DAY FROM STATISTICS JOIN DAYS ON ST_DYS_ID = DYS_ID;";
    executeTransaction(sql, []).then(results =>{
      $.each(results.rows, function(index, row){
        createStatisticRaport(row['DYS_YEAR'], row['DYS_MONTH'], row['DYS_DAY'], login, appKey).then(raport =>{
          var time = raport.statistics[0].Time;
          postStatistics(raport, url + "/statistics", function(){
            getDayIdByYMD(time.Year, time.Month, time.Day).then(x =>{
               afterStatisticsSucces(x);
            });
          });
        });
      });
    });
    
    var sqlHist = "SELECT DISTINCT HT_DYS_ID, DYS_YEAR, DYS_MONTH, DYS_DAY FROM HISTORIES JOIN DAYS ON HT_DYS_ID = DYS_ID;";
    executeTransaction(sqlHist, []).then(results =>{
      $.each(results.rows, function(index, row){
        createHistoryRaport(row['DYS_YEAR'], row['DYS_MONTH'], row['DYS_DAY'], login, appKey).then(raport =>{
          postHistories(raport, url + "/histories", function(){
            var now = new Date();
            afterHistoriesSucces(now);
          });
        });
      });
    });
  });
}

function afterStatisticsSucces(id){
  var sql = "DELETE FROM STATISTICS WHERE ST_DYS_ID = ?";
  executeTransaction(sql, [id]).then(results =>{
    console.log("Successfully deleted statisitcs.");
  });
}

function afterHistoriesSucces(date){
  var sql = "DELETE FROM HISTORIES WHERE HT_FROM <= ?";
  executeTransaction(sql, [date]).then(results =>{
    console.log("Successfully deleted histories.");
  });
}

setInterval(doJob, 10 * 60 * 1000);