function ObjectWithSetMemory(){
	this.url = undefined;
	this.domain = undefined;
	this.setTime = undefined;
	this.value = false; 

  this.hasValue = function(){
    return this.value;
  };
  
  this.setValue = function(url, domain, setTime){
    this.url = url;
    this.domain = domain;
    this.setTime = setTime;
    this.value = true;
  };
  
	this.compare = function(url){	
		return this.url == url;
	};
	
	this.registerEndEvent = function(url, domain, setTime){
	  var obj = {
	    url : this.url,
	    domain : this.domain,
	    from : this.setTime,
	    to : setTime
	  };
	  
	  this.setValue(url, domain, setTime);
	  
	  return obj;
	};
	
	this.clearObject = function(setTime){
	  var obj = {
	    url : this.url,
	    domain : this.domain,
	    from : this.setTime,
	    to : setTime
	  };
	  
	  this.value = false;
	  return obj;
	};
}