function post(url, obj, success){
  console.log(obj);
  $.ajax({
    url : url,
    datatype:'json',
    type: "post", 
    contentType: "application/json",
    data: JSON.stringify(obj),
    success:success
  });
}

function postStatistics(obj, url, success){
  post(url, obj, function(data){
    if(data.success){
      success();
    }
  }, function(e){
    console.log(e);
  });
}

function postHistories(obj, url, success){
  post(url, obj, function(data){
    if(data.success){
      success();
    }
  }, function(e){
    console.log(e);
  });
}






