var boot = require("../../test_www").boot,
	shutdown = require("../../test_www").shutdown,
	port = require("../../test_www").port,
	superagent = require('superagent'),
	expect = require('expect.js');

describe('Server', function () {
	before(function () {
		boot();
	});

	describe('Should responds', function () {
		it('Index html', function (done) {
			superagent
				.get('http://localhost:' + port)
				.end(function (err, res) {
					expect(res.status).to.equal(200);
					done();
				});
		});

		it('Users html', function (done) {
			superagent
				.get('http://localhost:' + port + "/users")
				.end(function (err, res) {
					expect(res.status).to.equal(200);
					done();
				});
		});

		it('Error', function (done) {
			superagent
				.get('http://localhost:' + port + "/asdfdsfusers")
				.end(function (err, res) {
					expect(res.status).to.equal(404);
					done();
				});
		});
	});

	after(function () {
		shutdown();
	});
});
	