var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.clearCookie("userDate");
  res.clearCookie("userKey");
  res.clearCookie("userName");
  res.locals.user = 'undefined';

  res.render('logout');
});

module.exports = router;