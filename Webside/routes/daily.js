var express = require('express');
var router = express.Router();
var request = require('request');
var configuration = require("../public/javascripts/Config/config.js");

router.get('/', function(req, res) {
  res.render('daily');
});

router.post('/get', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  getDaily(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

function getDaily(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      day : body.day,
      app : body.app
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.daily.getUrl,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}
module.exports = router;