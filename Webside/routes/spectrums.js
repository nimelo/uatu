var express = require('express');
var router = express.Router();
var request = require('request');
var configuration = require("../public/javascripts/Config/config.js");

router.get('/', function(req, res) {
  res.render('spectrums');
});

router.post('/spectrum/get', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  getSpectrum(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

function getSpectrum(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      from : body.from,
      elements : body.elements,
      range : body.range,
      groupBy : body.groupBy
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.spectrum.getUrl,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

module.exports = router;