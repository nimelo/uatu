var express = require('express');
var router = express.Router();
var request = require('request');
var configuration = require("../public/javascripts/Config/config.js");

router.get('/', function(req, res) {
  var pageData = {
    categories : [],
     /* [{Name: 'First', Visibility:'On', Id: 1},
      {Name: 'Second', Visibility:'Off'},
      {Name: 'Third', Visibility:'On'}],*/
    spectrums : [],/*
      [{'Name': 'First', 'Visibility':'On', 'Range' : 'Last 7 days'},
      {'Name': 'Second', 'Visibility':'Off', 'Range' : 'Last 14 days'},
      {'Name': 'Third', 'Visibility':'On', 'Range': 'Last 0 days'}],*/
    hardwares : [{'Name': 'First', 'Visibility':'On', 'Range' : 'Last 7 days'},
      {'Name': 'Second', 'Visibility':'Off', 'Range' : 'Last 14 days'},
      {'Name': 'Third', 'Visibility':'On', 'Range': 'Last 0 days'}],
    naming : [],/*[{'Name': 'Google Chrome', 'Path' : 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'},
    {'Name': 'Custom App', 'Path':'E:\BitBucket\UATU\Desktop\UserInterface\bin\x64\Debug\UserInterface.exe'}]*/
  };

  res.render('account', {pageData});
});

router.get('/category/list', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;

  getCategories(date, key, 
  function(){
    res.send(false);
  },
  function(body){
    res.send(body)
  });
});

router.get('/category/get/:id', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var id = req.params.id;

  getCategory(date, key, id, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/category/delete/:id', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var id = req.params.id;

  deleteCategory(date, key, id, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/category/add', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  addCategory(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/category/changeVisibility', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  changeVisibilityCategory(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/category/modify', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  modifyCategory(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});


function changeVisibilityCategory(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      categoryId : body.id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.category.change,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function addCategory(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      category : body
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.category.add,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function modifyCategory(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      category : body
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.category.modify,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function deleteCategory(uDate, uKey, id, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      categoryId : id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.category.delete,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function getCategories(uDate, uKey, fail, success){
  var obj = {
      date : uDate,
      key : uKey
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.category.getListUrl,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function getCategory(uDate, uKey, id, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      categoryId : id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.category.get,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

router.get('/spectrum/list', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;

  getSpectrums(date, key, 
  function(){
    res.send(false);
  },
  function(body){
    res.send(body)
  });
});

router.get('/spectrum/get/:id', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var id = req.params.id;

  getSpectrum(date, key, id, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/spectrum/delete/:id', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var id = req.params.id;

  deleteSpectrum(date, key, id, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/spectrum/add', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  addSpectrum(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/spectrum/changeVisibility', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  changeVisibilitySpectrum(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/spectrum/modify', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  modifySpectrum(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});


function changeVisibilitySpectrum(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      spectrumId : body.id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.spectrum.change,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function addSpectrum(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      spectrum : body
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.spectrum.add,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function modifySpectrum(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      spectrum : body
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.spectrum.modify,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function deleteSpectrum(uDate, uKey, id, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      spectrumId : id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.spectrum.delete,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function getSpectrums(uDate, uKey, fail, success){
  var obj = {
      date : uDate,
      key : uKey
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.spectrum.getListUrl,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function getSpectrum(uDate, uKey, id, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      spectrumId : id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.spectrum.get,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

router.get('/appNaming/list', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;

  getAppNamings(date, key, 
  function(){
    res.send(false);
  },
  function(body){
    res.send(body)
  });
});

router.post('/appNaming/delete/:id', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var id = req.params.id;

  deleteAppNaming(date, key, id, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

router.post('/appNaming/add', function(req, res){
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  var body = req.body;

  addAppNaming(date, key, body, 
    function(){
      res.send(false);
    },
    function(body){
      res.send(body)
    });
});

function addAppNaming(uDate, uKey, body, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      appNaming : body
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.appNaming.add,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function deleteAppNaming(uDate, uKey, id, fail, success){
  var obj = {
      date : uDate,
      key : uKey,
      appNamingId : id
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.appNaming.delete,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

function getAppNamings(uDate, uKey, fail, success){
  var obj = {
      date : uDate,
      key : uKey
  };

  var options = {
      method : 'post',
      body : obj,
      json : true,
      url : configuration.configuration.appNaming.getListUrl,
      headers: {

      }
  };

  request(options, function(err, res, body){
        if(err){
            fail();
            return;
        }else if(body.success){
            success(body);
        }else{
            fail();
        }   
    });
}

module.exports = router;