var express = require('express');
var router = express.Router();
var authentication = require('../public/javascripts/Authentication/authentication.js');

router.get('/', function(req, res) {
  res.render('login');
});

router.post('/auth', function(req, res){
  var body = req.body;
  authentication.login(body.login, body.password,
  function(response){
    res.cookie('userDate', response.date);
    res.cookie('userKey', response.key);
    res.cookie('userName', response.name);

    res.send(true);
  },
  function(){
     res.send(false);
  });
});

module.exports = router;