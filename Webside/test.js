var request = require('request')

var obj = {
    login: "test",
	appKey: "key"
};

var url = "http://127.0.0.1:10002/configuration"
var options = {
  method: 'post',
  body: obj, // Javascript object
  json: true, // Use,If you are sending JSON data
  url: url,
  headers: {
    // Specify headers, If any
  }
}

var result = {};
request(options, function (err, res, body) {
  if (err) {
    console.log('Error :', err)
    result = false;
    return;
  }
  console.log(' Body :', body)
  result = true;
});

console.log(result);