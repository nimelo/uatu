var request = require('request');
var configuration = require("../Config/config.js");

Authentication = {
    isCorrect : function(uDate, uKey, success, fail){
        var obj = {
            date : uDate,
            key : uKey,
            appKey : configuration.appKey
        };

        var options = {
            method : 'post',
            body : obj,
            json : true,
            url : configuration.authCheckUrl,
            headers: {

            }
        };

        request(options, function(err, res, body){
            if(err){
                fail();
                return;
            }else if(body.success){
                success(body);
            }else{
                fail();
            }   
        });
    },

    login : function(uLogin, uPass, success, fail){
        var obj = {
            login : uLogin,
            password : uPass
        };

        var options = {
            method : 'post',
            body : obj,
            json : true,
            url : configuration.authLoginUrl,
            headers: {

            }
        };

        request(options, function(err, res, body){
            if(err){
                fail();
                return;
            }else if(body.success){
                success(body);
            }else{
                fail();
            }          
        });
    }
};

module.exports = Authentication
