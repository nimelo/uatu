$(function(){
    var namings = [];
    $.get('/user/account/appNaming/list', function(data){
        if(data.success){
            namings = data.appNamings;            
        }else{
            alert("Error");
        }
    }).fail(function(){
        alert('Error');
    }) ;

    $('#day').datetimepicker({date : new Date(), format : 'DD/MM/YYYY'});

    $('#search').click(function(){
        $('#history').html("");
        var day = getDataFromDateTimePicker('day').date;

        getChart(day, true, function(data){
            $.each(data, function(index, value){
                $.each(namings, function(index2, name){
                    if(value.name == name.path){
                        value.name = name.name
                    }
                }); 
            });
            ChartsRendering.renderPie('#app', data, 'Applications', "For: " + $('#day').data().date);
        });

        getChart(day, false, function(data){
            $.each(data, function(index, value){
                $.each(namings, function(index2, name){
                    if(value.name == name.path){
                        value.name = name.name
                    }
                }); 
            });
            ChartsRendering.renderPie('#web', data, 'Websides', "For: " + $('#day').data().date);
        });
    });


    function getDataFromDateTimePicker(id){
        // 'DD/MM/YYYY HH:mm'
        var strVal = $('#' + id).data().date;

        var obj = {
            date : {
                Day : strVal.substring(0, 2),
                Month : strVal.substring(3, 5),
                Year : strVal.substring(6, 10),
            }         
        }

        return obj;
    }

    function getChart(cday, isApp, callback){
       var obj = {
           day : cday,
           app : isApp
       };
       $.post('/user/daily/get', obj ,function(data){
            if(data.success){
                callback(data.data);
            }else{
                alert("Error");
            }
        
        }).fail(function(){
            alert('Error');
        });
    }
    
});   