$('#loginBtn').click(function(){
    var uLogin = $('#login').val();
    var uPass = $('#password').val();

    $.ajax({
        type: 'POST',
        url: '/login/auth',
        data: JSON.stringify ({login : uLogin, password : uPass }),
        success: function(data){ 
            if(data == true){
                window.location = "/user/account";
            }else{
                alert('Incorrect credentials!');
            }
        },
        contentType: "application/json",
        dataType: 'json'
    });
});