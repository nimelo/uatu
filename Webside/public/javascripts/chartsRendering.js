function ChartsRendering() {

}

/**/
ChartsRendering.secondsToTime = function (seconds) {
    var hours = Math.floor(seconds / 3600);
    seconds -= hours * 3600;
    var minutes = Math.floor(seconds / 60);
    seconds -= minutes * 60;
    var result = "";

    if (hours != 0) {
        result += hours + "h ";
    }

    if (minutes != 0) {
        result += minutes + "min ";
    }

    if (seconds != 0) {
        result += seconds + "s.";
    }

    return result;
}

function getRandomColorArray(amount){
	var colors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"]
	return shuffle(colors);
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function getRandomRolor() {
    return Math.floor((Math.random() * 255 * 255 * 255));
} 

/**/
ChartsRendering.renderPie = function (elementId, data, title, subtitle) {
    $(elementId).highcharts({
        colors: getRandomColorArray(data.length),
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: title,
            style: {
                fontWeight : 'bold'
            }
        },
        subtitle: {
            text: subtitle
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
            formatter: function () {
                return "Time spent on <b>" + this.point.name + "</b>: " + "<b>" + ChartsRendering.secondsToTime(Math.round(this.y / 1000));
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    style: {
                        width: '100px'
                    },
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    formatter: function () {
                        return "Time spent on " + this.series.name + ": " + ChartsRendering.secondsToTime(Math.round(this.y / 1000));
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            name: "Today's domains",
            colorByPoint: true,
            data: data
        }]
    });
}


/**/
ChartsRendering.renderColumn = function (elementId, data, categories, title, subtitle) {

    $(elementId).highcharts({
        colors : getRandomColorArray(),
        chart: {
            type: 'column'
        },
        title: {
            text: title,
            style: {
                fontWeight : 'bold'
            }
        },
        subtitle: {
            text : subtitle
        },
        xAxis: {
            categories: categories,
            crosshair: true,
            tickInterval: 1
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Time spent [hours]'
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        tooltip: {
            shared: true,

            formatter: function () {
                var points = this.points;
                var pointsLength = points.length;
                var tooltipMarkup = pointsLength ? '<span style="font-size: 10px">' + /*categories[new Date().getMonth()] +*/ " " + this.x + '</span><br/>' : '';
                var index;
                var value;

                for (index = 0; index < pointsLength; index += 1) {
                    //value = ChartsRendering.secondsToTime(points[index].y);
                    tooltipMarkup += '<br/><span style="color:' + this.points[index].series.color + '">\u25CF</span>' + this.points[index].series.name + ': <b>' + ChartsRendering.secondsToTime(Math.round(points[index].y / 1000)) + '</b><br/>';
                }

                return tooltipMarkup;
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: data
    });
}

/**/
ChartsRendering.renderZoomLine = function(elementId, data, title, subtitle, tickInterval){
    $(elementId).highcharts({
            chart: {
                zoomType: 'x',
                spacingRight: 20
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },            
            title: {
                text: title,
                style: {
                    fontWeight : 'bold'
                }
            },
            subtitle: {
                text: subtitle
            },
            xAxis: {
                type: 'datetime',
                tickInterval: tickInterval,
                title: {
                    text: null
                }
            },
            yAxis: {
                title: {
                    text: 'Values'
                }
            },
            tooltip: {
                shared: true
            },
            legend: {
                enabled: true
            },
    
            series: data
        });
}