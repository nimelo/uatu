$(function () {
    function getVisibleCategories(){
        $.get('/user/account/category/list', function(data){
             if(data.success){
                $.get('/user/account/appNaming/list', function(namings){
                    if(namings.success){
                        var categories = data.categories;
                        $.each(categories, function(index, element){
                            createContainerFor(element);
                            renderChart(element, namings.appNamings);
                        });
                    }else{
                        alert("Error");
                    }
                    
                    }).fail(function(){
                        alert('Error');
                    })                
             }else{
                 alert("Error");
             }           
         }).fail(function(){
             alert('Error');
         })               
    }

    function createContainerFor(element){
        var html = "<div class='col-md-6'>";
        html += "<div id='" + element.id +"' class='chart'/><div/>";
        $('#charts').append(html);
    }

    function renderChart(element, namings){
        getCategoryById(element, function(data){
             getCategoryData(data, element, function(body){
                  $.each(body, function(index, value){
                       $.each(namings, function(index2, name){
                            if(value.name == name.path){
                                value.name = name.name
                            }
                       }); 
                  });
                  ChartsRendering.renderPie('#' + element.id , body, element.name, "For last " + element.range + " days");
             });
        });      
    }

   function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
   }

   function getCategoryData(data, element, callback){
       var now = addDays(new Date(), -element.range);
       var obj = {
           from : {
               Day : now.getDate(),
               Month : now.getMonth() + 1,
               Year : now.getFullYear()
           },
           range : element.range,
           elements : data
       };

       $.post('/user/categories/category/get', obj ,function(data){
            if(data.success){
                callback(data.data);
            }else{
                alert("Error");
            }
        
        }).fail(function(){
            alert('Error');
        });
   }

    function getCategoryById(element, callback){
        $.get('/user/account/category/get/' + element.id , function(data){
             if(data.success){
                 callback(data.category.elements);
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
    }

    getVisibleCategories();
});