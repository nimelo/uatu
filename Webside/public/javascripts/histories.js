$(function(){
    var namings = [];
    $.get('/user/account/appNaming/list', function(data){
        if(data.success){
            namings = data.appNamings;            
        }else{
            alert("Error");
        }
    }).fail(function(){
        alert('Error');
    }) ;

    var now = new Date();
    now.setHours(0);
    now.setMinutes(0);
    $('#from').datetimepicker({date : now, format : 'DD/MM/YYYY HH:mm'});
    $('#to').datetimepicker({date : new Date(), format : 'DD/MM/YYYY HH:mm'});

    var precision = new Date();
    precision.setHours(0);
    precision.setMinutes(15);
    precision.setSeconds(0);
    $('#precision').datetimepicker({date : precision, format : 'HH:mm:ss'});

    $('#search').click(function(){
        $('#history').html("");
        var dtFrom = getDataFromDateTimePicker('from');
        var dtTo = getDataFromDateTimePicker('to');
        var prec = getPrecisionFromDateTimePicker('precision');
        var obj = {
            from : dtFrom,
            to : dtTo,
            precision : prec
        };

        $.post('/user/histories/search', obj , function(data){
             if(data.success){
                 renderYears(data);
                 wireUpPanelClicks();
                 showFirst();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
    });

    function wireUpPanelClicks(){
        $('.panel-info').click(function(e){
            e.stopPropagation();
            var x = $(this).children().closest("div.panel-body.day");
            if(x.is(':visible')){
                $(x).hide();
            }else{
                $(x).show();
            }
        });

        $('.panel-warning').click(function(e){
            e.stopPropagation();
            var x = $(this).children().closest("div.panel-body.month");
            if(x.is(':visible')){
                $(x).hide();
            }else{
                $(x).show();
            }
        });

        $('.panel-danger').click(function(e){
            e.stopPropagation();
            var x = $(this).children().closest("div.panel-body.year");
            if(x.is(':visible')){
                $(x).hide();
            }else{
                $(x).show();
            }
        });

        $('tbody tr .chart').click(function(e){
            e.stopPropagation();
        });

        $('tbody tr').click(function(e){
            e.stopPropagation();
            x = $(this).next();
            var title = $(this).children("td:nth(0)").text();
            var subTitle = "From: " + $(this).children("td:nth(1)").text() + " To: " + $(this).children("td:nth(2)").text();
            var cfrom = $(this).attr('from');
            var ctime = $(this).attr('time');

            var obj = {
                from : cfrom,
                time : ctime
            }
            var id = 'chart' + cfrom + "" + ctime;
            if(x.hasClass('chart')){
                if(x.is(':visible')){
                    $(x).hide();
                }else{
                    $(x).show();
                }
            }else{
                $(this).after("<tr class='chart'><td colspan='4'><div id='" + id +"'> NO DATA AVALAIBLE </div></td></tr>")
                $.post('/user/hardwares/getfromtimeall', obj , function(data){
                if(data.success){
                    $.each(data.series, function(index, element){
                        var tmp = element.pointStart;
                        var now = new Date();
                        element.pointStart = new Date(tmp - now.getTimezoneOffset() * 60000).getTime();
                    })
                    if(data.series.length > 0)
                    {
                        ChartsRendering.renderZoomLine('#' + id, data.series, title, subTitle, Math.round(5 * 60 * 1000));
                        $('#' + id).click(function(e){
                            e.stopPropagation();
                        });
                    }
                }else{
                    alert("Error");
                }
                    
                }).fail(function(){
                    alert('Error');
                });
            } 

            

                      
        });
    }

    function showFirst(){
        $('.panel-danger div.panel-body').first().show();
        $('.panel-warning div.panel-body').first().show();
        $('.panel-info div.panel-body').first().show();

    }

    function getPrecisionFromDateTimePicker(id){
        // 'HH:mm:ss'
        var strVal = $('#' + id).data().date;
        var hours = Number(strVal.substr(0,2));
        var minutes = Number(strVal.substr(3,2));
        var seconds = Number(strVal.substr(6,2));

        return Math.round(Math.round(hours * 60 * 60  + minutes * 60 + seconds) * 1000);
    }

    function getDataFromDateTimePicker(id){
        // 'DD/MM/YYYY HH:mm'
        var strVal = $('#' + id).data().date;

        var obj = {
            date : {
                Day : strVal.substring(0, 2),
                Month : strVal.substring(3, 5),
                Year : strVal.substring(6, 10),
            },
            time : {
                hours : strVal.substring(11, 13),
                minutes : strVal.substring(14, 16)
            }         
        }

        return obj;
    }

    function renderYears(data){
        $.each(data.years, function(index, year){
            var panelId = renderYearPanel(year.name);
            renderMonths(panelId, year);
        });
    }

    function renderYearPanel(name){
        var id = "y" + name;
        var html = "<div class='panel panel-danger'>";
            html += "<div class='panel-heading'><b>" + name + "</b></div>";
            html += "<div class='panel-body year' style='display:none' id='" + id + "'/>";
        html += "</div>";
        $('#history').append(html);
        return id;
    }

    function renderMonths(id, year){
        $.each(year.months, function(index, month){
            var panelId = renderMonthPanel(id, month.name, year.name);
            renderDays(panelId, month, year.name);
        });
    }

    function renderMonthPanel(obj, monthName, yearName){
        var id = "m" + yearName + monthName;
        var html = "<div class='panel panel-warning'>";
            html += "<div class='panel-heading'><b>" + monthName + " " +yearName  + "</b></div>";
            html += "<div class='panel-body month' style='display:none' id='" + id + "'/>";
        html += "</div>";
        $('#'+ obj).append(html);
        return id;
    }

    function renderDays(id, month, yearName){
        $.each(month.days, function(index, day){
            var panelId = renderDayPanel(id, day.name, month.name, yearName);
            renderDay(panelId, day, month.name, yearName);
        });    
    }

    function renderDayPanel(obj, dayName, monthName, yearName){
        var id = "d" + yearName + monthName + dayName;
        var html = "<div class='panel panel-info'>";
            html += "<div class='panel-heading'><b>" + dayName + " " + monthName + " " +yearName+ "</b></div>";
            html += "<div class='panel-body day' style='display:none' id='" + id + "'/>";
        html += "</div>";
        $('#'+ obj).append(html);
        return id; 
    }

    function renderDay(id, day, monthName, yearName){
        var bodyId = renderTableHeader(id, day.name, monthName, yearName);
        $.each(day.data, function(index, entry){
            renderRow(bodyId, entry);
        });
    }

    function renderTableHeader(to, dayNname, monthName, yearName){
        var id = "b" + yearName + monthName + dayNname;
        var html = "<div><table class='table table-hover'>";
        html += "<thead><tr><th>Application</th><th>From</th><th>To</th><th>Timespan</th></tr></thead>"
        html += "<tbody id='" + id + "'></tbody>";
        html += "</div>"
        $('#' + to).append(html);
        return id;
    }

    function renderRow(obj, entry){
        var from = entry.from;
        var time = entry.time;
        var to = from + time;
        var html = "<tr from='" + from +"' time='"+ time + "'>";
        var name = coverNames(entry.name);
        if(name.length > 40){
             html += "<td title='" + name + "'><b>" + name.substring(0, 35) + " ...</b></td>";
        }else{
            html += "<td><b>" + name + "</b></td>";
        }
        
        html += "<td>"+ millisecondsToHHMMSS(from) + "</td><td>"+ millisecondsToHHMMSS(to) + "</td><td>"+ milisecondsToTimeSpan(time) + "</td></tr>";
        $('#'+ obj).append(html);
        return;
    }

    function millisecondsToHHMMSS(value){
        var time = new Date(value);
        return ("0" + time.getHours()).slice(-2)   + ":" + 
                ("0" + time.getMinutes()).slice(-2) + ":" + 
                ("0" + time.getSeconds()).slice(-2);
    }

    function milisecondsToTimeSpan(miliseconds){
        var seconds = Math.round(miliseconds / 1000);
        var hours = Math.floor(seconds / 3600);
        seconds -= hours * 3600;
        var minutes = Math.floor(seconds / 60);
        seconds -= minutes * 60;
        var result = "";

        if (hours != 0) {
            result += hours + "h ";
        }

        if (minutes != 0) {
            result += minutes + "min ";
        }

        if (seconds != 0) {
            result += seconds + "s.";
        }

        return result;
    }

    function coverNames(name){
        var tmpName = name;
        $.each(namings, function(index, element){
            if(name == element.path){
                tmpName = element.name;
                return false;
            }           
        });
        return tmpName;
    }
});