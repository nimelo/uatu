$(document).ready(function() {    

    $('.btn-danger').click(function(){
        $(this).closest('div[class="popup"]').hide();
        $('#fade').addClass('hidden');
        $('#categoryId').val('');
    });

    function renderCategoryElementList(fields, elements){
        var html = renderHeader(fields);
        html += renderCategoryElements(elements);
        html += "</table>";
        $('#categoryItems').html("");
        $('#categoryItems').append(html);
    }

    function renderHeader(fields){
        var html = "<table class='table table-hover'>";
        html += "<thead><tr>";
        $.each(fields, function(index, value){
            html += '<th><b>' + value + '</b></th>';
        });
        html += "</tr></thead>";
        return html;
    }

    function renderCategoryElements(elements){
        var html = "<tbody>";
        $.each(elements, function(index, value){
            html += renderCategoryElement(value);
        });

        html += "</tbody>"
        return html;
    }

    function renderCategoryElement(element){
        var html = "<tr ids=" + element.id + ">";
            html += "<td>" + element.name + "</td>";
            html += "<td>" + element.type + "</td>";
            html += '<td><button class="categoryItemDelete glyphicon glyphicon-remove-circle btn"></button></td>';
            html += "</tr>";
        return html;
    }

    function wireUpCategoryElementButtons(){
        $('.categoryItemDelete').click(function(){
                $(this).closest('tr').remove();
        });       
    }

    $('.categoryItemAdd').click(function(){
        var type = $('#categoryItemType').val();           
        var value = $('#categoryItemValue').val();
        var html = renderCategoryElement({'type':type, 'name':value})
        $('#categoryItems tbody').append(html);
        wireUpCategoryElementButtons();
    });

     $('#categoryAccept').click(function(){
         var categoryName = $('#categoryName').val();
         var categoryVisibility = $('#categoryVisibility').prop('checked');
         var categoryRange = $('#categoryRange').val();
         var categoryId = $('#categoryId').val();

         var categoryElements = getCategoryElementsFromTable();

         var obj = {
             info : {
                name : categoryName,
                visibility : categoryVisibility,
                range : categoryRange,
                id :categoryId,
             },            
             elements : categoryElements
         }
         if(categoryId == ''|| typeof categoryId === 'undefined'){
             addCategoryPost(obj, function(){
                $('#categoryAccept').closest('div[class="popup"]').hide();
                $('#fade').addClass('hidden');
                $('#categoryId').val('');
             }, function(){

             });
         }else{
             modifyCategoryPost(obj, function(){
                $('#categoryAccept').closest('div[class="popup"]').hide();
                $('#fade').addClass('hidden');
                $('#categoryId').val('');
             }, function(){
                 
             });
         }
     });

     function getCategoryElementsFromTable(){
         var obj = new Array();
         var trs = $('#categoryItems table tr');
         trs.splice(0,1);

         $.each(trs, function(index, element){
             var name = $($("td",element)[0]).text();
             var type = $($("td",element)[1]).text();
             obj.push({'name': name, 'type': type})
         });
         return obj;
     }

     function addCategoryPost(obj, success, fail){
          $.post('/user/account/category/add', obj, function(data){
             if(data.success){
                 loadCategoryList();
                 success();
             }else{
                 alert("Error");
                 fail();
             }
            
         }).fail(function(){
             alert('Error');
             fail();
         });
     }

     function modifyCategoryPost(obj, success, fail){
         $.post('/user/account/category/modify', obj, function(data){
            if(data.success){
                 loadCategoryList();
                 success();
             }else{
                 alert("Error");
                 fail();
             }
            
         }).fail(function(){
             alert('Error');
             fail();
         });    
     }

     function deleteCategoryPost(id){
         $.post('/user/account/category/delete/' + id , function(data){
             if(data.success){
                 loadCategoryList();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
     }

     function changeVisibilityPost(id){
         $.post('/user/account/category/changeVisibility' ,{'id': id} , function(data){
             if(data.success){
                 loadCategoryList();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
     }

     function loadCategoryList(){
         $.get('/user/account/category/list', function(data){
             if(data.success){
                 renderCategoryList(data);
                 wireUpListButtons();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         })
     }

     
    function getCategoryById(id){
        $.get('/user/account/category/get/' + id , function(data){
             if(data.success){
                 renderCategoryElementList(['Name','Type',' '], data.category.elements);
                 renderCategoryInfo(data.category.info);
                 wireUpCategoryElementButtons();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
    }

    function renderCategoryInfo(info){
        $('#categoryName').val(info.name);
        $('#categoryRange').val(info.range);
        $('#categoryVisibility').prop('checked', info.visibility);
    }

    function renderCategoryList(data){
        $('#categoryTable tbody').html("");
        var categories = data.categories;
        $.each(categories, function(index, element){
            var html = prepareHtmlForCategoryListElement(element);
            $('#categoryTable tbody').append(html);
        });
    }

     function prepareHtmlForCategoryListElement(element){       
        var html = "<tr id='" + element.id + "'>";
        html += "<td>" + element.name + "</td>";
        html += "<td>" + element.visibility + "</td>";
        html += "<td>" + element.range + "</td>";
        html += prepareHtmlForCategoryListElementButtons();
        html += "</tr>";
        return html;
     }

     function prepareHtmlForCategoryListElementButtons(){
         var classes = ['glyphicon-signal','glyphicon-remove', 'glyphicon-pencil'];
         var html = "<td>";
         $.each(classes, function(index, element){
            html += "<button class='btn glyphicon " + element + "'/>";
         });

         html += "</td>";
         return html;
     }

     loadCategoryList();

     function wireUpListButtons(){    
        $('#categoryTable .glyphicon-signal').click(function(){
            changeVisibilityPost($(this).closest('tr').attr('id'));
            loadCategoryList();
        });

        $('#categoryTable .glyphicon-remove').click(function(){
            deleteCategoryPost($(this).closest('tr').attr('id'));
            loadCategoryList();
        });

        $('#categoryTable .glyphicon-pencil').click(function(){
            $('#categoriesPopup').show();
            $('#fade').removeClass('hidden');
            var id = $(this).closest('tr').attr('id');
            console.log(id);
            $('#categoryId').val(id);
            var obj = getCategoryById(id);
        });
     }

     $('.categoryAdd').click(function(){
        clearCategoryPopup();
        $('#categoriesPopup').show();
        $('#fade').removeClass('hidden');
     });

     function clearCategoryPopup(){
          $('#categoryId').val("");
          $('#categoryItems tbody').html("");
          renderCategoryElementList(['Name','Type',' '], []);
          $('#categoryName').val("");
            $('#categoryRange').val("");
            $('#categoryVisibility').prop('checked', true);
     }
});
    