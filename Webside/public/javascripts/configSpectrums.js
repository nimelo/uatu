$(document).ready(function() {    

    $('.btn-danger').click(function(){
        $(this).closest('div[class="popup"]').hide();
        $('#fade').addClass('hidden');
        $('#spectrumId').val('');
    });

    function renderSpectrumElementList(fields, elements){
        var html = renderHeader(fields);
        html += renderSpectrumElements(elements);
        html += "</table>";
        $('#spectrumItems').html("");
        $('#spectrumItems').append(html);
    }

    function renderHeader(fields){
        var html = "<table class='table table-hover'>";
        html += "<thead><tr>";
        $.each(fields, function(index, value){
            html += '<th><b>' + value + '</b></th>';
        });
        html += "</tr></thead>";
        return html;
    }

    function renderSpectrumElements(elements){
        var html = "<tbody>";
        $.each(elements, function(index, value){
            html += renderSpectrumElement(value);
        });

        html += "</tbody>"
        return html;
    }

    function renderSpectrumElement(element){
        var html = "<tr ids=" + element.id + ">";
            html += "<td>" + element.name + "</td>";
            html += "<td>" + element.type + "</td>";
            html += '<td><button class="spectrumItemDelete glyphicon glyphicon-remove-circle btn"></button></td>';
            html += "</tr>";
        return html;
    }

    function wireUpSpectrumElementButtons(){
        $('.spectrumItemDelete').click(function(){
                $(this).closest('tr').remove();
        });       
    }

    $('.spectrumItemAdd').click(function(){
        var type = $('#spectrumItemType').val();           
        var value = $('#spectrumItemValue').val();
        var html = renderSpectrumElement({'type':type, 'name':value})
        $('#spectrumItems tbody').append(html);
        wireUpSpectrumElementButtons();
    });

     $('#spectrumAccept').click(function(){
         var spectrumName = $('#spectrumName').val();
         var spectrumVisibility = $('#spectrumVisibility').prop('checked');
         var spectrumRange = $('#spectrumRange').val();
         var spectrumId = $('#spectrumId').val();
         var spectrumGroupBy = $('#spectrumGroupBy').val();

         var spectrumElements = getSpectrumElementsFromTable();

         var obj = {
             info : {
                name : spectrumName,
                visibility : spectrumVisibility,
                range : spectrumRange,
                id :spectrumId,
                groupBy : spectrumGroupBy
             },            
             elements : spectrumElements
         }
         if(spectrumId == '' || typeof spectrumId === 'undefined'){
             addSpectrumPost(obj, function(){
                $('#spectrumAccept').closest('div[class="popup"]').hide();
                $('#fade').addClass('hidden');
                $('#spectrumId').val('');
             }, function(){

             });
         }else{
             modifySpectrumPost(obj, function(){
                $('#spectrumAccept').closest('div[class="popup"]').hide();
                $('#fade').addClass('hidden');
                $('#spectrumId').val('');
             }, function(){
                 
             });
         }
     });

     function getSpectrumElementsFromTable(){
         var obj = new Array();
         var trs = $('#spectrumItems table tr');
         trs.splice(0,1);

         $.each(trs, function(index, element){
             var name = $($("td",element)[0]).text();
             var type = $($("td",element)[1]).text();
             obj.push({'name': name, 'type': type})
         });
         return obj;
     }

     function addSpectrumPost(obj, success, fail){
          $.post('/user/account/spectrum/add', obj, function(data){
             if(data.success){
                 loadSpectrumList();
                 success();
             }else{
                 alert("Error");
                 fail();
             }
            
         }).fail(function(){
             alert('Error');
             fail();
         });
     }

     function modifySpectrumPost(obj, success, fail){
         $.post('/user/account/spectrum/modify', obj, function(data){
            if(data.success){
                 loadSpectrumList();
                 success();
             }else{
                 alert("Error");
                 fail();
             }
            
         }).fail(function(){
             alert('Error');
             fail();
         });    
     }

     function deleteSpectrumPost(id){
         $.post('/user/account/spectrum/delete/' + id , function(data){
             if(data.success){
                 loadSpectrumList();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
     }

     function changeVisibilityPost(id){
         $.post('/user/account/spectrum/changeVisibility' ,{'id': id} , function(data){
             if(data.success){
                 loadSpectrumList();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
     }

     function loadSpectrumList(){
         $.get('/user/account/spectrum/list', function(data){
             if(data.success){
                 renderSpectrumList(data);
                 wireUpListButtons();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         })
     }

     
    function getSpectrumById(id){
        $.get('/user/account/spectrum/get/' + id , function(data){
             if(data.success){
                 renderSpectrumElementList(['Name','Type',' '], data.spectrum.elements);
                 renderSpectrumInfo(data.spectrum.info);
                 wireUpSpectrumElementButtons();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
    }

    function renderSpectrumInfo(info){
        $('#spectrumName').val(info.name);
        $('#spectrumRange').val(info.range);
        $('#spectrumVisibility').prop('checked', info.visibility);
        $('#spectrumGroupBy').val(info.groupBy);
    }

    function renderSpectrumList(data){
        $('#spectrumTable tbody').html("");
        var spectrums = data.spectrums;
        $.each(spectrums, function(index, element){
            var html = prepareHtmlForSpectrumListElement(element);
            $('#spectrumTable tbody').append(html);
        });
    }

     function prepareHtmlForSpectrumListElement(element){       
        var html = "<tr id='" + element.id + "'>";
        html += "<td>" + element.name + "</td>";
        html += "<td>" + element.visibility + "</td>";
        html += "<td>" + element.range + "</td>";
        html += "<td>" + element.groupBy + "</td>";
        html += prepareHtmlForSpectrumListElementButtons();
        html += "</tr>";
        return html;
     }

     function prepareHtmlForSpectrumListElementButtons(){
         var classes = ['glyphicon-signal','glyphicon-remove', 'glyphicon-pencil'];
         var html = "<td>";
         $.each(classes, function(index, element){
            html += "<button class='btn glyphicon " + element + "'/>";
         });

         html += "</td>";
         return html;
     }

     loadSpectrumList();

     function wireUpListButtons(){    
        $('#spectrumTable .glyphicon-signal').click(function(){
            changeVisibilityPost($(this).closest('tr').attr('id'));
            loadSpectrumList();
        });

        $('#spectrumTable .glyphicon-remove').click(function(){
            deleteSpectrumPost($(this).closest('tr').attr('id'));
            loadSpectrumList();
        });

        $('#spectrumTable .glyphicon-pencil').click(function(){
            $('#spectrumsPopup').show();
            $('#fade').removeClass('hidden');
            var id = $(this).closest('tr').attr('id');
            console.log(id);
            $('#spectrumId').val(id);
            var obj = getSpectrumById(id);
        });
     }

     $('.spectrumAdd').click(function(){
        clearSpectrumPopup();
        $('#spectrumsPopup').show();
        $('#fade').removeClass('hidden');
     });

     function clearSpectrumPopup(){
          $('#spectrumId').val("");
          $('#spectrumItems tbody').html("");
          renderSpectrumElementList(['Name','Type',' '], []);
          $('#spectrumName').val("");
          $('#spectrumRange').val("");
          $('#spectrumVisibility').prop('checked', true);
     }
});
    