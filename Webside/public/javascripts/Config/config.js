exports.appKey = "!@#$%";    
exports.authCheckUrl = "http://127.0.0.1:10000/auth/check";      
exports.authLoginUrl = "http://127.0.0.1:10000/auth/login"; 
exports.configuration = {};
exports.configuration.category = {};
exports.configuration.category.getListUrl = "http://127.0.0.1:10002/configuration/category/getList";
exports.configuration.category.get = "http://127.0.0.1:10002/configuration/category/get";     
exports.configuration.category.add = "http://127.0.0.1:10002/configuration/category/add";  
exports.configuration.category.delete = "http://127.0.0.1:10002/configuration/category/delete";  
exports.configuration.category.modify = "http://127.0.0.1:10002/configuration/category/modify";  
exports.configuration.category.change = "http://127.0.0.1:10002/configuration/category/changeVisibility";

exports.configuration.spectrum = {};
exports.configuration.spectrum.getListUrl = "http://127.0.0.1:10002/configuration/spectrum/getList";
exports.configuration.spectrum.get = "http://127.0.0.1:10002/configuration/spectrum/get";     
exports.configuration.spectrum.add = "http://127.0.0.1:10002/configuration/spectrum/add";  
exports.configuration.spectrum.delete = "http://127.0.0.1:10002/configuration/spectrum/delete";  
exports.configuration.spectrum.modify = "http://127.0.0.1:10002/configuration/spectrum/modify";  
exports.configuration.spectrum.change = "http://127.0.0.1:10002/configuration/spectrum/changeVisibility";

exports.configuration.appNaming = {};
exports.configuration.appNaming.getListUrl = "http://127.0.0.1:10002/configuration/appNaming/getList";    
exports.configuration.appNaming.add = "http://127.0.0.1:10002/configuration/appNaming/add";  
exports.configuration.appNaming.delete = "http://127.0.0.1:10002/configuration/appNaming/delete";  

exports.category = {};
exports.category.getUrl = "http://127.0.0.1:10001/statistics/get";       

exports.spectrum = {};
exports.spectrum.getUrl = "http://127.0.0.1:10001/statistics/group";  

exports.histories = {};
exports.histories.getUrl = "http://127.0.0.1:10001/histories/get";

exports.hardwares = {};
exports.hardwares.getFromTimeAll = "http://127.0.0.1:10001/histories/hardwares";

exports.daily = {};
exports.daily.getUrl = "http://127.0.0.1:10001/statistics/daily";
                   