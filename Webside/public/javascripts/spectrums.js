$(function(){
       function getVisibleSpectrums(){
        $.get('/user/account/spectrum/list', function(data){
             if(data.success){
                $.get('/user/account/appNaming/list', function(namings){
                    if(namings.success){
                        var spectrums = data.spectrums;
                        $.each(spectrums, function(index, element){
                            createContainerFor(element);
                            renderChart(element, namings.appNamings);
                        });
                    }else{
                        alert("Error");
                    }
                    
                    }).fail(function(){
                        alert('Error');
                    })                  
             }else{
                 alert("Error");
             }           
         }).fail(function(){
             alert('Error');
         })               
    }

    function createContainerFor(element){
        var html = "<div class='col-md-6'>";
        html += "<div id='" + element.id +"' class='chart'/><div/>";
        $('#charts').append(html);
    }

    function renderChart(element, namings){
        getSpectrumById(element, function(data){
             getSpectrumData(data, element, function(body){
                 $.each(body.data, function(index, value){
                       $.each(namings, function(index2, name){
                            if(value.name == name.path){
                                value.name = name.name
                            }
                       }); 
                  });
                  ChartsRendering.renderColumn('#' + element.id , body.data, body.fields, element.name, "For last " + element.range + " days");
             });
        });      
    }

   function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
   }

   function getSpectrumData(data, element, callback){
       var now = addDays(new Date(), -element.range);
       var obj = {
           from : {
               Day : now.getDate(),
               Month : now.getMonth() + 1,
               Year : now.getFullYear()
           },
           range : element.range,
           elements : data,
           groupBy : element.groupBy
       };

       $.post('/user/spectrums/spectrum/get', obj ,function(data){
            if(data.success){
                callback(data);
            }else{
                alert("Error");
            }
        
        }).fail(function(){
            alert('Error');
        });
   }

    function getSpectrumById(element, callback){
        $.get('/user/account/spectrum/get/' + element.id , function(data){
             if(data.success){
                 callback(data.spectrum.elements);
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
    }

    getVisibleSpectrums();

});
