$(document).ready(function() {    

    $('.btn-danger').click(function(){
        $(this).closest('div[class="popup"]').hide();
        $('#fade').addClass('hidden');
        $('#appNamingId').val('');
    });

    function renderAppNamingElementList(fields, elements){
        var html = renderHeader(fields);
        html += renderAppNamingElements(elements);
        html += "</table>";
        $('#appNamingItems').html("");
        $('#appNamingItems').append(html);
    }

    function renderHeader(fields){
        var html = "<table class='table table-hover'>";
        html += "<thead><tr>";
        $.each(fields, function(index, value){
            html += '<th><b>' + value + '</b></th>';
        });
        html += "</tr></thead>";
        return html;
    }

    function renderAppNamingElements(elements){
        var html = "<tbody>";
        $.each(elements, function(index, value){
            html += renderAppNamingElement(value);
        });

        html += "</tbody>"
        return html;
    }

    function renderAppNamingElement(element){
        var html = "<tr ids=" + element.id + ">";
            html += "<td>" + element.name + "</td>";
            html += "<td>" + element.type + "</td>";
            html += '<td><button class="appNamingItemDelete glyphicon glyphicon-remove-circle btn"></button></td>';
            html += "</tr>";
        return html;
    }

    function wireUpAppNamingElementButtons(){
        $('.appNamingItemDelete').click(function(){
                $(this).closest('tr').remove();
        });       
    }

    $('.appNamingItemAdd').click(function(){
        var type = $('#appNamingItemType').val();           
        var value = $('#appNamingItemValue').val();
        var html = renderAppNamingElement({'type':type, 'name':value})
        $('#appNamingItems tbody').append(html);
        wireUpAppNamingElementButtons();
    });

     $('#appNamingAccept').click(function(){
         var appNamingName = $('#appNamingName').val();
         var appNamingPath = $('#appNamingPath').val();

         var obj = {
             name : appNamingName,
             path : appNamingPath
        }
        addAppNamingPost(obj, function(){
            $('#appNamingAccept').closest('div[class="popup"]').hide();
            $('#fade').addClass('hidden');
            $('#appNamingId').val('');
        }, function(){

        });         
     });

     function addAppNamingPost(obj, success, fail){
          $.post('/user/account/appNaming/add', obj, function(data){
             if(data.success){
                 loadAppNamingList();
                 success();
             }else{
                 alert("Error");
                 fail();
             }
            
         }).fail(function(){
             alert('Error');
             fail();
         });
     }

     function deleteAppNamingPost(id){
         $.post('/user/account/appNaming/delete/' + id , function(data){
             if(data.success){
                 loadAppNamingList();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         });
     }

     function loadAppNamingList(){
         $.get('/user/account/appNaming/list', function(data){
             if(data.success){
                 renderAppNamingList(data);
                 wireUpListButtons();
             }else{
                 alert("Error");
             }
            
         }).fail(function(){
             alert('Error');
         })
     }

    function renderAppNamingList(data){
        $('#appNamingTable tbody').html("");
        var appNamings = data.appNamings;
        $.each(appNamings, function(index, element){
            var html = prepareHtmlForAppNamingListElement(element);
            $('#appNamingTable tbody').append(html);
        });
    }

     function prepareHtmlForAppNamingListElement(element){       
        var html = "<tr id='" + element.id + "'>";
        html += "<td>" + element.name + "</td>";
        html += "<td>" + element.path + "</td>";
        html += prepareHtmlForAppNamingListElementButtons();
        html += "</tr>";
        return html;
     }

     function prepareHtmlForAppNamingListElementButtons(){
         var classes = ['glyphicon-remove'];
         var html = "<td>";
         $.each(classes, function(index, element){
            html += "<button class='btn glyphicon " + element + "'/>";
         });

         html += "</td>";
         return html;
     }

     loadAppNamingList();

     function wireUpListButtons(){    
        $('#appNamingTable .glyphicon-remove').click(function(){
            deleteAppNamingPost($(this).closest('tr').attr('id'));
            loadAppNamingList();
        });
     }

     $('.appNamingAdd').click(function(){
        clearAppNamingPopup();
        $('#appNamingPopup').show();
        $('#fade').removeClass('hidden');
     });

     function clearAppNamingPopup(){
          $('#appNamingName').val("");
          $('#appNamingPath').val("");
     }
});
    