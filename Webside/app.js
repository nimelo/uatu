var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var authentication = require('./public/javascripts/Authentication/authentication.js')

var routes = require('./routes/index');
var users = require('./routes/users');
var categories = require('./routes/categories');
var spectrums = require('./routes/spectrums');
var histories = require('./routes/histories');
var daily = require('./routes/daily');
var hardwares = require('./routes/hardwares');
var about = require('./routes/about');
var contact = require('./routes/contact');
var login = require('./routes/login');
var logout = require('./routes/logout');
var account = require('./routes/account');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('*', function (req, res, next) {
  if(typeof req.cookies.userName !== 'undefined'){
    res.locals.user = {
                name : req.cookies.userName
    };
  }  
  
  next();
 
});

app.use('/user/*', function (req, res, next) {
  var date = req.cookies.userDate;
  var key = req.cookies.userKey;
  if(!date || !key){
      res.redirect('/');
  }else{
    authentication.isCorrect(
    date,
    key,
    function(){
        res.locals.user = {
                name : req.cookies.userName
        };
        next();
    }, 
    function(){      
        res.clearCookie("userDate");
        res.clearCookie("userKey");
        res.clearCookie("userName");
        res.locals.user = 'undefined';
        res.redirect('/');
    }); 
  }
  
});

app.use('/', routes);
app.use('/user/categories', categories);
app.use('/user/spectrums', spectrums);
app.use('/user/account', account);
app.use('/user/histories', histories);
app.use('/user/daily', daily);
app.use('/about', about);
app.use('/contact', contact);
app.use('/login', login);
app.use('/user/logout', logout);
app.use('/user/hardwares', hardwares);
/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
