﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPUTemperatureObserver
{
    [Serializable]
    public class CPUTemp : MarshalByRefObject
    {
        public Dictionary<string, float> Temperatures { get; set; }

        public CPUTemp(Dictionary<string, float> temps)
        {
            this.Temperatures = temps;
        }
    }
}
