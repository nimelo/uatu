﻿using OpenHardwareMonitor.Collections;
using OpenHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPUTemperatureObserver
{
    class Program
    {
        private static void Main(string[] args)
        {
            var myComputer = new Computer();

            myComputer.CPUEnabled = true;        
            myComputer.Open();

            foreach (var hardwareItem in myComputer.Hardware)
            {
                hardwareItem.Update();
                hardwareItem.GetReport();

                Console.WriteLine(hardwareItem.GetReport());

                foreach (var sensor in hardwareItem.Sensors)
                {
                    if (sensor.SensorType == SensorType.Temperature)
                    {
                        Console.WriteLine("{0} {1} {2} = {3}", sensor.Name, sensor.Hardware, sensor.SensorType, sensor.Value);

                    }

                }
            }
        }
    }

}
