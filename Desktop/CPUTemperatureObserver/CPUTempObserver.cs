﻿using Common.Observer.Periodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.ExtensionMethods;
using OpenHardwareMonitor.Hardware;
using Common.Observer;
using System.ComponentModel.Composition;
using System.Reflection;
using Common.Observer.HardwareObserver;

namespace CPUTemperatureObserver
{
    [Export(typeof(AbstractObserver))]
    public class CPUTempObserver : AbstractHardwareObserver
    {
        #region Overrided methods       
        public override string Name
        {
            get
            {
                return nameof(CPUTempObserver);
            }
        }

        protected override HardwareQueryResult Query(ElapsedEventArgs elapsed)
        {
            return new HardwareQueryResult(GetTemperaturesFrom(GetTemperatureSensors()), elapsed.SignalTime.ToUnixUtc(), (long)this.QueryFrequency);
        }
        #endregion

        #region Properties
        public Computer Computer { get; protected set; }
        #endregion

        #region Ctors
        public CPUTempObserver()
        {
            this.QueryFrequency = 15 * 1000;
            this.Computer = new Computer();
            this.Computer.CPUEnabled = true;
            this.Computer.Open();
        }
        #endregion

        #region Private methods
        private IEnumerable<ISensor> GetTemperatureSensors()
        {
            foreach (var hardwareItem in this.Computer.Hardware)
            {
                hardwareItem.Update();

                foreach (var sensor in hardwareItem.Sensors)
                {
                    if (sensor.SensorType == SensorType.Temperature)
                    {
                        yield return sensor;
                    }
                }
            }
        }

        private Dictionary<string, double> GetTemperaturesFrom(IEnumerable<ISensor> sensors)
        {
            var dictionary = new Dictionary<string, double>();

            foreach (var sensor in sensors)
            {
                dictionary.Add(sensor.Name, sensor.Value ?? 0f);
            }

            return dictionary;
        }
        #endregion
    }
}
