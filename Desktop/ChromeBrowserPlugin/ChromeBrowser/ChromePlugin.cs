﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Automation;
using Common.EventArgs;
using Common.ProgramPlugin;
using Common.Raports;
using ChromeBrowser.Raport;

namespace ChromeBrowser
{
    public class ChromePlugin : AbstractProgramPlugin<ChromePluginContainer>
    {
        #region Public methods
        public override string GetName()
        {
            return nameof(ChromePlugin);
        }

        public override uint GetTimePeriod()
        {
            return 1000;
        }

        public override string ServiceName()
        {
            return "webside";
        }

        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == MachineStateEnum.IDLE)
            {
                this.CanHandle = false;
                var lastSwap = this.CurrentObject.ClearValue();
#if (DEBUG)
                Console.WriteLine($"{lastSwap}");
#endif
            }
            else if (e.State == MachineStateEnum.RESUME)
            {
                this.CanHandle = true;
            }
        }

        #endregion

        #region Protected methods
        protected override void Handle(ElapsedEventArgs e)
        {
            IntPtr handle = this.GetForegroundWindowHandle();
            uint pId = this.GetWindowThreadProcessId(handle);
            string path = this.GetExecutionPathFrom(pId);

            if (this.Paths.Contains(path))
            {
                this.HandleBrowserUrl(pId);
            }
            else
            {
                if (!this.CurrentObject.IsEmpty())
                {
                    var lastSwap = this.CurrentObject.ClearValue();
                  
#if (DEBUG)
                    Console.WriteLine($"Was:{lastSwap.Value.Url} from:{lastSwap.From} to:{lastSwap.To}");
#endif
                }
            }
        }

        protected virtual void HandleBrowserUrl(uint pId)
        {
            var url = this.ExtractBrowserUrl(pId);
            if (this.CurrentObject.IsEmpty())
            {
                this.CurrentObject.SetValue(new ChromePluginContainer(url));
            }
            else
            {
                if (!this.CurrentObject.Equals(url))
                {
                    var lastSwap = this.CurrentObject.SwapValue(new ChromePluginContainer(url));
#if (DEBUG)
                    Console.WriteLine($"Was:{lastSwap.Value.Url} from:{lastSwap.From} to:{lastSwap.To}");
#endif
                }
            }
        }

        private string ExtractBrowserUrl(uint pId)
        {
            Process process = Process.GetProcessById((int)pId);
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            AutomationElement edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
            if (edit == null)
                return null;
            return ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
        }

        #endregion

        #region Private methods
        private string GetExecutionPathFrom(uint pId)
        {
            var process = Process.GetProcessById((int)pId);
            return process.Modules[0].FileName;
        }
        #endregion

        #region Properties
        public IEnumerable<string> Paths { get; set; }
        #endregion
        public ChromePlugin()
        {
            this.Paths = new List<string>()
            {
                @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe",
                @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
            };
        }
    }
}
