﻿namespace ChromeBrowser
{
    public class ChromePluginContainer
    {
        public string Url { get; set; }

        public ChromePluginContainer(string url)
        {
            this.Url = url;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is string)
            {
                return this.Url == (string)obj;
            }

            return base.Equals(obj);
        }

        public override string ToString()
        {
            return Url;
        }
    }
}