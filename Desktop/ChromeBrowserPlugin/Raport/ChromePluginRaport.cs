﻿using Common.Raports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChromeBrowser.Raport
{
    public class ChromePluginRaport
    {

        #region Properties
        public ChromePluginHistory history { get; set; }
        public ChromePluginStatistics statistics { get; set; }
        #endregion

        #region Ctors
        public ChromePluginRaport()
        {
            this.history = new ChromePluginHistory();
            this.statistics = new ChromePluginStatistics();
        }
        #endregion

        #region Methods
        public void AddObject(string obj, DateTime from, DateTime to)
        {
            this.history.AddObject(obj, from, to);
            this.statistics.AddObject(obj, from, to);
        }
        #endregion
    }
}
