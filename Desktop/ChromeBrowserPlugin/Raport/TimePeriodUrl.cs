﻿using System;

namespace ChromeBrowser.Raport
{
    public class TimePeriodUrl
    {
        public TimePeriodUrl()
        {
        }

        public long from { get; set; }
        public long to { get; set; }
        public string url { get; set; }
    }
}