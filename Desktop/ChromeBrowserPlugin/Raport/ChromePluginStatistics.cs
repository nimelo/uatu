﻿using System;
using System.Collections.Generic;

namespace ChromeBrowser.Raport
{
    public class ChromePluginStatistics
    {
        public void AddObject(string obj, DateTime from, DateTime to)
        {
            var host = GetHostFromUrl(obj);
            if (this.statistics.Keys.Contains(host))
            {
                this.statistics[host] += (uint)(to - from).Seconds;
            }
            else
            {
                this.statistics.Add(host, (uint)(to - from).Seconds);
            }
        }

        public object Get()
        {
            return this.statistics;
        }

        public IDictionary<string, uint> statistics { get; set; }

        public ChromePluginStatistics()
        {
            this.statistics = new Dictionary<string, uint>();
        }

        private string GetHostFromUrl(string url)
        {
            string domain = string.Empty;
            if (url.IndexOf("://") > -1)
            {
                domain = url.Split('/')[2];
            }
            else
            {
                domain = url.Split('/')[0];
            }

            return domain.Split(':')[0];
        }
    }
}