﻿using System;
using System.Collections.Generic;

namespace ChromeBrowser.Raport
{
    public class ChromePluginHistory
    {
        #region IHistory
        public void AddObject(string obj, DateTime from, DateTime to)
        {
            var host = this.GetHostFromUrl(obj);
            if (this.history.Keys.Contains(host))
            {
                this.history[host].Add(new TimePeriodUrl()
                {
                    from = new DateTimeOffset(from).ToUnixTimeSeconds(),
                    to = new DateTimeOffset(to).ToUnixTimeSeconds(),
                    url = obj.Substring(obj.LastIndexOf(host))
                });
            }
            else
            {
                this.history.Add(host, new List<TimePeriodUrl>()
                {
                    new TimePeriodUrl()
                    {
                        from = new DateTimeOffset(from).ToUnixTimeSeconds(),
                        to = new DateTimeOffset(to).ToUnixTimeSeconds(),
                        url = obj.Substring(obj.LastIndexOf(host))
                    }
                });
            }
        }

        public IDictionary<string, List<TimePeriodUrl>> Get()
        {
            return this.history;
        }
        #endregion

        #region Properties
        public IDictionary<string, List<TimePeriodUrl>> history { get; set; }
        #endregion

        #region Ctors
        public ChromePluginHistory()
        {
            this.history = new Dictionary<string, List<TimePeriodUrl>>();
        }
        #endregion

        private string GetHostFromUrl(string url)
        {
            string domain = string.Empty;
            if(url.IndexOf("://") > -1)
            {
                domain = url.Split('/')[2];
            }
            else
            {
                domain = url.Split('/')[0];
            }

            return domain.Split(':')[0];
            //return new Uri(url).Host;
        }
    }
}