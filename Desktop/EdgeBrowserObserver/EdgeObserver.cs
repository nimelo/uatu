﻿using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EventArgs;
using System.Timers;
using Common.ExtensionMethods;
using System.Reflection;

namespace EdgeBrowserObserver
{
    public class EdgeObserver : AbstractNonPeriodicalEventsObserver<NonPeriodicalQueryResult<string>, string, string>
    {
        public override string Name
        {
            get
            {
                return nameof(EdgeObserver);
            }
        }

        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == Common.MachineStateEnum.IDLE)
            {
                this.Stop();
            }
            else if (e.State == Common.MachineStateEnum.RESUME)
            {
                this.Start();
            }
        }

        protected override string QueryObservedKey(ElapsedEventArgs elapsed)
        {
            throw new NotImplementedException();
        }

        protected override string QueryObservedValue(string key, ElapsedEventArgs elapsed)
        {
            throw new NotImplementedException();
        }

        public override void Stop()
        {
            if (this.Object.HasValue)
            {
                var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                this.Object.ClearObject();
                OnQueryOccured(new NonPeriodicalQueryResult<string>(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime));
            }
            base.Stop();
        }

        protected override void Observe(ElapsedEventArgs elapsed)
        {
          /*  if (ChromeQueryHelper.GetExecutionPath().Contains(@"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"))
            {
                base.Observe(elapsed);
            }
            else
            {
                if (this.Object.HasValue)
                {
                    var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                    this.Object.ClearObject();
                    OnQueryOccured(new NonPeriodicalQueryResult<string>(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime - endedEvent.StartTime));
                }
            }*/
        }
    }
}
