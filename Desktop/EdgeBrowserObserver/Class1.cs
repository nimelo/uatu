﻿#region Using directives
using System;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;
#endregion

/// <summary>
/// A sample application for capturing current Internet Explorer windows' URLs.
/// By Stanimir Stoyanov, www.stoyanoff.info
/// 
/// NOTE: Tested with Internet Explorer 6 only.
/// </summary>
class IEURLCrawler
{
    #region Constants
    // Define the necessary constants.
    private const string IEClassName = "IEFrame";
    private static readonly IntPtr WM_GETTEXT = new IntPtr(13);
    #endregion

    #region Entry Point
    [STAThread]
    static void Main(string[] args)
    {
        IntPtr hWnd = IntPtr.Zero;

        // Initialize the Windows Enumerating Filter: IE Frame Windows only.
        SafeNativeMethods.EnumerateWindowsFilters enumWindFilter;
        enumWindFilter.inCaption = null;
        enumWindFilter.inClass = IEClassName;
        enumWindFilter.windowHandles = new ArrayList();

        SafeNativeMethods.EnumWindows(new SafeNativeMethods.EnumWindowsProc(WindowEnum), ref enumWindFilter);

        // The IE Window Hierarchy is: (NOTE: IE6-tested only!)
        // IEFrame -> WorkerW -> ReBarWindow32 -> ComboBoxEx32 (-> ComboBox -> Edit).

        while (true)
        {
            foreach (IntPtr chWnd in enumWindFilter.windowHandles)
            {
                hWnd = SafeNativeMethods.FindWindowEx(chWnd, IntPtr.Zero, "WorkerW", null);
                if (hWnd != IntPtr.Zero)
                {
                    hWnd = SafeNativeMethods.FindWindowEx(hWnd, IntPtr.Zero, "ReBarWindow32", null);
                    if (hWnd != IntPtr.Zero)
                    {
                        hWnd = SafeNativeMethods.FindWindowEx(hWnd, IntPtr.Zero, "Address Band Root", null);

                        if (hWnd != IntPtr.Zero)
                        {
                            hWnd = SafeNativeMethods.FindWindowEx(hWnd, IntPtr.Zero, "Edit", null);
                            if (hWnd != IntPtr.Zero)
                            {
                                // Get ahold of the URL...
                                StringBuilder sb = new StringBuilder(256);
                                SafeNativeMethods.SendMessage(hWnd, WM_GETTEXT, new IntPtr(sb.Capacity), sb);

                                // ... and output it.
                                Console.WriteLine(sb.ToString());
                            }
                        }

                    }
                }
            }
            System.Threading.Thread.Sleep(1000);
            Console.Clear();
        }

        // Clean up.
        enumWindFilter.windowHandles.Clear();
        enumWindFilter.windowHandles = null;

        // Let the Console Window hold on.
        Console.WriteLine("\r\nPress Return to exit.");
        Console.Read();
    }
    #endregion

    #region Window Enumerating Procedure.
    // The Window Enumerating procedure (filter non-IE windows).
    private static int WindowEnum(
        IntPtr hWnd,
        ref SafeNativeMethods.EnumerateWindowsFilters lParam)
    {
        StringBuilder sb = new StringBuilder(256);
        SafeNativeMethods.GetClassName(hWnd, sb, sb.Capacity);

        if (lParam.inClass != null)
        {
            if (sb.ToString() == lParam.inClass)
                lParam.windowHandles.Add(hWnd);
        }
        else
        {
            lParam.windowHandles.Add(hWnd);
        }
        return 1;
    }
    #endregion

    #region Win32
    // This Class defines the Win32 P/Invoke functions that we will use.
    class SafeNativeMethods
    {
        private const string User32 = "user32.dll";

        [StructLayout(LayoutKind.Sequential)]
        public struct EnumerateWindowsFilters
        {
            public IList windowHandles;
            public string inCaption;
            public string inClass;
        }

        [DllImport(User32, EntryPoint = "GetClassNameW", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr GetClassName(
            IntPtr hWnd,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpClassName,
            int nMaxCount
          );

        [DllImport(User32, EntryPoint = "SendMessageW", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr SendMessage(
            IntPtr hWnd,
            IntPtr Msg,
            IntPtr wParam,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder lParam
          );

        [DllImport(User32)]
        public static extern IntPtr FindWindowEx(
            IntPtr hWndParent,
            IntPtr hWndChild,
            string WndClass,
            string WndTitle
            );

        [DllImport(User32)]
        public extern static int EnumWindows(
            EnumWindowsProc lpEnumFunc,
            ref EnumerateWindowsFilters lParam
            );

        public delegate int EnumWindowsProc(
            IntPtr hwnd,
            ref SafeNativeMethods.EnumerateWindowsFilters lParam
            );
    }
    #endregion
}