﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ExtensionMethods
{
    public static class DateTimeExtensionMethods
    {
        public static long ToUnixUtc(this DateTime time)
        {
            return new DateTimeOffset(time).ToUnixTimeMilliseconds();
        }
    }
}
