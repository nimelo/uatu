﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum MachineStateEnum
    {
        IDLE,
        RESUME,
        SUSPEND,
        STATUS_CHANGE,
        SESSION_ENDING,
        SESSION_END_SHUTDOWN,
        SESSION_END_LOGOFF
    }
}
