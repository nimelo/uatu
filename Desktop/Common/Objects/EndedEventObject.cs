﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Objects
{
    public sealed class EndedEventObject<KeyType, ValueType>
    {
        public EndedEventObject(NonPeriodicalEventObject<KeyType, ValueType> obj, long endTime)
        {
            this.Key = obj.Key;
            this.Value = obj.Value;
            this.StartTime = obj.Time.Value;
            this.EndTime = endTime;
        }

        public long EndTime { get; private set; }
        public KeyType Key { get; private set; }
        public long StartTime { get; private set; }
        public ValueType Value { get; private set; }
    }
}
