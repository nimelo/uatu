﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Objects
{
    public class NonPeriodicalEventObject<KeyType, ValueType>
    {
        #region Properties
        public KeyType Key { get; protected set; }
        public ValueType Value { get; protected set; }
        public bool HasValue { get; protected set; }
        public long? Time { get; protected set; }
        #endregion

        #region Ctors
        public NonPeriodicalEventObject(KeyType key, ValueType value, long time)
        {
            this.SetValue(key, value, time);
        }

        public NonPeriodicalEventObject()
        {
            this.HasValue = false;
        }
        #endregion

        #region Public methods
        public void SetValue(KeyType key, ValueType value, long time)
        {
            this.Key = key;
            this.Value = value;
            this.Time = time;
            this.HasValue = true;
        }

        public EndedEventObject<KeyType, ValueType> RegisterEndEvent(long time)
        {
            return new EndedEventObject<KeyType, ValueType>(this, time);
        }

        public void ClearObject()
        {
            this.HasValue = false; 
        }
        #endregion

    }
}
