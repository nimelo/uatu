﻿using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EventArgs;
using System.Timers;
using Common.ExtensionMethods;

namespace Common.Observer.BrowserObserver
{
    public abstract class AbstractBrowserObserver : AbstractNonPeriodicalEventsObserver<BrowserQueryResult, string, string>
    {
        #region Overrided methods
        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == Common.MachineStateEnum.IDLE)
            {
                if (this.Object.HasValue)
                {
                    if (!this.IdleExeceptionUrls.Any(x => x.StartsWith(this.Object.Key)))
                    {
                        this.Stop();
                    }
                }
            }
            else if (e.State == Common.MachineStateEnum.RESUME)
            {
                this.Start();
            }
        }

        public override void Stop()
        {
            if (this.Object.HasValue)
            {
                var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                this.Object.ClearObject();
                BrowserQueryResult obj = this.GetQueryResultFromEndedEvent(endedEvent);
                OnQueryOccured(obj);
            }
            base.Stop();
        }

        protected override string QueryObservedValue(string key, ElapsedEventArgs elapsed)
        {
            return key;
        }
        #endregion

        #region Properties
        private List<string> _executablePaths;

        public List<string> ExecutablePaths
        {
            get { return _executablePaths; }
            set { _executablePaths = value; }
        }

        private List<string> _idleExecptionsUrls;

        public List<string> IdleExeceptionUrls
        {
            get { return _idleExecptionsUrls; }
            set { _idleExecptionsUrls = value; }
        }       
        #endregion

        #region Public methods
        public abstract string BrowserName { get; }
        #endregion

        #region Ctors
        public AbstractBrowserObserver()
        {
            this.ExecutablePaths = new List<string>();
            this.IdleExeceptionUrls = new List<string>();
        }
        #endregion
    }
}
