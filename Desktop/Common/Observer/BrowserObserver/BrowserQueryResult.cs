﻿namespace Common.Observer.BrowserObserver
{
    public class BrowserQueryResult : NonPeriodical.NonPeriodicalQueryResult<string>
    {
        public BrowserQueryResult(string value, long from, long time) : base(value, from, time)
        {
        }
    }
}