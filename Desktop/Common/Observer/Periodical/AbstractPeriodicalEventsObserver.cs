﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.EventArgs;

namespace Common.Observer.Periodical
{
    public abstract class AbstractPeriodicalEventsObserver<ObservedValue, QueryResult> : AbstractObserver where QueryResult : PeriodicalQueryResult<ObservedValue>
    {
        #region Override methods
        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            
        }

        protected override void Observe(ElapsedEventArgs elapsed)
        {
            QueryResult queryResult = this.Query(elapsed);
            this.OnQueryOccured(queryResult);
        }
        #endregion

        protected abstract QueryResult Query(ElapsedEventArgs elapsed);
    }
}
