﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Observer.Periodical
{
    public class PeriodicalQueryResult<T> : AbstractQueryResult
    {
        public T Value { get; set; }

        public PeriodicalQueryResult(T value, long time) : base(time)
        {
            this.Value = value;
        }
    }
}
