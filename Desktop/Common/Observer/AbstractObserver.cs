﻿using Common.EventArgs;
using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Timers;

namespace Common.Observer
{
    public abstract class AbstractObserver : MarshalByRefObject
    {
        #region Events
        public event EventHandler<ObservationError> ErrorOccured;
        public event EventHandler<AbstractQueryResult> QueryOccured;
        #endregion

        #region OnEvents
        public abstract void OnMachineStateEvent(object source, DetectorEventArgs e);
        #endregion

        #region Protected methods
        protected virtual void OnErrorOccured(ObservationError e)
        {
            ErrorOccured?.Invoke(this, e);
        }

        protected virtual void OnQueryOccured(AbstractQueryResult e)
        {
            QueryOccured?.Invoke(this, e);
        }

        protected virtual void OnTimerElapsed(object sender, ElapsedEventArgs elapsed)
        {
            try
            {
                this.Observe(elapsed);
            }
            catch (Exception e)
            {
                OnErrorOccured(new ObservationError(e));
            }
        }

        protected abstract void Observe(ElapsedEventArgs elapsed);
        #endregion

        #region Public methods
        public virtual void Start()
        {
            if(this.Timer == null)
            {
                this.Timer = new Timer(this.QueryFrequency);
                this.Timer.Elapsed += OnTimerElapsed;
            }

            if (!this.IsRunning)
            {
                this.Timer.Start();
                this.IsRunning = true;
            }
        }

        public virtual void Stop()
        {
            if (this.IsRunning)
            {
                this.Timer.Stop();
                this.IsRunning = false;
            }
        }
        #endregion

        #region Fields
        private double _queryFrequency;
        #endregion

        #region Properties

        public virtual double QueryFrequency
        {
            get
            {
                return _queryFrequency;
            }

            protected set
            {
                _queryFrequency = value;
            }
        }

        public abstract string Name { get; }

        public virtual string Version
        {
            get { return "undefined"; }
        }

        protected Timer Timer { get; set; }

        public bool IsRunning { get; protected set; }
        #endregion

        #region Ctors
        public AbstractObserver()
        {
            this.QueryFrequency = 1000f;
            this.IsRunning = false;
        }

        public AbstractObserver(Timer timer)
        {
            this.Timer = timer;
            this.Timer.Elapsed += OnTimerElapsed;
            this.QueryFrequency = (uint)this.Timer.Interval;
            this.IsRunning = false;
        }
        #endregion
    }
}
