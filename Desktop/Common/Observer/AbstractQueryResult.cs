﻿using System;

namespace Common.Observer
{
    public abstract class AbstractQueryResult : MarshalByRefObject
    {
        public long From { get; set; }

        public virtual object ToJSONObject()
        {
            return this;
        }

        public AbstractQueryResult(long from)
        {
            this.From = from;
        }
    }
}