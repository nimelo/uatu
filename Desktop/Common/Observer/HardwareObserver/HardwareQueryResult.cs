﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Observer.HardwareObserver
{
    public class HardwareQueryResult : Periodical.PeriodicalQueryResult<Dictionary<string, double>>
    {
        public long Frequency { get; protected set; }
        public HardwareQueryResult(Dictionary<string, double> dictionary, long time, long freq) : base(dictionary, time)
        {
            this.Frequency = freq;
        }
    }
}
