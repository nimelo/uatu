﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.Observer.Periodical;

namespace Common.Observer.HardwareObserver
{
    public abstract class AbstractHardwareObserver : AbstractPeriodicalEventsObserver<Dictionary<string, double>, HardwareQueryResult>
    {       
    }
}
