﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.EventArgs;
using Common.Observer.NonPeriodical;
using Common.ExtensionMethods;

namespace Common.Observer.ApplicationObserver
{
    public abstract class AbstractApplicationObserver : AbstractNonPeriodicalEventsObserver<ApplicationObserverQueryResult, string, string>
    {
        #region Overrided methods
        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == Common.MachineStateEnum.IDLE)
            {
                if (this.Object.HasValue)
                {
                    if (!this.IdleExceptions.Contains(this.Object.Value))
                    {
                        this.Stop();
                    }
                }
            }
            else if (e.State == Common.MachineStateEnum.RESUME)
            {
                this.Start();
            }
        }

        public override void Stop()
        {
            if (this.Object.HasValue)
            {
                var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                this.Object.ClearObject();
                ApplicationObserverQueryResult obj = this.GetQueryResultFromEndedEvent(endedEvent);
                OnQueryOccured(obj);
            }
            base.Stop();
        }
        #endregion

        #region Properties
        private List<string> _idleExeceptions;

        public List<string> IdleExceptions
        {
            get { return _idleExeceptions; }
            set { _idleExeceptions = value; }
        }

        #endregion

        #region Ctors
        public AbstractApplicationObserver()
        {
            this.IdleExceptions = new List<string>();
        }
        #endregion
    }
}
