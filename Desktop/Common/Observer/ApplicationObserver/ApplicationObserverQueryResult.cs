﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Observer.ApplicationObserver
{
    public class ApplicationObserverQueryResult : NonPeriodical.NonPeriodicalQueryResult<string>
    {
        public ApplicationObserverQueryResult(string value, long from, long period) : base(value, from, period)
        {
        }
    }
}
