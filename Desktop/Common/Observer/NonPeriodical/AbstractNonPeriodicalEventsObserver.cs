﻿using System;
using System.Timers;
using Common.EventArgs;
using Common.Objects;
using Common.ExtensionMethods;

namespace Common.Observer.NonPeriodical
{
    public abstract class AbstractNonPeriodicalEventsObserver<QueryResult, ObservedValue, ObservedValueKey>: AbstractObserver where QueryResult : AbstractQueryResult
    {
        #region Overrided methods
        protected override void Observe(ElapsedEventArgs elapsed)
        {
            ObservedValueKey key = this.QueryObservedKey(elapsed);

            if (!this.Object.HasValue)
            {
                this.Object.SetValue(key, this.QueryObservedValue(key, elapsed), elapsed.SignalTime.ToUnixUtc());
            }
            else
            {
                if (!this.Object.Key.Equals(key))
                {                   
                    EndedEventObject<ObservedValueKey, ObservedValue> endedEvent = this.Object.RegisterEndEvent(elapsed.SignalTime.ToUnixUtc());
                    this.Object.ClearObject();
                    QueryResult queryResult = this.GetQueryResultFromEndedEvent(endedEvent);
                    OnQueryOccured(queryResult);
                }
            }
        }
        #endregion

        #region Protected methods
        protected abstract QueryResult GetQueryResultFromEndedEvent(EndedEventObject<ObservedValueKey, ObservedValue> endedEvent);

        protected abstract ObservedValue QueryObservedValue(ObservedValueKey key, ElapsedEventArgs elapsed);

        protected abstract ObservedValueKey QueryObservedKey(ElapsedEventArgs elapsed);
        #endregion

        #region Properties
        protected NonPeriodicalEventObject<ObservedValueKey, ObservedValue> Object { get; set; }
        #endregion

        #region Ctors
        public AbstractNonPeriodicalEventsObserver()
        {
            this.Object = new NonPeriodicalEventObject<ObservedValueKey, ObservedValue>();
        }
        #endregion
    }
}
