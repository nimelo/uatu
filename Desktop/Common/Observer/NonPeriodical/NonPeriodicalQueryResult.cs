﻿using Common.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Observer.NonPeriodical
{
    public class NonPeriodicalQueryResult<T> : AbstractQueryResult
    {
        public T Value { get; set; }
        public long Time { get; set; }

        public NonPeriodicalQueryResult(T value, long from, long time) :base(from)
        {
            this.Value = value;
            this.Time = time;
        }
    }
}
