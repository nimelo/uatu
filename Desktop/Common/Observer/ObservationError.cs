﻿using System;

namespace Common.Observer
{
    public class ObservationError : MarshalByRefObject
    {
        public System.Exception Exception { get; protected set; }

        public ObservationError(System.Exception e)
        {
            this.Exception = e;
        }
    }
}
