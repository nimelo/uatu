﻿namespace Common.EventArgs
{
    public class DetectorEventArgs
    {
        public Common.MachineStateEnum State { get; set; }
        public DetectorEventArgs(Common.MachineStateEnum state)
        {
            this.State = state;
        }
    }
}