﻿using System;

namespace Common.EventArgs
{
    public class PluginErrorEventArgs : System.EventArgs
    {
        public string Messasge { get; set; }

        public PluginErrorEventArgs(string msg)
        {
            this.Messasge = msg;
        }
    }
}