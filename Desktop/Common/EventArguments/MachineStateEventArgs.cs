﻿using System;

namespace Common.EventArgs
{
    public class MachineStateEventArgs : System.EventArgs
    {
        public MachineStateEnum State { get; set; }

        public MachineStateEventArgs(MachineStateEnum e)
        {

        }
    }
}