﻿using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EventArgs;
using System.Timers;
using Common.ExtensionMethods;
using System.Reflection;

namespace ChromeBrowserObserver
{
    public class ChromeObserver : AbstractNonPeriodicalEventsObserver<NonPeriodicalQueryResult<string>, string, string>
    {
        public override string Name
        {
            get
            {
                return nameof(ChromeObserver);
            }
        }

        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == Common.MachineStateEnum.IDLE)
            {
                this.Stop();
            }
            else if (e.State == Common.MachineStateEnum.RESUME)
            {
                this.Start();
            }
        }

        protected override string QueryObservedKey(ElapsedEventArgs elapsed)
        {
            var key = ChromeQueryHelper.GetCurrentUrl();
            return key ?? this.Object.Value;
        }

        protected override string QueryObservedValue(string key, ElapsedEventArgs elapsed)
        {
            return key;
        }

        public override void Stop()
        {
            if (this.Object.HasValue)
            {
                var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                this.Object.ClearObject();
                OnQueryOccured(new NonPeriodicalQueryResult<string>(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime));
            }
            base.Stop();
        }

        protected override void Observe(ElapsedEventArgs elapsed)
        {
            if (ChromeQueryHelper.GetExecutionPath().Contains(@"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"))
            {
                base.Observe(elapsed);
            }
            else
            {
                if (this.Object.HasValue)
                {
                    var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                    this.Object.ClearObject();
                    OnQueryOccured(new NonPeriodicalQueryResult<string>(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime - endedEvent.StartTime));
                }
            }
        }
    }
}
