﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace ChromeBrowserObserver
{
    public static class ChromeQueryHelper
    {
        public static string GetCurrentUrl()
        {
            var pId = GetForegroundProcessId();
            Process process = Process.GetProcessById((int)pId);
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            AutomationElement edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
            if (edit == null)
                return null;
            return ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
        }
        
        private static uint GetForegroundProcessId()
        {
            var handle = GetForegroundWindowHandle();
            var pId = GetWindowThreadProcessId(handle);

            return pId;
        }

        #region DLL imports
        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint processId);
        #endregion

        #region Static Methods
        public static IntPtr GetForegroundWindowHandle()
        {
            return GetForegroundWindow();
        }

        public static uint GetWindowThreadProcessId(IntPtr windowHandle)
        {
            uint processId;

            GetWindowThreadProcessId(windowHandle, out processId);

            return processId;
        }

        public static string GetExecutionPath()
        {
            uint pId = GetForegroundProcessId();
            var process = Process.GetProcessById((int)pId);

            return process.Modules[0].FileName;
        }
        #endregion
    }
}
