﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.EventArgs;
using Common.Observer.NonPeriodical;
using Common.ExtensionMethods;
using Common.Observer;
using System.ComponentModel.Composition;
using System.Reflection;
using Common.Observer.ApplicationObserver;
using Common.Objects;

namespace ApplicationObserver
{
    [Export(typeof(AbstractObserver))]
    public class AppObserver : AbstractApplicationObserver
    {
        #region Override methods
        public override string Name
        {
            get
            {
                return nameof(AppObserver);
            }
        }

        protected override ApplicationObserverQueryResult GetQueryResultFromEndedEvent(EndedEventObject<string, string> endedEvent)
        {
            return new ApplicationObserverQueryResult(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime - endedEvent.StartTime);
        }

        protected override string QueryObservedKey(ElapsedEventArgs elapsed)
        {
            var handle = ApplicationQueryHelper.GetForegroundWindowHandle();
            var pId = ApplicationQueryHelper.GetWindowThreadProcessId(handle);
            var path = ApplicationQueryHelper.GetExecutionPath(pId);

            return path;
        }

        protected override string QueryObservedValue(string key, ElapsedEventArgs elapsed)
        {
            return key;
        }       
        #endregion
    }
}
