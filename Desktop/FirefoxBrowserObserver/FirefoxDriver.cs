﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FirefoxBrowserObserver
{
    public static class FirefoxDriver
    {
        #region Public methods
        public static string GetExecutionPath()
        {
            uint pId = GetForegroundProcessId();
            var process = Process.GetProcessById((int)pId);

            return process.Modules[0].FileName;
        }
        #endregion

        #region DLL imports
        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint processId);
        #endregion

        #region Private methods
        private static uint GetForegroundProcessId()
        {
            var handle = GetForegroundWindowHandle();
            var pId = GetWindowThreadProcessId(handle);

            return pId;
        }

        private static IntPtr GetForegroundWindowHandle()
        {
            return GetForegroundWindow();
        }

        private static uint GetWindowThreadProcessId(IntPtr windowHandle)
        {
            uint processId;

            GetWindowThreadProcessId(windowHandle, out processId);

            return processId;
        }
        #endregion
    }
}
