﻿using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EventArgs;
using System.Timers;
using Common.ExtensionMethods;
using Common.Observer;
using System.ComponentModel.Composition;
using System.Reflection;
using Common.Observer.BrowserObserver;
using Common.Objects;

namespace FirefoxBrowserObserver
{
    [Export(typeof(AbstractObserver))]
    public class FirefoxObserver : AbstractBrowserObserver
    {
        #region Overrided methods        
        public override string Name
        {
            get
            {
                return nameof(FirefoxObserver);
            }
        }

        public override string BrowserName
        {
            get
            {
                return @"firefox";
            }
        }

        protected override string QueryObservedKey(ElapsedEventArgs elapsed)
        {
            var key = this.GetCurrentUrl();
            return key ?? this.Object.Value;
        }

        protected override void Observe(ElapsedEventArgs elapsed)
        {
            if (this.ExecutablePaths.Contains(FirefoxDriver.GetExecutionPath()))
            {
                if (!this.DDE.IsConnected)
                {
                    this.DDE.Connect();
                }

                base.Observe(elapsed);
            }
            else
            {
                if (this.Object.HasValue)
                {
                    var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                    this.Object.ClearObject();
                    var queryResult = this.GetQueryResultFromEndedEvent(endedEvent);
                    OnQueryOccured(queryResult);
                }
            }
        }

        protected override BrowserQueryResult GetQueryResultFromEndedEvent(EndedEventObject<string, string> endedEvent)
        {
            return new BrowserQueryResult(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime - endedEvent.StartTime);
        }
        #endregion

        #region Private methods
        private string GetCurrentUrl()
        {
            try
            {
                String url = this.DDE.Request("URL", Int32.MaxValue);
                Int32 stop = url.IndexOf('"', 1);

                return url.Substring(1, stop - 1);
            }
            catch (Exception e)
            {
                OnErrorOccured(new Common.Observer.ObservationError(e));
                return null;
            }
        }
        #endregion

        #region Properties
        protected NDde.Client.DdeClient DDE { get; set; }        
        #endregion

        #region Ctors
        public FirefoxObserver() : base()
        {
            this.DDE = new NDde.Client.DdeClient("Firefox", "WWW_GetWindowInfo");
            this.DDE.Disconnected += DDE_Disconnected;
            this.ExecutablePaths?.Add((@"C:\Program Files (x86)\Mozilla Firefox\firefox.exe"));
        }

        private void DDE_Disconnected(object sender, NDde.Client.DdeDisconnectedEventArgs e)
        {
            if (this.Object.HasValue)
            {
                var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                this.Object.ClearObject();
                var queryResult = this.GetQueryResultFromEndedEvent(endedEvent);
                OnQueryOccured(queryResult);
            }
        }
        #endregion
    }
}
