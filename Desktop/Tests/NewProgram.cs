﻿using Common.Observer;
using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Observer.Periodical;
using CPUTemperatureObserver;
using System.Web.Script.Serialization;
using System.Collections.Concurrent;

namespace Tests
{
    public class NewProgram
    {
        public static ConcurrentBag<AbstractQueryResult> results = new ConcurrentBag<AbstractQueryResult>();
        public static ConcurrentDictionary<AbstractObserver, ConcurrentBag<AbstractQueryResult>> resultPerObserver = new ConcurrentDictionary<AbstractObserver, ConcurrentBag<AbstractQueryResult>>();

        static void Main(string[] args)
        {
            AbstractObserver plugin = new ApplicationObserver.AppObserver();
            plugin.ErrorOccured += Plugin_ErrorOccured;
            plugin.QueryOccured += Plugin_QueryOccured;

            AbstractObserver plugin2 = new ChromeBrowserObserver.ChromeObserver();
            plugin2.ErrorOccured += Plugin_ErrorOccured;
            plugin2.QueryOccured += Plugin_QueryOccured;

            AbstractObserver plugin3 = new CPUTemperatureObserver.CPUTempObserver();
            plugin3.ErrorOccured += Plugin_ErrorOccured;
            plugin3.QueryOccured += Plugin_QueryOccured2;

            AbstractObserver plugin4 = new FirefoxBrowserObserver.FirefoxObserver();
            plugin4.ErrorOccured += Plugin_ErrorOccured;
            plugin4.QueryOccured += Plugin_QueryOccured;

            AbstractObserver plugin5 = new InternetExplorerBrowserObserver.InternetExplorerObserver();
            plugin5.ErrorOccured += Plugin_ErrorOccured;
            plugin5.QueryOccured += Plugin_QueryOccured;

            List<AbstractObserver> lists = new List<AbstractObserver>()
            {
                plugin/*, plugin2*/, plugin3, plugin4, plugin5
            };

            lists.ForEach(x => x.Start());
            var obj = new object();
            while (true)
            {
                System.Threading.Thread.Sleep(3000);
                foreach (var item in results)
                {
                   // Console.WriteLine(new JavaScriptSerializer().Serialize(item));
                }
            }

        }

        private static void Plugin_QueryOccured2(object sender, Common.Observer.AbstractQueryResult e)
        {
            var result = e as PeriodicalQueryResult<CPUTemp>;
            foreach (var item in result.Value.Temperatures)
            {
                Console.WriteLine($"{DateTimeOffset.FromUnixTimeMilliseconds(result.Time)} {item.Key} {item.Value}");
            }
            resultPerObserver.GetOrAdd(sender as AbstractObserver, new ConcurrentBag<AbstractQueryResult>()).Add(e);
        }

        private static void Plugin_QueryOccured(object sender, Common.Observer.AbstractQueryResult e)
        {
            var result = e as NonPeriodicalQueryResult<string>;
            Console.WriteLine($"{(sender as AbstractObserver).Name}: {result.Value} {result.From} {result.Time}");
            resultPerObserver.GetOrAdd(sender as AbstractObserver, new ConcurrentBag<AbstractQueryResult>()).Add(e);
        }

        private static void Plugin_ErrorOccured(object sender, Common.Observer.ObservationError e)
        {
            Console.WriteLine($"{(sender as AbstractObserver).Name + "\n"}{e.Exception} ->{e.Exception.Message}");
        }
    }
}
