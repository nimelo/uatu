﻿using Common.Observer;
using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    class Program3
    {
        static void Main(string[] args)
        {
            var observersPath = Directory.GetDirectories(System.Environment.CurrentDirectory);
            var shadowPath = Path.Combine(System.Environment.CurrentDirectory, "Shadow");

            Directory.CreateDirectory(shadowPath);

            var setup = new AppDomainSetup()
            {
                CachePath = shadowPath,
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = observersPath.First()
            };

            AppDomain domain = AppDomain.CreateDomain("Host_AppDomain", AppDomain.CurrentDomain.Evidence, setup);



            /*Core.ObserverImporter lib = new Core.ObserverImporter();
            lib.DoImport(System.Environment.CurrentDirectory);

            foreach (var item in lib.Observers)
            {
                item.ErrorOccured += Plugin_ErrorOccured;
                item.QueryOccured += Plugin_QueryOccured;
                item.Start();
            }
     
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                lib.RemoveFromCatalog(Path.Combine(System.Environment.CurrentDirectory, "App"));
            }
            */
        }

        private static void Plugin_QueryOccured(object sender, Common.Observer.AbstractQueryResult e)
        {
            var result = e as NonPeriodicalQueryResult<string>;
            Console.WriteLine($"{(sender as AbstractObserver).Name}: {result.Value} {result.From} {result.Time}");           
        }

        private static void Plugin_ErrorOccured(object sender, Common.Observer.ObservationError e)
        {
            Console.WriteLine($"{(sender as AbstractObserver).Name + "\n"}{e.Exception} ->{e.Exception.Message}");
        }
    }
}
