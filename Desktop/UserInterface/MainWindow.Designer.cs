﻿namespace UserInterface
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.mainWindowPanel = new System.Windows.Forms.Panel();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.loginPage = new System.Windows.Forms.TabPage();
            this.loginTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.loggedInAsControl = new UserInterface.LoggedInAsPanel.LoggedInAsControl();
            this.pluginsPage = new System.Windows.Forms.TabPage();
            this.pluginControl = new UserInterface.PluginPanel.PluginControl();
            this.inforamtionPage = new System.Windows.Forms.TabPage();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.mainWindowPanel.SuspendLayout();
            this.mainTabControl.SuspendLayout();
            this.loginPage.SuspendLayout();
            this.loginTableLayoutPanel.SuspendLayout();
            this.pluginsPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainWindowPanel
            // 
            this.mainWindowPanel.Controls.Add(this.mainTabControl);
            this.mainWindowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainWindowPanel.Location = new System.Drawing.Point(0, 0);
            this.mainWindowPanel.Name = "mainWindowPanel";
            this.mainWindowPanel.Size = new System.Drawing.Size(364, 411);
            this.mainWindowPanel.TabIndex = 0;
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.loginPage);
            this.mainTabControl.Controls.Add(this.pluginsPage);
            this.mainTabControl.Controls.Add(this.inforamtionPage);
            this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabControl.Location = new System.Drawing.Point(0, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(364, 411);
            this.mainTabControl.TabIndex = 0;
            // 
            // loginPage
            // 
            this.loginPage.Controls.Add(this.loginTableLayoutPanel);
            this.loginPage.Location = new System.Drawing.Point(4, 22);
            this.loginPage.Name = "loginPage";
            this.loginPage.Padding = new System.Windows.Forms.Padding(3);
            this.loginPage.Size = new System.Drawing.Size(356, 385);
            this.loginPage.TabIndex = 0;
            this.loginPage.Text = "Login";
            this.loginPage.UseVisualStyleBackColor = true;
            // 
            // loginTableLayoutPanel
            // 
            this.loginTableLayoutPanel.ColumnCount = 1;
            this.loginTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.loginTableLayoutPanel.Controls.Add(this.loggedInAsControl, 0, 0);
            this.loginTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.loginTableLayoutPanel.Name = "loginTableLayoutPanel";
            this.loginTableLayoutPanel.RowCount = 2;
            this.loginTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.loginTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.loginTableLayoutPanel.Size = new System.Drawing.Size(350, 379);
            this.loginTableLayoutPanel.TabIndex = 0;
            // 
            // loggedInAsControl
            // 
            this.loggedInAsControl.Location = new System.Drawing.Point(3, 3);
            this.loggedInAsControl.Name = "loggedInAsControl";
            this.loggedInAsControl.Size = new System.Drawing.Size(344, 120);
            this.loggedInAsControl.TabIndex = 0;
            // 
            // pluginsPage
            // 
            this.pluginsPage.Controls.Add(this.pluginControl);
            this.pluginsPage.Location = new System.Drawing.Point(4, 22);
            this.pluginsPage.Name = "pluginsPage";
            this.pluginsPage.Padding = new System.Windows.Forms.Padding(3);
            this.pluginsPage.Size = new System.Drawing.Size(356, 385);
            this.pluginsPage.TabIndex = 1;
            this.pluginsPage.Text = "Plugins";
            this.pluginsPage.UseVisualStyleBackColor = true;
            // 
            // pluginControl
            // 
            this.pluginControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pluginControl.Location = new System.Drawing.Point(3, 3);
            this.pluginControl.Name = "pluginControl";
            this.pluginControl.Size = new System.Drawing.Size(350, 379);
            this.pluginControl.TabIndex = 0;
            // 
            // inforamtionPage
            // 
            this.inforamtionPage.Location = new System.Drawing.Point(4, 22);
            this.inforamtionPage.Name = "inforamtionPage";
            this.inforamtionPage.Padding = new System.Windows.Forms.Padding(3);
            this.inforamtionPage.Size = new System.Drawing.Size(356, 385);
            this.inforamtionPage.TabIndex = 2;
            this.inforamtionPage.Text = "Informations";
            this.inforamtionPage.UseVisualStyleBackColor = true;
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "UATU - The Watcher";
            this.notifyIcon.Visible = true;
            this.notifyIcon.Click += new System.EventHandler(this.OnNotifyIconCLick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 411);
            this.Controls.Add(this.mainWindowPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "UATU - The Watcher";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Resize += new System.EventHandler(this.OnWindowResize);
            this.mainWindowPanel.ResumeLayout(false);
            this.mainTabControl.ResumeLayout(false);
            this.loginPage.ResumeLayout(false);
            this.loginTableLayoutPanel.ResumeLayout(false);
            this.pluginsPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mainWindowPanel;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage loginPage;
        private System.Windows.Forms.TabPage pluginsPage;
        private System.Windows.Forms.TabPage inforamtionPage;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.TableLayoutPanel loginTableLayoutPanel;
        public PluginPanel.PluginControl pluginControl;
        private LoggedInAsPanel.LoggedInAsControl loggedInAsControl;
    }
}

