﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.Strategies;
using UserInterface.Configurations;
using System.IO;

namespace UserInterface.PluginPanel
{
    public class PluginControlPresenter
    {
        private List<PluginsGridViewRecord> _model;
        private IObserversManager _observersManager;
        private IPluginControl _view;

        public PluginControlPresenter(IObserversManager pluginsManager, IPluginControl view)
        {
            this._model = GetModelFrom(pluginsManager);
            this._observersManager = pluginsManager;
            this._view = view;
            this.WireUpEvents();
        }

        private List<PluginsGridViewRecord> GetModelFrom(IObserversManager pluginsManager)
        {
            var pModels = new List<PluginsGridViewRecord>();
            foreach (var plugin in pluginsManager.GetObservers())
            {
                pModels.Add(new PluginsGridViewRecord()
                {
                    Name = plugin.Value.Name,
                    Version = plugin.Value.Version,
                    IsActive = plugin.Value.IsRunning,
                    Path = plugin.Key
                });
            }

            return pModels;
        }

        private void WireUpEvents()
        {
            this._view.WireUpDataSource(this._model);
            this._view.AddPlugin += OnAddPlugin;
            this._view.RemovePlugin += OnRemovePlugin;
            this._view.StartPlugin += OnStartPlugin;
            this._view.StopPlugin += OnStopPlugin;
        }

        private void OnStopPlugin(object sender, PluginsGridViewRecord e)
        {
            ShowMessageBoxOnException.Invoke(() =>
            {
                if (this._observersManager.Stop(e.Path))
                {
                    this._model.Where(x => x.Version == e.Version && x.Name == e.Name).First().IsActive = false;
                    this._view.NotifyDataChanged();
                }
            });
        }

        private void OnStartPlugin(object sender, PluginsGridViewRecord e)
        {
            ShowMessageBoxOnException.Invoke(() =>
            {
                if (this._observersManager.Start(e.Path))
                {
                    this._model.Where(x => x.Version == e.Version && x.Name == e.Name).First().IsActive = true;
                    this._view.NotifyDataChanged();
                }
            });
        }

        private void OnRemovePlugin(object sender, PluginsGridViewRecord e)
        {
            ShowMessageBoxOnException.Invoke(() =>
            {
                this._observersManager.RemoveObserver(e.Path);
                var toRemove = this._model.Where(x => x.Name == e.Name && x.Version == e.Version).First();
                this._model.Remove(toRemove);               
                this.RemoveDirectory(e.Path);
                this._view.NotifyDataChanged();
            });
        }

        private void OnAddPlugin(object sender, string directory)
        {
            ShowMessageBoxOnException.Invoke(() =>
            {
                var newPath = Path.Combine(System.Environment.CurrentDirectory, ApplicationSettingsManager.ApplicationConfiguration.ObserversPath, directory.Split(Path.DirectorySeparatorChar).Last());
                this.CopyDirectoryR(directory, newPath);
               
                var plugin = this._observersManager.AddObserver(newPath);

                this._model.Add(new PluginsGridViewRecord()
                {
                    Name = plugin.Name,
                    Version = plugin.Version,
                    Path = newPath
                });
                
                this._view.NotifyDataChanged();               
            });
        }

        private void CopyDirectoryR(string directory, string newPath)
        {
            if (Directory.Exists(newPath))
            {
                throw new ArgumentException($"Directory already exits: {newPath}");
            }
            var di = Directory.CreateDirectory(newPath);
           
            foreach (var file in Directory.GetFiles(directory))
            {
                File.Copy(file, Path.Combine(newPath, Path.GetFileName(file)));
            }
        }

        private void RemoveDirectory(string name)
        {
            Directory.Delete(name, true);
        }

        private void CopyFile(string from, string to)
        {
            System.IO.File.Copy(from, to);
        }
    }
}
