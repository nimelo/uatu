﻿namespace UserInterface.PluginPanel
{
    public class PluginsGridViewRecord
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Version { get; set; }
        public string Path { get; set; }
    }
}