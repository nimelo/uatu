﻿using System;
using System.Collections.Generic;

namespace UserInterface.PluginPanel
{
    public interface IPluginControl
    {
        event EventHandler<string> AddPlugin;
        event EventHandler<PluginsGridViewRecord> RemovePlugin;
        event EventHandler<PluginsGridViewRecord> StartPlugin;
        event EventHandler<PluginsGridViewRecord> StopPlugin;

        void OnAddPlugin(string path);
        void OnRemovePlugin(PluginsGridViewRecord pluginName);
        void OnStopPlugin(PluginsGridViewRecord pluginName);
        void OnStartPlugin(PluginsGridViewRecord pluginName);

        void WireUpDataSource(List<PluginsGridViewRecord> records);
        void NotifyDataChanged();
    }
}