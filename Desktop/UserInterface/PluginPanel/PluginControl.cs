﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface.PluginPanel
{
    public partial class PluginControl : UserControl, IPluginControl
    {
        public PluginControl()
        {
            InitializeComponent();
        }

        public event EventHandler<string> AddPlugin;
        public event EventHandler<PluginsGridViewRecord> RemovePlugin;
        public event EventHandler<PluginsGridViewRecord> StartPlugin;
        public event EventHandler<PluginsGridViewRecord> StopPlugin;

        public void OnAddPlugin(string path)
        {
            this.AddPlugin?.Invoke(this, path);
        }

        public void OnRemovePlugin(PluginsGridViewRecord pluginName)
        {
            this.RemovePlugin?.Invoke(this, pluginName);
        }

        public void OnStartPlugin(PluginsGridViewRecord pluginName)
        {
            this.StartPlugin?.Invoke(this, pluginName);
        }

        public void OnStopPlugin(PluginsGridViewRecord pluginName)
        {
            this.StopPlugin?.Invoke(this, pluginName);
        }

        private void addPluginButton_Click(object sender, EventArgs e)
        {
            var result = this.folderBrowserDialog.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                var path = this.folderBrowserDialog.SelectedPath;
                OnAddPlugin(path);
            }              
        }

        private PluginsGridViewRecord GetSelectedRecord()
        {
            return new PluginsGridViewRecord()
            {
                Name = gridView[0, gridView.CurrentRow.Index].Value.ToString(),
                Version = gridView[2, gridView.CurrentRow.Index].Value.ToString(),
                Path = gridView[3, gridView.CurrentRow.Index].Value.ToString()
            };
        }

        private void ContextDeleteClicked(object sender, EventArgs e)
        {
            this.OnRemovePlugin(GetSelectedRecord());
        }

        private void ContextStartClicked(object sender, EventArgs e)
        {
            this.OnStartPlugin(GetSelectedRecord());
        }

        private void ContextStopClicked(object sender, EventArgs e)
        {
            this.OnStopPlugin(GetSelectedRecord());
        }

        public void WireUpDataSource(List<PluginsGridViewRecord> records)
        {
            this.pluginsGridViewModelBindingSource.DataSource = records;
        }

        public void NotifyDataChanged()
        {
            var records = this.pluginsGridViewModelBindingSource.DataSource;
            this.pluginsGridViewModelBindingSource.DataSource = null;
            this.pluginsGridViewModelBindingSource.DataSource = records;
        }


    }
}
