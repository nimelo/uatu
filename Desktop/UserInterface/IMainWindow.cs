﻿using System.Windows.Forms;

namespace UserInterface
{
    public interface IMainWindow
    {
        void EnableLoginTab(bool enable);
        void EnableInformationTab(bool enable);
        void EnablePluginTab(bool enable);
                
        PluginPanel.PluginControl GetPluginControl();
        LoggedInAsPanel.LoggedInAsControl GetLoggedInAsControl();

        void NotifyOnTray(string title, string msg, ToolTipIcon icon, int timeout = 5000);
    }
}