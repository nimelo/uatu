﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Strategies
{
    public static class ShowMessageBoxOnException
    {
        public static void Invoke(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }
    }
}
