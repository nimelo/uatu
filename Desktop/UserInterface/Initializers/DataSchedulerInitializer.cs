﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.Scheduler;

namespace UserInterface.Initializers
{
    public static class DataSchedulerInitializer
    {
        public static DataScheduler InitializeAndGet(ObserversDataStorage.ObserversDataStorage storage, long period)
        {
            return new DataScheduler(storage, period);
        }
    }
}
