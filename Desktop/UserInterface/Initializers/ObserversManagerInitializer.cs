﻿using Common.Observer.ApplicationObserver;
using Common.Observer.BrowserObserver;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.Configurations;

namespace UserInterface.Initializers
{
    public static class ObserversManagerInitializer
    {
        public static IObserversManager InitializeAndGet(AppDomain domain, string observersPath)
        {
            IObserversManager manager = Create(domain);
            LoadObservers(manager, observersPath);
            Configure(manager);
            return manager;
        }

        private static void LoadObservers(IObserversManager manager, string observersPath)
        {
            foreach (var item in System.IO.Directory.GetDirectories(observersPath))
            {
                try
                {
                    manager.AddObserver(item);
                }
                catch (Exception e)
                {

                }

            }
        }

        private static void Configure(IObserversManager manager)
        {
            ConfiguraBrowserObservers(manager);
            ConfigureApplicationObservers(manager);
        }

        private static void ConfigureApplicationObservers(IObserversManager manager)
        {
            var config = ApplicationSettingsManager.ProgramsConfiguration;

            foreach (AbstractApplicationObserver observer in manager.GetObservers().Values.Where(x => x is AbstractApplicationObserver))
            {
                observer.IdleExceptions.AddRange(config.IdleExceptionPaths);
            }
        }

        private static void ConfiguraBrowserObservers(IObserversManager manager)
        {
            var config = ApplicationSettingsManager.BrowsersConfiguration;

            foreach (AbstractBrowserObserver observer in manager.GetObservers().Values.Where(x => x is AbstractBrowserObserver))
            {
                var browserName = observer.BrowserName;
                if (config.BrowsersExecutablePaths.Keys.Contains(browserName))
                {
                    observer.ExecutablePaths.Add(config.BrowsersExecutablePaths[browserName]);
                }

                observer.IdleExeceptionUrls.AddRange(config.IdleExceptionPaths);
            }
        }

        private static IObserversManager Create(AppDomain domain)
        {
            var ob = domain.CreateInstanceAndUnwrap(typeof(ObserverImporter).Assembly.FullName, typeof(ObserverImporter).FullName) as ObserverImporter;
            ob.Detector = new MachineEventsDetector(new DetectorsConfiguration()
            {
                IdleDetectorTimerPeriod = 1000,
                IdleDetectorIdlePeriod = ApplicationSettingsManager.ObserversCommonConfiguration.IdlePeriod
            });
            return ob;
        }
    }
}
