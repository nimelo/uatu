﻿using Common;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserInterface.Configurations;
using UserInterface.MainWindowClasses;
using Common.Observer;
using Common.Observer.BrowserObserver;
using Common.Observer.ApplicationObserver;
using UserInterface.Initializers;

namespace UserInterface
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ApplicationSettingsManager.Initialize();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);          
             
            var view = new MainWindow();
            var mwp = new MainWindowClasses.MainWindowPresenter(view);
            Application.Run(view);
        }
    }
}
