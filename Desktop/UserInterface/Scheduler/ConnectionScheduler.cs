﻿using Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using UserInterface.StaticContainer;

namespace UserInterface.Scheduler
{
    public class ConnectionScheduler
    {
        public ConnectionScheduler(ServiceConfiguration conf, long period)
        {
            this.ResolveConfiguration = conf;
            this.ConnectionCheckPeriod = period;
            this.Timer = new Timer(period);
            this.Timer.Elapsed += OnTimerElapsed;
            this.IsRunning = false;            
        }

        #region Private methods
        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.TryConnect();
        }

        private void TryConnect()
        {
            try
            {
                if(ApplicationContainer.Get.Communicator == null)
                {
                    var communicator = new Communicator(this.ResolveConfiguration);
                    ApplicationContainer.Get.Communicator = communicator;
                    OnConnectionSuccessOccured?.Invoke(this, null);
                }               
            }
            catch (Exception e)
            {
                OnConnectionErrorOccured?.Invoke(this, e);
            }
            
        }
        #endregion

        #region Properties
        public long ConnectionCheckPeriod { get; private set; }
        public ServiceConfiguration ResolveConfiguration { get; private set; }
        public Timer Timer { get; private set; }
        public bool IsRunning { get; set; }
        #endregion

        #region Events
        public event EventHandler<Exception> OnConnectionErrorOccured;
        public event EventHandler OnConnectionSuccessOccured;
        #endregion

        #region Public methods
        public void Start()
        {
            if (!this.IsRunning)
            {
                this.Timer.Start();
                this.IsRunning = true;
                OnTimerElapsed(null, null);
            }
        }

        public void Stop()
        {
            if (this.IsRunning)
            {
                this.Timer.Stop();
                this.IsRunning = false;
            }
        }
        #endregion
    }
}
