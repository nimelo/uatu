﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UserInterface.ObserversDataStorage;

namespace UserInterface.Scheduler
{
    public static class DailyDataStorageUtils
    {
        public static string ToJson(object value)
        {
            var obj = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(value);
            return obj;
        }

        public static List<History> ToHistories(string json)
        {
            var obj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<History>>(json);
            return obj;
        }

        public static List<Statistics> ToStatistics(string json)
        {
            var obj = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<Statistics>>(json);
            return obj;
        }
    }
}
