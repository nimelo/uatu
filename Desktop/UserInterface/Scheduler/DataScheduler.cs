﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using UserInterface.Configurations;
using UserInterface.ObserversDataStorage;
using Common.ExtensionMethods;
using Communication.Raporting;
using Communication;
using UserInterface.StaticContainer;

namespace UserInterface.Scheduler
{
    public class DataScheduler
    {
        #region Properties
        public bool IsRunning { get; set; }
        private ObserversDataStorage.ObserversDataStorage Storage { get; set; }
        public long Period { get; protected set; }
        private Timer Timer { get; set; }
        #endregion

        #region Ctors
        public DataScheduler(ObserversDataStorage.ObserversDataStorage storage, long period)
        {
            this.Storage = storage;
            this.Period = period;
            this.Timer = new Timer(period);
            this.Timer.Elapsed += OnTimerElapsed;
            this.IsRunning = false;
        }
        #endregion

        #region Public methods
        public void Start()
        {
            if (!this.IsRunning)
            {
                this.IsRunning = true;
                this.Timer.Start();
            }
        }

        public void Stop()
        {
            if (this.IsRunning)
            {
                this.Stop();
                this.IsRunning = false;
            }
        }

        public void ForceSendCurrentRaport()
        {
            this.SendCurrentRaport(DateTime.Now);
        }

        public void ForceSendOldRaports()
        {
            this.SendOldRaports(DateTime.Now);
        }
        #endregion

        #region Private methods
        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.SendOldRaports(e.SignalTime);
            this.SendCurrentRaport(e.SignalTime);
        }

        private void SendCurrentRaport(DateTime e)
        {
            var raport = this.Storage.ChangeStorage().ToList();
            this.SendCurrentStatistics(raport, e);
            this.SendCurrentHistory(raport, e);
        }

        private void SendCurrentStatistics(List<DailyObserverStorage> raport, DateTime e)
        {
            var statistics = raport.Select(x => x.Statistics);
            if(!this.SendStatistics(statistics, e))
            {
                this.SaveStatistics(statistics, e);
            }
        }

        private void SaveStatistics(IEnumerable<Statistics> statistics, DateTime e)
        {
            var json = DailyDataStorageUtils.ToJson(statistics);
            var filePath = Path.Combine(ApplicationSettingsManager.ApplicationConfiguration.RaportPath, $"{e.ToUnixUtc()}.statistic");
            File.WriteAllText(filePath, json);
        }

        private bool SendStatistics(IEnumerable<Statistics> statistics, DateTime e)
        {
            var config = ApplicationSettingsManager.LoginConfiguration;
            try
            {
                var result = ApplicationContainer.Get.Communicator.Raport.RaportStatistics(config.Login, config.AppKey, statistics);
                return result.IsSuccess;
            }
            catch (Exception)
            {
                return false;
            }           
        }

        private void SendCurrentHistory(List<DailyObserverStorage> raport, DateTime e)
        {
            var histories = raport.Select(x => x.History);
            if(!this.SendHistories(histories, e))
            {
                this.SaveHistories(histories, e);
            }
        }

        private bool SendHistories(IEnumerable<History> histories, DateTime e)
        {
            var config = ApplicationSettingsManager.LoginConfiguration;
            try
            {
                var result = ApplicationContainer.Get.Communicator.Raport.RaportHistory(config.Login, config.AppKey, histories);
                return result.IsSuccess;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void SaveHistories(IEnumerable<History> histories, DateTime e)
        {
            var json = DailyDataStorageUtils.ToJson(histories);
            var filePath = Path.Combine(ApplicationSettingsManager.ApplicationConfiguration.RaportPath, $"{e.ToUnixUtc()}.history");
            File.WriteAllText(filePath, json);
        }
       
        private void SendOldRaports(DateTime e)
        {
            this.SendOldStatistics(e);
            this.SendOldHistory(e);           
        }

        private void SendOldHistory(DateTime e)
        {
            foreach (var file in Directory.GetFiles(ApplicationSettingsManager.ApplicationConfiguration.RaportPath, "*.history"))
            {
                try
                {
                    var history = DailyDataStorageUtils.ToHistories(File.ReadAllText(file));
                    if(this.SendHistories(history, e))
                    {
                        File.Delete(file);
                    }                  
                }
                catch (Exception ex)
                {
                   
                }                            
            }
        }

        private void SendOldStatistics(DateTime e)
        {
            foreach (var file in Directory.GetFiles(ApplicationSettingsManager.ApplicationConfiguration.RaportPath, "*.statistic"))
            {
                try
                {
                    var statistic = DailyDataStorageUtils.ToStatistics(File.ReadAllText(file));
                    if(this.SendStatistics(statistic, e))
                    {
                        File.Delete(file);
                    }                 
                }
                catch (Exception ex)
                {

                }
            }
        }
        #endregion

    }
}
