﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserInterface.LoggedInAsPanel;
using UserInterface.LoginPanel;
using UserInterface.PluginPanel;

namespace UserInterface
{
    public partial class MainWindow : Form, IMainWindow
    {
        public MainWindow()
        {
            InitializeComponent();        
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.Hide();
        }

        private void OnWindowResize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }
        }

        private void OnNotifyIconCLick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        #region
        public void EnableLoginTab(bool enable)
        {
            this.loginPage.Enabled = enable;
        }

        public void EnableInformationTab(bool enable)
        {
            this.inforamtionPage.Enabled = enable;
        }

        public void EnablePluginTab(bool enable)
        {
            this.pluginsPage.Enabled = enable;
        }     

        public PluginControl GetPluginControl()
        {
            return this.pluginControl;
        }

        public LoggedInAsControl GetLoggedInAsControl()
        {
            return this.loggedInAsControl;
        }

        public void NotifyOnTray(string title, string msg, ToolTipIcon icon, int timeout = 5000)
        {
            this.notifyIcon.ShowBalloonTip(timeout, title, msg, icon);
        }
        #endregion
    }
}
