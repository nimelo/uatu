﻿namespace UserInterface.LoggedInAsPanel
{
    partial class LoggedInAsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.typeLabel = new System.Windows.Forms.Label();
            this.loggedAsLabel = new System.Windows.Forms.Label();
            this.currentUserLabel = new System.Windows.Forms.Label();
            this.logoutButton = new System.Windows.Forms.Button();
            this.appkeyLabel = new System.Windows.Forms.Label();
            this.currentAppKeyLabel = new System.Windows.Forms.Label();
            this.currentTypeLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.currentTypeLabel, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.currentAppKeyLabel, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.typeLabel, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.loggedAsLabel, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.currentUserLabel, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.logoutButton, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.appkeyLabel, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(424, 211);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.typeLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeLabel.Location = new System.Drawing.Point(51, 104);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(42, 52);
            this.typeLabel.TabIndex = 6;
            this.typeLabel.Text = "Type:";
            this.typeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // loggedAsLabel
            // 
            this.loggedAsLabel.AutoSize = true;
            this.loggedAsLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.loggedAsLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggedAsLabel.Location = new System.Drawing.Point(3, 0);
            this.loggedAsLabel.Name = "loggedAsLabel";
            this.loggedAsLabel.Size = new System.Drawing.Size(90, 52);
            this.loggedAsLabel.TabIndex = 0;
            this.loggedAsLabel.Text = "Current user:";
            this.loggedAsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // currentUserLabel
            // 
            this.currentUserLabel.AutoSize = true;
            this.currentUserLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentUserLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentUserLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.currentUserLabel.Location = new System.Drawing.Point(99, 0);
            this.currentUserLabel.Name = "currentUserLabel";
            this.currentUserLabel.Size = new System.Drawing.Size(322, 52);
            this.currentUserLabel.TabIndex = 3;
            this.currentUserLabel.Text = "[Anonymous]";
            this.currentUserLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // logoutButton
            // 
            this.logoutButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoutButton.Location = new System.Drawing.Point(99, 159);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(322, 49);
            this.logoutButton.TabIndex = 4;
            this.logoutButton.Text = "Relog";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // appkeyLabel
            // 
            this.appkeyLabel.AutoSize = true;
            this.appkeyLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.appkeyLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appkeyLabel.Location = new System.Drawing.Point(30, 52);
            this.appkeyLabel.Name = "appkeyLabel";
            this.appkeyLabel.Size = new System.Drawing.Size(63, 52);
            this.appkeyLabel.TabIndex = 5;
            this.appkeyLabel.Text = "App key:";
            this.appkeyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // currentAppKeyLabel
            // 
            this.currentAppKeyLabel.AutoSize = true;
            this.currentAppKeyLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentAppKeyLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentAppKeyLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.currentAppKeyLabel.Location = new System.Drawing.Point(99, 52);
            this.currentAppKeyLabel.Name = "currentAppKeyLabel";
            this.currentAppKeyLabel.Size = new System.Drawing.Size(322, 52);
            this.currentAppKeyLabel.TabIndex = 7;
            this.currentAppKeyLabel.Text = "[ACTH@#$5]";
            this.currentAppKeyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // currentTypeLabel
            // 
            this.currentTypeLabel.AutoSize = true;
            this.currentTypeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentTypeLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentTypeLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.currentTypeLabel.Location = new System.Drawing.Point(99, 104);
            this.currentTypeLabel.Name = "currentTypeLabel";
            this.currentTypeLabel.Size = new System.Drawing.Size(322, 52);
            this.currentTypeLabel.TabIndex = 8;
            this.currentTypeLabel.Text = "[Desktop]";
            this.currentTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoggedInAsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "LoggedInAsControl";
            this.Size = new System.Drawing.Size(424, 211);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label loggedAsLabel;
        private System.Windows.Forms.Label currentUserLabel;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Label appkeyLabel;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.Label currentAppKeyLabel;
        private System.Windows.Forms.Label currentTypeLabel;
    }
}
