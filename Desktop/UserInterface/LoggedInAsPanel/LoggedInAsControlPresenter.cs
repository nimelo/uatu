﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.Configurations;

namespace UserInterface.LoggedInAsPanel
{
    public class LoggedInAsControlPresenter
    {
        private LoggedInAsModel model;
        private ILoggedInAsView view;

        public LoggedInAsControlPresenter(LoggedInAsModel model, ILoggedInAsView view)
        {
            this.model = model;
            this.view = view;
            this.view.UpdateView();
        }
    }
}
