﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserInterface.Configurations;

namespace UserInterface.LoggedInAsPanel
{
    public partial class LoggedInAsControl : UserControl, ILoggedInAsView
    {
        public LoggedInAsControl()
        {
            InitializeComponent();
        }

        public event EventHandler RelogClicked;

        public void OnRelogClicked()
        {
            this.RelogClicked?.Invoke(this, EventArgs.Empty);
        }

        public void UpdateView()
        {
            var config = ApplicationSettingsManager.LoginConfiguration;
            this.currentUserLabel.Text = config.Login;
            this.currentAppKeyLabel.Text = config.AppKey;

        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            OnRelogClicked();
        }
    }
}
