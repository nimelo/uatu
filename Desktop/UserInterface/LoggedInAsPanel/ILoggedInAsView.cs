﻿using System;

namespace UserInterface.LoggedInAsPanel
{
    public interface ILoggedInAsView
    {
        void UpdateView();

        event EventHandler RelogClicked;

        void OnRelogClicked();
    }
}