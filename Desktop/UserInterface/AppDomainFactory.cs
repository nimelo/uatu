﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface
{
    public static class AppDomainFactory
    {
        public static AppDomain Create(string friendlyName, string shadowPath, string copyPath, Evidence evidence)
        {
            var setup = new AppDomainSetup()
            {
                CachePath = shadowPath,
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = copyPath
            };

            var domain = AppDomain.CreateDomain(friendlyName, evidence, setup);

            return domain;
        }
    }
}
