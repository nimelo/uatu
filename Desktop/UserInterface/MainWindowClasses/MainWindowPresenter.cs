﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Core;
using UserInterface.Configurations;
using UserInterface.LoginPanel;
using UserInterface.PluginPanel;
using UserInterface.LoggedInAsPanel;
using UserInterface.Initializers;
using UserInterface.Scheduler;
using Communication;
using UserInterface.StaticContainer;
using Communication.Configuration;

namespace UserInterface.MainWindowClasses
{
    public class MainWindowPresenter
    {
        private IObserversManager _observersManager;
        private IMainWindow _view;
        private LoggedInAsControlPresenter _loggedInAsControl;
        private PluginControlPresenter _pluginPresenter;
        private AppDomain _domain;
        private ObserversDataStorage.ObserversDataStorage _storage;
        private DataScheduler _dataScheduler;
        private ConnectionScheduler _connectionScheduler;
        private Communicator _communicator;

        public MainWindowPresenter(IMainWindow view)
        {
            var applicationConfiguration = ApplicationSettingsManager.ApplicationConfiguration;            
            this._domain = AppDomainFactory.Create("Host_AppDomain", applicationConfiguration.ShadowPath, applicationConfiguration.ObserversPath, AppDomain.CurrentDomain.Evidence);           
            this._observersManager = ObserversManagerInitializer.InitializeAndGet(this._domain, applicationConfiguration.ObserversPath);

            _storage = new ObserversDataStorage.ObserversDataStorage();
            _observersManager.ObserverQueryOccured += _storage.OnDataOccured;
            _dataScheduler = DataSchedulerInitializer.InitializeAndGet(_storage, applicationConfiguration.RaportingPeriod);
            _connectionScheduler = new ConnectionScheduler(ApplicationSettingsManager.ResolveConfiguration.ToServiceConfiguration(), /*10 */ 60 * 1000);
            this.WireUpConnectionScheduler(_connectionScheduler);
            this._view = view;

            _observersManager.Start();
            this.SetUpLoggedInAsControl(view);
            this.SetUpPluginsControl(view);

            this.SetUpView();
            this.WireUpPresenters();
            _connectionScheduler.Start();
            _dataScheduler.Start();
        }

        private void WireUpConnectionScheduler(ConnectionScheduler _connectionScheduler)
        {
            _connectionScheduler.OnConnectionErrorOccured += OnConnectionErrorOccured;
            _connectionScheduler.OnConnectionSuccessOccured += OnConnectionSuccessOccured;
        }

        private void OnConnectionSuccessOccured(object sender, EventArgs e)
        {
            try {
                var conf = ApplicationSettingsManager.LoginConfiguration;
                var result = ApplicationContainer.Get.Communicator?.Login.LoginByAppKey(conf.Login, conf.AppKey);
                if (!result.IsSuccess)
                {
                    this._view.NotifyOnTray("UATU - the Watcher", "Cannot login in. Change credentials!", System.Windows.Forms.ToolTipIcon.Error);
                }
                else
                {
                    try
                    {
                        var configuration = ApplicationContainer.Get.Communicator.Configuration.GetConfiguration(conf.Login, conf.AppKey);
                        if (configuration.IsSuccess)
                        {
                            var appIdleExceptions = configuration.AppExceptions ?? new List<string>();
                            var webIdleExceptions = configuration.WebExceptions ?? new List<string>();
                            this._observersManager.Reconfigure(configuration.BrowsersExePaths, appIdleExceptions, webIdleExceptions);
                            this.SaveConfiguration(configuration);
                        }
                        else
                        {
                            this._view.NotifyOnTray("UATU - the Watcher", "Cannot load configuration!", System.Windows.Forms.ToolTipIcon.Warning);
                        }
                    }
                    catch (Exception exp)
                    {
                        this._view.NotifyOnTray("UATU - the Watcher", "Cannot load configuration!", System.Windows.Forms.ToolTipIcon.Warning);
                    }
                }
            }
            catch (Exception exp2)
            {

            }
        }

        private void SaveConfiguration(ConfigurationResult configuration)
        {
            var browserConf = ApplicationSettingsManager.BrowsersConfiguration;
            var appConf = ApplicationSettingsManager.ProgramsConfiguration;
            var list = new StringList();

            list.AddRange(configuration.WebExceptions ?? new List<string>());
            browserConf.IdleExceptionPaths = list;
            var dic = new DictionaryStringString();

            foreach (var item in configuration.BrowsersExePaths)
            {
                dic.Add(item.Key, item.Value);
            }

            browserConf.BrowsersExecutablePaths = dic;
            browserConf.Save();

            var appList = new StringList();
            appList.AddRange(configuration.AppExceptions);

            appConf.IdleExceptionPaths = appList;
            appConf.Save();
        }

        int counter = 0;
        private void OnConnectionErrorOccured(object sender, Exception e)
        {
            if(counter == 0 || counter % (2 * 60) == 0)
            {
                this._view.NotifyOnTray("UATU - the Watcher", "Cannot connect to resolving service!", System.Windows.Forms.ToolTipIcon.Info);
            }        
        }

        private void WireUpPresenters()
        {
            this._view.GetLoggedInAsControl().RelogClicked += RelogClicked;
        }

        private void RelogClicked(object sender, EventArgs e)
        {
            LoginActions.Login(true);
            this._view.GetLoggedInAsControl().UpdateView();
            OnConnectionSuccessOccured(null, null);
        }

        private void OnLoginIn(object sender, EventArgs e)
        {
            this._observersManager.Start();
        }

        private void SetUpView()
        {

        }

        private void SetUpPluginsControl(IMainWindow view)
        {
            this._pluginPresenter = new PluginPanel.PluginControlPresenter(this._observersManager, view.GetPluginControl());
        }

        private void SetUpLoggedInAsControl(IMainWindow view)
        {
            this._loggedInAsControl = new LoggedInAsControlPresenter(new LoggedInAsModel(), view.GetLoggedInAsControl());
        }
    }
}
