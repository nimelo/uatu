﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.Configurations;
using UserInterface.LoginPanel;
using UserInterface.StaticContainer;

namespace UserInterface.MainWindowClasses
{
    public static class LoginActions
    {
        public static void Login(bool allowExit = false)
        {
            var form = new LoginForm.LoginForm(allowExit);
            LoginPanel.LoginControlPresenter p = new LoginPanel.LoginControlPresenter(new LoginPanel.LoginControlModel(), form.GetLoginControl());
            p.LoggedIn += P_LoggedIn;
            var result = form.ShowDialog();            
            form.Dispose();
        }

        private static void P_LoggedIn(object sender, LoginResult e)
        {
            var configuration = ApplicationSettingsManager.LoginConfiguration;
            configuration.AppKey = e.AppPassword;
            configuration.Login = e.Login;
            configuration.Save();
        }

        public static bool TryLoginFromSettings()
        {
            var config = ApplicationSettingsManager.LoginConfiguration;
            var result = ApplicationContainer.Get.Communicator.Login.LoginByAppKey(config.Login, config.AppKey);
            if (result.IsSuccess)
            {
                config.AppKey = result.AppPassword;
                config.Login = result.Login;
                config.Save();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
