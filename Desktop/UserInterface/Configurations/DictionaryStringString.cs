﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class DictionaryStringString : Dictionary<string, string>
    {
        private static char RecordDelimiter = '|';
        private static string KeyValueDelimiter = @"-";

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> item in this)
            {
                sb.Append($"{item.Key}{KeyValueDelimiter}{item.Value}{RecordDelimiter}");
            }

            return sb.ToString();
        }

        public static DictionaryStringString FromString(string value)
        {
            DictionaryStringString obj = new DictionaryStringString();

            foreach (var keyValue in value.Split(new char[] { RecordDelimiter }, StringSplitOptions.RemoveEmptyEntries))
            {
                var splittedKeyValue = keyValue.Split(new string[] { KeyValueDelimiter }, StringSplitOptions.RemoveEmptyEntries);
                obj.Add(splittedKeyValue[0], splittedKeyValue[1]);
            }

            return obj;
        }
    }
}
