﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class LoginConfiguration : ConfigurationSection
    {
        #region Properties
        [ConfigurationProperty(nameof(Login))]
        public string Login
        {
            get { return (string)this[nameof(Login)]; }
            set { this[nameof(Login)] = value; }
        }

        [ConfigurationProperty(nameof(AppKey))]
        public string AppKey
        {
            get { return (string)this[nameof(AppKey)]; }
            set { this[nameof(AppKey)] = value; }
        }
        #endregion

        #region Methods        
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(LoginConfiguration)) as LoginConfiguration;

            if (section == null)
            {
                config.Sections.Add(nameof(LoginConfiguration), LoginConfiguration.Default);
                section = LoginConfiguration.Default;
            }

            section.Login = this.Login;
            section.AppKey = this.AppKey;

            config.Save();
        }
        #endregion

        #region Statics
        public static LoginConfiguration Default
        {
            get
            {
                return new LoginConfiguration()
                {
                    Login = string.Empty,
                    AppKey = string.Empty                   
                };
            }
        }
        #endregion
    
    }
}
