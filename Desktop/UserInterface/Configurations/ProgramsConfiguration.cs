﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class ProgramsConfiguration : ConfigurationSection
    {
        #region Properties
        [ConfigurationProperty(nameof(IdleExceptionPaths))]
        [TypeConverter(typeof(StringListConverter))]
        public StringList IdleExceptionPaths
        {
            get { return (StringList)this[nameof(IdleExceptionPaths)]; }
            set { this[nameof(IdleExceptionPaths)] = value; }
        }       
        #endregion

        #region Methods        
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(ProgramsConfiguration)) as ProgramsConfiguration;

            if (section == null)
            {
                config.Sections.Add(nameof(ProgramsConfiguration), ProgramsConfiguration.Default);
                section = ProgramsConfiguration.Default;
            }

            section.IdleExceptionPaths = this.IdleExceptionPaths;            
            config.Save();
        }
        #endregion

        #region Statics
        public static ProgramsConfiguration Default
        {
            get
            {
                return new ProgramsConfiguration()
                {
                    IdleExceptionPaths = new StringList()
                    {
                        @"C:\Program Files\MPC-HC\mpc-hc64.exe", 
                    }
                };
            }
        }
        #endregion
    }
}
