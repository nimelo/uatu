﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class ServerConfiguration : ConfigurationSection
    {
        #region Properties
        [ConfigurationProperty(nameof(Address))]
        public string Address
        {
            get { return (string)this[nameof(Address)]; }
            set { this[nameof(Address)] = value; }
        }

        [ConfigurationProperty(nameof(Port))]
        public uint Port
        {
            get { return (uint)this[nameof(Port)]; }
            set { this[nameof(Port)] = value; }
        }
        #endregion

        #region Methods        
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(ServerConfiguration)) as ServerConfiguration;

            if(section == null)
            {
                config.Sections.Add(nameof(ServerConfiguration), ServerConfiguration.Default);
                section = ServerConfiguration.Default;
            }

            section.Address = this.Address;
            section.Port = this.Port;
            config.Save();
        }
        #endregion

        #region Statics
        public static ServerConfiguration Default
        {
            get
            {
                return new ServerConfiguration()
                {
                    Address = "127.0.0.1",
                    Port = 1111
                };
            }
        }
        #endregion
    }
}
