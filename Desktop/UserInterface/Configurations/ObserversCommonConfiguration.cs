﻿using System.Configuration;
using System.IO;

namespace UserInterface.Configurations
{
    public class ObserversCommonConfiguration : ConfigurationSection
    {
        #region Properties
        [ConfigurationProperty(nameof(IdlePeriod))]
        public uint IdlePeriod
        {
            get { return (uint)this[nameof(IdlePeriod)]; }
            set { this[nameof(IdlePeriod)] = value; }
        }

        [ConfigurationProperty(nameof(DumpsDirectory))]
        public string DumpsDirectory
        {
            get { return (string)this[nameof(DumpsDirectory)]; }
            set { this[nameof(DumpsDirectory)] = value; }
        }
        #endregion

        #region Methods
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(ObserversCommonConfiguration)) as ObserversCommonConfiguration;

            if (section == null)
            {
                config.Sections.Add(nameof(ObserversCommonConfiguration), ObserversCommonConfiguration.Default);
                section = ObserversCommonConfiguration.Default;
            }

            section.IdlePeriod = this.IdlePeriod;
            section.DumpsDirectory = this.DumpsDirectory;

            config.Save();
        }
        #endregion

        #region Statics
        public static ObserversCommonConfiguration Default
        {
            get
            {
                return new ObserversCommonConfiguration()
                {
                    IdlePeriod = 3 * 60 * 1000,
                    DumpsDirectory = Path.Combine(System.Environment.CurrentDirectory, "Dumps")
                };
            }
        }
        #endregion

    }
}