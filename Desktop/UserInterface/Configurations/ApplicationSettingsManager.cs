﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class ApplicationSettingsManager
    {
        public static LoginConfiguration LoginConfiguration
        {
            get
            {
                return Get(nameof(LoginConfiguration)) as LoginConfiguration;
            }
        }

        public static ServerConfiguration ServerConfiguration
        {
            get
            {
                return Get(nameof(ServerConfiguration)) as ServerConfiguration;
            }
        }

        public static ObserversCommonConfiguration ObserversCommonConfiguration
        {
            get
            {
                return Get(nameof(ObserversCommonConfiguration)) as ObserversCommonConfiguration;
            }
        }

        public static ApplicationConfiguration ApplicationConfiguration
        {
            get
            {
                return Get(nameof(ApplicationConfiguration)) as ApplicationConfiguration;
            }
        }

        public static BrowsersConfiguration BrowsersConfiguration
        {
            get
            {
                return Get(nameof(BrowsersConfiguration)) as BrowsersConfiguration;
            }
        }

        public static ProgramsConfiguration ProgramsConfiguration
        {
            get
            {
                return Get(nameof(ProgramsConfiguration)) as ProgramsConfiguration;
            }
        }

        public static ResolveConfiguration ResolveConfiguration
        {
            get
            {
                return Get(nameof(ResolveConfiguration)) as ResolveConfiguration;
            }
        }

        private static ConfigurationSection Get(string name)
        {
            var configAll = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var config = configAll.GetSection(name);

            return config;
        }

        public static void Initialize()
        {
            var login = Get(nameof(LoginConfiguration)) as LoginConfiguration;
            if(login == null) LoginConfiguration.Default.Save();

            var server = Get(nameof(ServerConfiguration)) as ServerConfiguration;
            if (server == null) ServerConfiguration.Default.Save();

            var observersCommon = Get(nameof(ObserversCommonConfiguration)) as ObserversCommonConfiguration;
            if (observersCommon == null) ObserversCommonConfiguration.Default.Save();

            var applicationConfiguration = Get(nameof(ApplicationConfiguration)) as ApplicationConfiguration;
            if (applicationConfiguration == null) ApplicationConfiguration.Default.Save();

            var browsersConfig = Get(nameof(BrowsersConfiguration)) as BrowsersConfiguration;
            if (browsersConfig == null) BrowsersConfiguration.Default.Save();

            var programsConfig = Get(nameof(ProgramsConfiguration)) as ProgramsConfiguration;
            if (programsConfig == null) ProgramsConfiguration.Default.Save();

            var resolveConfig = Get(nameof(ResolveConfiguration)) as ResolveConfiguration;
            if (resolveConfig == null) ResolveConfiguration.Default.Save();
        }
    }
}
