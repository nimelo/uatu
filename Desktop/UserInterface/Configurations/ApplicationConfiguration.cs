﻿using System.Configuration;
using System.IO;

namespace UserInterface.Configurations
{
    public class ApplicationConfiguration : ConfigurationSection
    {

        #region Properties
        [ConfigurationProperty(nameof(ShadowPath))]
        public string ShadowPath
        {
            get { return (string)this[nameof(ShadowPath)]; }
            set { this[nameof(ShadowPath)] = value; }
        }

        [ConfigurationProperty(nameof(ObserversPath))]
        public string ObserversPath
        {
            get { return (string)this[nameof(ObserversPath)]; }
            set { this[nameof(ObserversPath)] = value; }
        }

        [ConfigurationProperty(nameof(RaportPath))]
        public string RaportPath
        {
            get { return (string)this[nameof(RaportPath)]; }
            set { this[nameof(RaportPath)] = value; }
        }

        [ConfigurationProperty(nameof(RaportingPeriod))]
        public long RaportingPeriod
        {
            get { return (long)this[nameof(RaportingPeriod)]; }
            set { this[nameof(RaportingPeriod)] = value; }
        }
        #endregion

        #region Methods        
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(ApplicationConfiguration)) as ApplicationConfiguration;

            if (section == null)
            {
                config.Sections.Add(nameof(ApplicationConfiguration), ApplicationConfiguration.Default);
                section = ApplicationConfiguration.Default;
            }

            section.ShadowPath = this.ShadowPath;
            section.ObserversPath = this.ObserversPath;
            section.RaportPath = this.RaportPath;
            section.RaportingPeriod = this.RaportingPeriod;

            config.Save();
        }
        #endregion

        #region Statics
        public static ApplicationConfiguration Default
        {
            get
            {
                Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "Shadow"));
                Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "Observers"));
                Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "Raports"));
                return new ApplicationConfiguration()
                {
                    ShadowPath = Path.Combine(System.Environment.CurrentDirectory, "Shadow"),
                    ObserversPath = Path.Combine(System.Environment.CurrentDirectory, "Observers"),
                    RaportPath = Path.Combine(System.Environment.CurrentDirectory, "Raports"),
                    RaportingPeriod = 10 * 60 * 1000,
                };
            }
        }
        #endregion

    }
}