﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class StringList : List<string>
    {
        private static char Delimiter = '|';

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (string item in this)
            {
                sb.Append(item).Append(Delimiter);
            }

            return sb.ToString();
        }

        public static StringList FromString(string value)
        {
            var splittedStrings = value.Split(new char[] { Delimiter }, StringSplitOptions.RemoveEmptyEntries);
            var list = new StringList();

            foreach (var str in splittedStrings)
            {
                list.Add(str);
            }

            return list;
        }
    }
}
