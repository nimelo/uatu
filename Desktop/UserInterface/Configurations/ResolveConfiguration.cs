﻿using System;
using System.Configuration;
using Communication;

namespace UserInterface.Configurations
{
    public class ResolveConfiguration : ConfigurationSection
    {

        #region Properties
        [ConfigurationProperty(nameof(Address))]
        public string Address
        {
            get { return (string)this[nameof(Address)]; }
            set { this[nameof(Address)] = value; }
        }

        [ConfigurationProperty(nameof(Port))]
        public string Port
        {
            get { return (string)this[nameof(Port)]; }
            set { this[nameof(Port)] = value; }
        }

        [ConfigurationProperty(nameof(Path))]
        public string Path
        {
            get { return (string)this[nameof(Path)]; }
            set { this[nameof(Path)] = value; }
        }
        #endregion

        #region Methods        
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(ResolveConfiguration)) as ResolveConfiguration;

            if (section == null)
            {
                config.Sections.Add(nameof(ResolveConfiguration), ResolveConfiguration.Default);
                section = ResolveConfiguration.Default;
            }

            section.Path = this.Path;
            section.Address = this.Address;
            section.Port = this.Port;

            config.Save();
        }
        #endregion

        #region Statics
        public static ResolveConfiguration Default
        {
            get
            {
                return new ResolveConfiguration()
                {
                    Address = "127.0.0.1",
                    Port = "9000",
                    Path = "resolve"
                };
            }
        }
        #endregion

        public ServiceConfiguration ToServiceConfiguration()
        {
            return new ServiceConfiguration()
            {
                Port = this.Port,
                Address = this.Address,
                Path = this.Path
            };
        }
    }
}