﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class BrowsersConfiguration : ConfigurationSection
    {
        #region Properties
        [ConfigurationProperty(nameof(IdleExceptionPaths))]
        [TypeConverter(typeof(StringListConverter))]
        public StringList IdleExceptionPaths
        {
            get { return (StringList)this[nameof(IdleExceptionPaths)]; }
            set { this[nameof(IdleExceptionPaths)] = value; }
        }

        [ConfigurationProperty(nameof(BrowsersExecutablePaths))]
        [TypeConverter(typeof(DictionaryStringStringConverter))]
        public DictionaryStringString BrowsersExecutablePaths
        {
            get { return (DictionaryStringString)this[nameof(BrowsersExecutablePaths)]; }
            set { this[nameof(BrowsersExecutablePaths)] = value; }
        }
        #endregion

        #region Methods        
        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var section = config.GetSection(nameof(BrowsersConfiguration)) as BrowsersConfiguration;

            if (section == null)
            {
                config.Sections.Add(nameof(BrowsersConfiguration), BrowsersConfiguration.Default);
                section = BrowsersConfiguration.Default;
            }

            section.IdleExceptionPaths = this.IdleExceptionPaths;
            section.BrowsersExecutablePaths = this.BrowsersExecutablePaths;
            config.Save();
        }
        #endregion

        #region Statics
        public static BrowsersConfiguration Default
        {
            get
            {
                return new BrowsersConfiguration()
                {
                    IdleExceptionPaths = new StringList()
                    {
                        @"https://www.twitch.tv/", @"https://www.youtube.com/watch?"
                    },
                    BrowsersExecutablePaths = new DictionaryStringString()
                    {
                        {"iexplore", @"C:\Program Files\Internet Explorer\iexplore.exe"},
                        {"firefox",  @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe"}
                    }
                };
            }
        }
        #endregion
    }
}
