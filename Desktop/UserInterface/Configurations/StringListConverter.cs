﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;

namespace UserInterface.Configurations
{
    public sealed class StringListConverter : ConfigurationConverterBase
    {
        internal bool ValidateType(object value, Type expected)
        {
            bool result;

            if ((value != null) &&
                (value.GetType() != expected))
                result = false;
            else
                result = true;

            return result;
        }

        public override bool CanConvertTo(ITypeDescriptorContext ctx, Type type)
        {
            return (type == typeof(string));
        }

        public override bool CanConvertFrom(ITypeDescriptorContext ctx, Type type)
        {
            return (type == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
        {
            ValidateType(value, typeof(StringList));
         
            return (value as StringList).ToString();
        }

        public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
        {
            return StringList.FromString(data as string);
        }
    }
}