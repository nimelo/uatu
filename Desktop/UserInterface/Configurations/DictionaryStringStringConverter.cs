﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Configurations
{
    public class DictionaryStringStringConverter : ConfigurationConverterBase
    {
        internal bool ValidateType(object value, Type expected)
        {
            bool result;

            if ((value != null) &&
                (value.GetType() != expected))
                result = false;
            else
                result = true;

            return result;
        }

        public override bool CanConvertTo(ITypeDescriptorContext ctx, Type type)
        {
            return (type == typeof(string));
        }

        public override bool CanConvertFrom(ITypeDescriptorContext ctx, Type type)
        {
            return (type == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
        {
            ValidateType(value, typeof(DictionaryStringString));

            return (value as DictionaryStringString).ToString();
        }

        public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
        {
            return DictionaryStringString.FromString(data as string);
        }
    
    }
}
