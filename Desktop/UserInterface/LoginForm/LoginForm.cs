﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UserInterface.LoginPanel;

namespace UserInterface.LoginForm
{
    public partial class LoginForm : Form
    {
        private LoginControlPresenter loginControlPresenter;

        public LoginForm(bool allowExit = false)
        {
            InitializeComponent();

            if (allowExit)
            {
                this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            }

            this.SetUp();
            this.WireUp();
        }

        private void WireUp()
        {
            this.loginControlPresenter.LoggedIn += LoginControlPresenter_LoggedIn;
        }

        private void LoginControlPresenter_LoggedIn(object sender, LoginResult e)
        {
            this.Close();
        }

        private void SetUp()
        {
            this.loginControlPresenter = new LoginPanel.LoginControlPresenter(new LoginPanel.LoginControlModel(), this.loginControl1);
        }

        public LoginControl GetLoginControl()
        {
            return this.loginControl1;
        }
    }
}
