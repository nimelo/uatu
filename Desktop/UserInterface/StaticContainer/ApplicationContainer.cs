﻿using Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.StaticContainer
{
    public class ApplicationContainer
    {
        private static ApplicationContainer instance;

        private ApplicationContainer()
        {
        }

        public static ApplicationContainer Get
        {
            get
            {
                if(instance == null)
                {
                    instance = new ApplicationContainer();
                }

                return instance;
            }
        }

        public Communicator Communicator { get; set; }
    }
}
