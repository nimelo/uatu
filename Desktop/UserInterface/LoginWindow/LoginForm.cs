﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface.LoginWindow
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void loginControl1_Load(object sender, EventArgs e)
        {
            var window  = new MainWindow();
            window.ShowDialog();
            Application.Exit();
        }
    }
}
