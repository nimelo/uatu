﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.LoginPanel
{
    public partial class LoginControlPresenter
    {
        private LoginControlModel _model;
        private LoginControl _view;

        public LoginControlPresenter(LoginControlModel model, LoginControl view)
        {
            this._model = model;
            this._view = view;
            this.WireUpViewEvents();
        }

        private void WireUpViewEvents()
        {
            this._view.LoginButtonClicked += OnLoginButtonClicked;
        }

        private void OnLoginButtonClicked(object sender, LoginButtonData e)
        {
            try
            {
                this._model.Login = e.Login;
                this._model.Password = e.Password;
                var value = this._model.LoginIn();
                if (value.Success)
                {
                    LoggedIn?.Invoke(this, value);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Invalid Credentials!");
                }
            }
            catch (Exception exp)
            {
                System.Windows.Forms.MessageBox.Show("Cannot connect to service!");
            }                 
        }

        public event EventHandler<LoginResult> LoggedIn;
    }
}
