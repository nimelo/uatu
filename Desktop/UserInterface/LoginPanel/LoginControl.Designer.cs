﻿namespace UserInterface.LoginPanel
{
    partial class LoginControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginControlMainPanel = new System.Windows.Forms.Panel();
            this.loginControlLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.middleRowTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.loginTextBox = new System.Windows.Forms.TextBox();
            this.loginButton = new System.Windows.Forms.Button();
            this.loginControlMainPanel.SuspendLayout();
            this.loginControlLayoutPanel.SuspendLayout();
            this.middleRowTableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginControlMainPanel
            // 
            this.loginControlMainPanel.Controls.Add(this.loginControlLayoutPanel);
            this.loginControlMainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginControlMainPanel.Location = new System.Drawing.Point(0, 0);
            this.loginControlMainPanel.Name = "loginControlMainPanel";
            this.loginControlMainPanel.Size = new System.Drawing.Size(357, 60);
            this.loginControlMainPanel.TabIndex = 0;
            // 
            // loginControlLayoutPanel
            // 
            this.loginControlLayoutPanel.ColumnCount = 2;
            this.loginControlLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.loginControlLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.loginControlLayoutPanel.Controls.Add(this.middleRowTableLayout, 0, 0);
            this.loginControlLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginControlLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.loginControlLayoutPanel.Name = "loginControlLayoutPanel";
            this.loginControlLayoutPanel.RowCount = 1;
            this.loginControlLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.loginControlLayoutPanel.Size = new System.Drawing.Size(357, 60);
            this.loginControlLayoutPanel.TabIndex = 0;
            this.loginControlLayoutPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.loginControlLayoutPanel_Paint);
            // 
            // middleRowTableLayout
            // 
            this.middleRowTableLayout.ColumnCount = 2;
            this.loginControlLayoutPanel.SetColumnSpan(this.middleRowTableLayout, 2);
            this.middleRowTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.middleRowTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.middleRowTableLayout.Controls.Add(this.passwordTextBox, 0, 1);
            this.middleRowTableLayout.Controls.Add(this.loginTextBox, 0, 0);
            this.middleRowTableLayout.Controls.Add(this.loginButton, 1, 0);
            this.middleRowTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.middleRowTableLayout.Location = new System.Drawing.Point(3, 3);
            this.middleRowTableLayout.Name = "middleRowTableLayout";
            this.middleRowTableLayout.RowCount = 2;
            this.middleRowTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.middleRowTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.middleRowTableLayout.Size = new System.Drawing.Size(351, 54);
            this.middleRowTableLayout.TabIndex = 2;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passwordTextBox.Location = new System.Drawing.Point(3, 30);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(257, 20);
            this.passwordTextBox.TabIndex = 1;
            this.passwordTextBox.UseSystemPasswordChar = true;
            // 
            // loginTextBox
            // 
            this.loginTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginTextBox.Location = new System.Drawing.Point(3, 3);
            this.loginTextBox.Name = "loginTextBox";
            this.loginTextBox.Size = new System.Drawing.Size(257, 20);
            this.loginTextBox.TabIndex = 0;
            this.loginTextBox.UseSystemPasswordChar = true;
            // 
            // loginButton
            // 
            this.loginButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginButton.Location = new System.Drawing.Point(266, 3);
            this.loginButton.Name = "loginButton";
            this.middleRowTableLayout.SetRowSpan(this.loginButton, 2);
            this.loginButton.Size = new System.Drawing.Size(82, 48);
            this.loginButton.TabIndex = 2;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // LoginControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.loginControlMainPanel);
            this.Name = "LoginControl";
            this.Size = new System.Drawing.Size(357, 60);
            this.loginControlMainPanel.ResumeLayout(false);
            this.loginControlLayoutPanel.ResumeLayout(false);
            this.middleRowTableLayout.ResumeLayout(false);
            this.middleRowTableLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel loginControlMainPanel;
        private System.Windows.Forms.TableLayoutPanel loginControlLayoutPanel;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox loginTextBox;
        private System.Windows.Forms.TableLayoutPanel middleRowTableLayout;
        private System.Windows.Forms.Button loginButton;
    }
}
