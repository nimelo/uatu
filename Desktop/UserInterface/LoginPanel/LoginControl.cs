﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface.LoginPanel
{
    public partial class LoginControl : UserControl, ILoginControl
    {
        public LoginControl()
        {
            InitializeComponent();
        }

        public event EventHandler<LoginButtonData> LoginButtonClicked;

        public void OnLoginButtonClicked(LoginButtonData e)
        {
            this.LoginButtonClicked?.Invoke(this, e);
        }

        private void loginControlLayoutPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            OnLoginButtonClicked(new LoginButtonData()
            {
                Login = this.loginTextBox.Text,
                Password = this.passwordTextBox.Text
            });
        }
    }
}
