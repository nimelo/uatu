﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.StaticContainer;

namespace UserInterface.LoginPanel
{
    public class LoginControlModel
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public LoginResult LoginIn()
        {
            try
            {
                var result = ApplicationContainer.Get.Communicator.Login.LoginCredentials(this.Login, this.Password);
                if (result.IsSuccess)
                {
                    return new LoginResult()
                    {
                        Login = result.Login,
                        AppPassword = result.AppPassword,
                        Success = true                     
                    };
                }
                else
                {
                    return new LoginResult()
                    {
                        Success = false
                    };
                }
            }
            catch (Exception e)
            {
                throw;
            }          
        }
    }
}
