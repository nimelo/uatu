﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.LoginPanel
{
    interface ILoginControl
    {
        event EventHandler<LoginButtonData> LoginButtonClicked;

        void OnLoginButtonClicked(LoginButtonData e);
    }
}
