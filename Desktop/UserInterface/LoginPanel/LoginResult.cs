﻿namespace UserInterface.LoginPanel
{
    public class LoginResult
    {
        public string Login { get; set; }
        public string AppPassword { get; set; }
        public bool Success { get; set; }
    }
}