﻿namespace UserInterface.LoginPanel
{
    public class LoginButtonData
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}