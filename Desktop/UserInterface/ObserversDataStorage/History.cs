﻿using System;
using System.Collections.Generic;
using Common.Observer.ApplicationObserver;
using Common.Observer.BrowserObserver;
using Common.Observer.HardwareObserver;

namespace UserInterface.ObserversDataStorage
{
    public class History
    {
        public Dictionary<string, ApplicationData> ApplicationsHistory { get; set; }
        public Dictionary<string, BrowserData> WebsidesHistory { get; set; }
        public Dictionary<string, HardwareData> HardwaresHistory { get; set; }
        public YearMonthDay Time { get; set; }

        public History()
        {
            this.ApplicationsHistory = new Dictionary<string, ApplicationData>();
            this.WebsidesHistory = new Dictionary<string, BrowserData>();
            this.HardwaresHistory = new Dictionary<string, HardwareData>();
        }

        public History(YearMonthDay time) : this()
        {
            this.Time = time;
        }

        public void ResolveBrowserQueryResult(AbstractBrowserObserver abstractBrowserObserver, BrowserQueryResult browserQueryResult)
        {
            if (this.WebsidesHistory.ContainsKey(browserQueryResult.Value))
            {
                this.WebsidesHistory[browserQueryResult.Value].History.Add(new FromTo(browserQueryResult.From, browserQueryResult.Time));
            }
            else
            {
                this.WebsidesHistory.Add(browserQueryResult.Value, new BrowserData());
                this.WebsidesHistory[browserQueryResult.Value].History.Add(new FromTo(browserQueryResult.From, browserQueryResult.Time));
            }
        }

        public void ResolveHardwareObserver(AbstractHardwareObserver abstractHardwareObserver, HardwareQueryResult hardwareQueryResult)
        {
            if (this.HardwaresHistory.ContainsKey(abstractHardwareObserver.Name))
            {
                var history = this.HardwaresHistory[abstractHardwareObserver.Name];
                this.FullfillHardwareData(history, hardwareQueryResult.Value);
            }
            else
            {
                var hd = new HardwareData(hardwareQueryResult.Frequency, hardwareQueryResult.From);
                this.HardwaresHistory.Add(abstractHardwareObserver.Name, hd);
                this.FullfillHardwareData(hd, hardwareQueryResult.Value);
            }
        }

        private void FullfillHardwareData(HardwareData history, Dictionary<string, double> value)
        {
            foreach (KeyValuePair<string, double> item in value)
            {
                if (history.Values.ContainsKey(item.Key))
                {
                    history.Values[item.Key].Add(item.Value);
                }
                else
                {
                    var list = new List<double>() { item.Value };
                    history.Values.Add(item.Key, list);
                }
            }
        }

        public void ResolveApplicationObserverResult(AbstractApplicationObserver abstractApplicationObserver, ApplicationObserverQueryResult applicationObserverQueryResult)
        {
            if (this.ApplicationsHistory.ContainsKey(applicationObserverQueryResult.Value))
            {
                this.ApplicationsHistory[applicationObserverQueryResult.Value].History.Add(new FromTo(applicationObserverQueryResult.From, applicationObserverQueryResult.Time));
            }
            else
            {
                this.ApplicationsHistory.Add(applicationObserverQueryResult.Value, new ApplicationData());
                this.ApplicationsHistory[applicationObserverQueryResult.Value].History.Add(new FromTo(applicationObserverQueryResult.From, applicationObserverQueryResult.Time));
            }
        }
    }
}