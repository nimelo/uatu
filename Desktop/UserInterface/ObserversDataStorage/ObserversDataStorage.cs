﻿using Common.Observer;
using Common.Observer.ApplicationObserver;
using Common.Observer.BrowserObserver;
using Common.Observer.HardwareObserver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.ObserversDataStorage
{
    public class ObserversDataStorage : MarshalByRefObject
    {
        private object _lock = new object();

        public HashSet<DailyObserverStorage> Storage { get; set; }

        public ObserversDataStorage()
        {
            this.Storage = new HashSet<DailyObserverStorage>();
        }

        public void OnDataOccured(object sender, Common.Observer.AbstractQueryResult e)
        {
            lock (_lock)
            {
                var ymd = YearMonthDay.FromUTCMiliseconds(e.From);

                if (this.Storage.Any(x => x.Time.Equals(ymd)))
                {
                    this.Storage.Where(x => x.Time.Equals(ymd)).SingleOrDefault()?.OnDataOccured(sender, e);
                }
                else
                {
                    this.Storage.Add(new DailyObserverStorage(ymd));
                    this.Storage.Where(x => x.Time.Equals(ymd)).SingleOrDefault()?.OnDataOccured(sender, e);
                }
            }
        }

        public HashSet<DailyObserverStorage> ChangeStorage()
        {
            HashSet<DailyObserverStorage> toReturn;
            lock (_lock)
            {
                toReturn = this.Storage;
                this.Storage = new HashSet<DailyObserverStorage>();
            }

            return toReturn;
        }
    }
}
