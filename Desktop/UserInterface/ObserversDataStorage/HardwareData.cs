﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UserInterface.ObserversDataStorage
{
    [Serializable]
    public class HardwareData
    {
        public IDictionary<string, List<double>> Values { get; set; }
        public long QueryFrequency { get; set; }
        public long From { get; set; }

        public HardwareData()
        {
            this.Values = new Dictionary<string, List<double>>();
        }

        public HardwareData(long freq, long from)
        {
            this.From = from;
            this.QueryFrequency = freq;
            this.Values = new Dictionary<string, List<double>>();
        }
    }
}