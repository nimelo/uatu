﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.ObserversDataStorage
{
    [Serializable]
    public class FromTo
    {
        public long From { get; set; }
        public long Time { get; set; }

        public FromTo(long from, long time)
        {
            this.From = from;
            this.Time = time;
        }

        public FromTo()
        {

        }
    }
}
