﻿using Common.Observer.ApplicationObserver;
using Common.Observer.BrowserObserver;
using Common.Observer.HardwareObserver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.ObserversDataStorage
{
    [Serializable]
    public class DailyObserverStorage : MarshalByRefObject
    {
        public Statistics Statistics { get; set; }
        public History History { get; set; }
        public YearMonthDay Time { get; set; }

        public DailyObserverStorage(YearMonthDay ymd)
        {
            this.Time = ymd;
            this.Statistics = new Statistics(ymd);
            this.History = new History(ymd);
        }

        public DailyObserverStorage()
        {
            this.History = new History();
            this.Statistics = new Statistics();
        }

        public void OnDataOccured(object sender, Common.Observer.AbstractQueryResult e)
        {
            if (sender is AbstractApplicationObserver)
            {
                this.ResolveApplicationObserver(sender as AbstractApplicationObserver, e as ApplicationObserverQueryResult);
            }
            else if (sender is AbstractBrowserObserver)
            {
                this.ResolveBrowsersObserver(sender as AbstractBrowserObserver, e as Common.Observer.BrowserObserver.BrowserQueryResult);
            }
            else if (sender is AbstractHardwareObserver)
            {
                this.ResolveHardwareObserver(sender as AbstractHardwareObserver, e as HardwareQueryResult);
            }
        }

        private void ResolveBrowsersObserver(AbstractBrowserObserver abstractBrowserObserver, Common.Observer.BrowserObserver.BrowserQueryResult browserQueryResult)
        {
            this.Statistics.ResolveBrowserObserverResult(abstractBrowserObserver, browserQueryResult);
            this.History.ResolveBrowserQueryResult(abstractBrowserObserver, browserQueryResult);
        }

        private void ResolveHardwareObserver(AbstractHardwareObserver abstractHardwareObserver, HardwareQueryResult hardwareQueryResult)
        {
            this.History.ResolveHardwareObserver(abstractHardwareObserver, hardwareQueryResult);
        }

        private void ResolveApplicationObserver(AbstractApplicationObserver abstractApplicationObserver, ApplicationObserverQueryResult applicationObserverQueryResult)
        {
            this.Statistics.ResolveApplicationObserverResult(abstractApplicationObserver, applicationObserverQueryResult);
            this.History.ResolveApplicationObserverResult(abstractApplicationObserver, applicationObserverQueryResult);
        }
    }
}
