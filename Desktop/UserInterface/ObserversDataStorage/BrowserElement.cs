﻿using Common.Observer.BrowserObserver;

namespace UserInterface.ObserversDataStorage
{
    public class BrowserHistoryElement
    {
        public string Url { get; set; }
        public long Time { get; set; }
        public long From { get; set; }

        public static BrowserHistoryElement FromBrowserQueryResult(BrowserQueryResult e)
        {
            return new BrowserHistoryElement()
            {
                Url = e.Value,
                From = e.From,
                Time = e.Time
            };
        }
    }
}