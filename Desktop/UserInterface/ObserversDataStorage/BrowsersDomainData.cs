﻿using Common.Observer.BrowserObserver;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UserInterface.ObserversDataStorage
{
    [Serializable]
    public class BrowsersDomainData
    {
        public List<BrowserHistoryElement> History { get; set; }

        public BrowsersDomainData()
        {
            this.History = new List<BrowserHistoryElement>();           
        }
    }
}