﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.ObserversDataStorage
{
    [Serializable]
    public class YearMonthDay
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public override int GetHashCode()
        {
            return 0;
        }

        public YearMonthDay(int year, int month, int day)
        {
            this.Year = year;
            this.Month = month;
            this.Day = day;
        }

        public YearMonthDay()
        {

        }

        public override bool Equals(object obj)
        {
            if(obj is YearMonthDay)
            {
                var ymd = obj as YearMonthDay;
                return (this.Year == ymd.Year && this.Month == ymd.Month && this.Day == ymd.Day);
            }
            return base.Equals(obj);
        }

        public static YearMonthDay FromUTCMiliseconds(long time)
        {
            var date = DateTimeOffset.FromUnixTimeMilliseconds(time).Date;

            return new YearMonthDay(date.Year, date.Month, date.Day);
        }
    }
}
