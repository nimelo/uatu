﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.ObserversDataStorage
{
    public class DailyObserverStorageEqualityComparer : IEqualityComparer<DailyObserverStorage>
    {
        public bool Equals(DailyObserverStorage x, DailyObserverStorage y)
        {
            return x.Time.Equals(y.Time);
        }

        public int GetHashCode(DailyObserverStorage obj)
        {
            return 0;
        }
    }
}
