﻿namespace UserInterface.ObserversDataStorage
{
    public class StringLongContainer
    {
        public string StringValue { get; set; }
        public long LongValue { get; set; }

        public StringLongContainer(string strValue, long longValue)
        {
            this.StringValue = strValue;
            this.LongValue = longValue;
        }

        public override bool Equals(object obj)
        {
            if(obj is StringLongContainer)
            {
                return this.StringValue == (obj as StringLongContainer).StringValue;
            }

            return base.Equals(obj);
        }
    }
}