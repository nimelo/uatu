﻿using System.Collections.Generic;

namespace UserInterface.ObserversDataStorage
{
    public class BrowserData
    {
        public List<FromTo> History { get; set; }

        public BrowserData()
        {
            History = new List<FromTo>();
        }
    }
}