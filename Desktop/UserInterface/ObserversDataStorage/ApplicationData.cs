﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UserInterface.ObserversDataStorage
{
    [Serializable]
    public class ApplicationData
    {
        public List<FromTo> History { get; set; }

        public ApplicationData()
        {
            History = new List<FromTo>();
        }
    }  
}