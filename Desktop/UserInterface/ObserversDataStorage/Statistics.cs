﻿using System;
using System.Collections.Generic;
using Common.Observer.ApplicationObserver;
using Common.Observer.BrowserObserver;

namespace UserInterface.ObserversDataStorage
{
    public class Statistics
    {
        public Dictionary<string, long> ApplicationStatistics { get; set; }
        public Dictionary<string, long> WebsidesStatistics { get; set; }
        public YearMonthDay Time { get; set; }

        public Statistics()
        {
            this.ApplicationStatistics = new Dictionary<string, long>();
            this.WebsidesStatistics = new Dictionary<string, long>();
        }

        public Statistics(YearMonthDay time) : this()
        {
            this.Time = time;
        }

        public void ResolveBrowserObserverResult(AbstractBrowserObserver abstractBrowserObserver, BrowserQueryResult browserQueryResult)
        {
            if (this.WebsidesStatistics.ContainsKey(ExtractDomain(browserQueryResult.Value)))
            {
                this.WebsidesStatistics[ExtractDomain(browserQueryResult.Value)] += browserQueryResult.Time;
            }
            else
            {
                this.WebsidesStatistics.Add(ExtractDomain(browserQueryResult.Value), browserQueryResult.Time);
            }
        }

        public void ResolveApplicationObserverResult(AbstractApplicationObserver abstractApplicationObserver, ApplicationObserverQueryResult applicationObserverQueryResult)
        {
            if (this.ApplicationStatistics.ContainsKey(applicationObserverQueryResult.Value))
            {
                this.ApplicationStatistics[applicationObserverQueryResult.Value] += applicationObserverQueryResult.Time;
            }
            else
            {
                this.ApplicationStatistics.Add(applicationObserverQueryResult.Value, applicationObserverQueryResult.Time);
            }
        }

        private string ExtractDomain(string url)
        {
            string domain;
            if (url.IndexOf(@"://") > -1)
            {
                domain = url.Split('/')[2];
            }
            else
            {
                domain = url.Split('/')[0];
            }

            return domain.Split(':')[0];
        }
    }
}