﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramsPlugin
{
    public static class ProgramsPluginAdditionalDataExtractor
    {
        public static string ExtractExecutionPath(uint pid)
        {
            var process = Process.GetProcessById((int)pid);

            return process.Modules[0].FileName;
        }
    }
}
