﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.EventArgs;
using Common.ProgramPlugin;
using ProgramsPlugin.Raport;
using Common.Raports;

namespace ProgramsPlugin
{
    public class ProgramsPlugin : AbstractProgramPlugin<ProgramPluginContainer>
    {
        #region Public 
        public override string ServiceName()
        {
            return "programs";
        }

        public override string GetName()
        {
            return nameof(ProgramsPlugin);
        }

        public override uint GetTimePeriod()
        {
            return 1000;
        }

        #endregion

        #region Overrided Methdods
        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == MachineStateEnum.IDLE)
            {
                this.CanHandle = false;
                var lastSwap = this.CurrentObject.ClearValue();
           
#if (DEBUG)
                Console.WriteLine($"{lastSwap}");
#endif
            }
            else if (e.State == MachineStateEnum.RESUME)
            {
                this.CanHandle = true;
            }
        }

        protected override void Handle(ElapsedEventArgs e)
        {
            IntPtr handle = this.GetForegroundWindowHandle();
            uint pId = this.GetWindowThreadProcessId(handle);

            if (this.CurrentObject.IsEmpty())
            {
                this.CurrentObject.SetValue(new ProgramPluginContainer(pId, ProgramsPluginAdditionalDataExtractor.ExtractExecutionPath(pId)));
            }
            else
            {
                if (!this.CurrentObject.Equals(pId))
                {
                    var lastSwap = this.CurrentObject.SwapValue(new ProgramPluginContainer(pId, ProgramsPluginAdditionalDataExtractor.ExtractExecutionPath(pId)));
                  

#if (DEBUG)
                    Console.WriteLine($"{lastSwap}");
#endif
                }
            }
        }
        #endregion

        #region Fields
        private uint[] systemPids = { 0, 4, 8 };
        #endregion

        #region Ctors
        public ProgramsPlugin()
        {
            this.CurrentObject = new ObjectWithSetMemory<ProgramPluginContainer>();
            this.CanHandle = true;
            this.CreateNewRaport();        
        }
        #endregion

        #region Private Methods
        void CreateNewRaport()
        {
         
        }
        #endregion

        #region Properties
        #endregion
    }
}
