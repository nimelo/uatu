﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramsPlugin
{
    public class ProgramPluginContainer
    {
        public uint ProcessId { get; set; }
        public string ExecutionPath { get; set; }

        public ProgramPluginContainer(uint pId, string path)
        {
            this.ProcessId = pId;
            this.ExecutionPath = path;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is uint)
            {
                return this.ProcessId == (uint)obj;
            }

            return base.Equals(obj);
        }

        public override string ToString()
        {
            return ExecutionPath;
        }
    }
}
