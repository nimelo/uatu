﻿using Common.Raports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramsPlugin.Raport
{
    public class ProgramPluginRaport 
    {
        #region IRaport

        #endregion

        #region Properties
        public ProgramPluginHistory history { get; set; }
        public ProgramPluginStatistics statistics { get; set; }
        #endregion

        #region Ctors
        public ProgramPluginRaport()
        {
            this.history = new ProgramPluginHistory();
            this.statistics = new ProgramPluginStatistics();
        }
        #endregion

        #region Methods
        public void AddObject(string obj, DateTime from, DateTime to)
        {
            this.history.AddObject(obj, from, to);
            this.statistics.AddObject(obj, from, to);
        }
        #endregion
    }
}
