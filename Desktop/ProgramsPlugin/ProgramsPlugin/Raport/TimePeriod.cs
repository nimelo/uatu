﻿using System;

namespace ProgramsPlugin.Raport
{
    public class TimePeriod
    {
        public TimePeriod()
        {
        }

        public long from { get; set; }
        public long to { get; set; }
    }
}