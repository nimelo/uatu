﻿using Common.Raports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramsPlugin.Raport
{
    public class ProgramPluginStatistics
    {
        #region IStatistics
        public void AddObject(string obj, DateTime from, DateTime to)
        {
            if (this.statistics.ContainsKey(obj))
            {
                this.statistics[obj] += (uint)(to - from).Seconds;
            }
            else
            {
                this.statistics.Add(obj, (uint)(to - from).Seconds);
            }
        }

        public IDictionary<string, uint> Get()
        {
            return this.statistics;
        }
        #endregion

        #region Fields
        public IDictionary<string, uint> statistics { get; set; }
        #endregion

        #region Ctors
        public ProgramPluginStatistics()
        {
            this.statistics = new Dictionary<string, uint>();
        }
        #endregion
    }
}
