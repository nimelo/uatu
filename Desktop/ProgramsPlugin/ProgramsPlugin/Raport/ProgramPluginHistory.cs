﻿using Common.Raports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramsPlugin.Raport
{
    public class ProgramPluginHistory
    {
        #region IHistory
        public void AddObject(string obj, DateTime from, DateTime to)
        {
            if (this.history.Keys.Contains(obj))
            {
                this.history[obj].Add(new TimePeriod()
                {
                    from = new DateTimeOffset(from).ToUnixTimeSeconds(),
                    to = new DateTimeOffset(to).ToUnixTimeSeconds()
                });
            }
            else
            {
                this.history.Add(obj, new List<TimePeriod>()
                {
                    new TimePeriod()
                    {
                        from = new DateTimeOffset(from).ToUnixTimeSeconds(),
                        to = new DateTimeOffset(to).ToUnixTimeSeconds()
                    }
                });
            }
        }

        public IDictionary<string, List<TimePeriod>> Get()
        {
            return this.history;
        }
        #endregion

        #region Properties
        public IDictionary<string, List<TimePeriod>> history { get; set; }
        #endregion

        #region Ctors
        public ProgramPluginHistory()
        {
            this.history = new Dictionary<string, List<TimePeriod>>();
        }
        #endregion
    }
}
