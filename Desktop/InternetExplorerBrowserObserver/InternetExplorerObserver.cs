﻿using Common.Observer.NonPeriodical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.EventArgs;
using System.Timers;
using Common.ExtensionMethods;
using System.Reflection;
using Common.Observer.BrowserObserver;
using Common.Objects;

namespace InternetExplorerBrowserObserver
{
    public class InternetExplorerObserver : AbstractBrowserObserver
    {
        #region Overrided methods
        public override string Name
        {
            get
            {
                return nameof(InternetExplorerObserver);
            }
        }

        public override string BrowserName
        {
            get
            {
                return @"iexplore";
            }
        }

        protected override string QueryObservedKey(ElapsedEventArgs elapsed)
        {
            var key = this.GetCurrentUrl();
            return key ?? this.Object.Value;
        }

        protected override void Observe(ElapsedEventArgs elapsed)
        {
            if (this.ExecutablePaths.Contains(InternetExplorerDriver.GetExecutionPath()))
            {
                base.Observe(elapsed);
            }
            else
            {
                if (this.Object.HasValue)
                {
                    var endedEvent = this.Object.RegisterEndEvent(DateTime.Now.ToUnixUtc());
                    this.Object.ClearObject();
                    this.GetQueryResultFromEndedEvent(endedEvent);
                }
            }
        }

        protected override BrowserQueryResult GetQueryResultFromEndedEvent(EndedEventObject<string, string> endedEvent)
        {
            return new BrowserQueryResult(endedEvent.Value, endedEvent.StartTime, endedEvent.EndTime - endedEvent.StartTime);
        }
        #endregion

        #region Private methods
        private string GetCurrentUrl()
        {
            var handle = InternetExplorerDriver.GetForegroundWindowHandle();
            return InternetExplorerDriver.GetUrlFromHandlesPath(handle, "WorkerW", "ReBarWindow32", "Address Band Root", "Edit");
        }
        #endregion

        #region Ctors
        public InternetExplorerObserver() : base()
        {
            this.ExecutablePaths?.Add(@"C:\Program Files\Internet Explorer\iexplore.exe");
        }
        #endregion
    }
}
