﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace InternetExplorerBrowserObserver
{
    public static class InternetExplorerDriver
    {
        public static string GetUrlFromHandlesPath(IntPtr mainWindowHandle, params string[] paths)
        {
            var handle = mainWindowHandle;
            foreach (var name in paths)
            {
                handle = FindWindowEx(handle, IntPtr.Zero, name, null);
            }

            StringBuilder sb = new StringBuilder(256);
            SendMessage(handle, WM_GETTEXT, new IntPtr(sb.Capacity), sb);

            return sb.ToString();
        }

        public static string GetExecutionPath()
        {
            uint pId = GetForegroundProcessId();
            var process = Process.GetProcessById((int)pId);

            return process.Modules[0].FileName;
        }

        #region DLL imports

        private static IntPtr WM_GETTEXT = new IntPtr(13);
        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint processId);

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(
           IntPtr hWndParent,
           IntPtr hWndChild,
           string WndClass,
           string WndTitle
           );

        [DllImport("user32.dll", EntryPoint = "SendMessageW", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr SendMessage(
            IntPtr hWnd,
            IntPtr Msg,
            IntPtr wParam,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder lParam
          );
        #endregion

        #region Private methods
        private static uint GetForegroundProcessId()
        {
            var handle = GetForegroundWindowHandle();
            var pId = GetWindowThreadProcessId(handle);

            return pId;
        }

        public static IntPtr GetForegroundWindowHandle()
        {
            return GetForegroundWindow();
        }

        private static uint GetWindowThreadProcessId(IntPtr windowHandle)
        {
            uint processId;

            GetWindowThreadProcessId(windowHandle, out processId);

            return processId;
        }
        #endregion
    }
}
