﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Configuration.Response
{
    public class ConfigurationResponse
    {
        public bool success { get; set; }
        public long raportingPeriod { get; set; }
        public List<string> appExceptions { get; set; }
        public List<string> webExceptions { get; set; }
        public Dictionary<string, string> browsersExePaths { get; set; }
    }
}
