﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Configuration.Request
{
    public class ConfigurationRequest
    {
        public string login { get; set; }
        public string appKey { get; set; }

        public ConfigurationRequest(string login, string appKey)
        {
            this.login = login;
            this.appKey = appKey;
        }
    }
}
