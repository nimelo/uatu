﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Configuration
{
    public class ConfigurationResult
    {
        public bool IsSuccess { get; set; }
        public List<string> AppExceptions { get; set; }
        public List<string> WebExceptions { get; set; }
        public long RaportingPeriod { get; set; }
        public Dictionary<string, string> BrowsersExePaths { get; set; }
    }
}
