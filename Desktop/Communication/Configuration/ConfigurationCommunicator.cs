﻿using Communication.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Configuration
{
    public class ConfigurationCommunicator : AbstractCommunicator
    {
        public ConfigurationCommunicator(ServiceConfiguration configuration) : base(configuration)
        {
        }

        public ConfigurationResult GetConfiguration(string login, string appKey)
        {
            var request = new Configuration.Request.ConfigurationRequest(login, appKey);
            var response = HTMLPostUtil.Post<Configuration.Response.ConfigurationResponse>(this.Configuration.GetHttpUrl(), request);
            return new ConfigurationResult()
            {
                IsSuccess = response.success,
                BrowsersExePaths = response.browsersExePaths,
                WebExceptions = response.webExceptions,
                AppExceptions = response.appExceptions,
                RaportingPeriod = response.raportingPeriod
            };
        }
    }
}
