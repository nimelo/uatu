﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Resolving.Request
{
    public class ResolveRequest
    {      
        public string secretString { get; set; }
        public Query query { get; set; }
        public ResolveRequest(string secretKey, string serviceName)
        {
            this.secretString = secretKey;
            this.query = new Query()
            {
                serviceName = serviceName
            };
        }
    }

    public class Query
    {
        public string serviceName { get; set; }
    }
}
