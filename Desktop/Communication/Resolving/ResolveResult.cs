﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Resolving
{
    public class ResolveResult
    {
        public bool IsSuccess{get;set;}
        public Dictionary<string, ServiceConfiguration> Services { get; set; }

        public ResolveResult()
        {
            this.Services = new Dictionary<string, ServiceConfiguration>();
            this.IsSuccess = false;
        }
    }
}
