﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Resolving.Response
{
    public class ResolveResponse
    {
        public bool isSuccess { get; set; }
        public string ip { get; set; }
        public string port { get; set; }
        public string path { get; set; }
    }
}
