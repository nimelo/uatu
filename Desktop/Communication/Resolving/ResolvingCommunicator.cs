﻿using Communication.Resolving.Request;
using Communication.Resolving.Response;
using Communication.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Resolving
{
    public class ResolvingCommunicator : AbstractCommunicator
    {
        public static string LoginServiceName = "login";
        public static string ConfigurationServiceName = "configuration";
        public static string RaportServiceName = "raport";

        public ResolvingCommunicator(ServiceConfiguration configuration) : base(configuration)
        {
        }

        public ServiceConfiguration GetLoginConfiguration()
        {
            return this.GetConfigurationByServiceName(LoginServiceName);
        }

        public ServiceConfiguration GetConfigurationConfiguration()
        {
            return this.GetConfigurationByServiceName(ConfigurationServiceName);
        }

        public ServiceConfiguration GetRaportConfiguration()
        {
            return this.GetConfigurationByServiceName(RaportServiceName);
        }

        private ServiceConfiguration GetConfigurationByServiceName(string serviceName)
        {
            var objResponse = HTMLPostUtil.Post<ResolveResponse>(this.Configuration.GetHttpUrl(), new ResolveRequest(Variables.SecretKey, serviceName));
            
            return new ServiceConfiguration()
            {
                Address = objResponse.ip,
                Port = objResponse.port,
                Path = objResponse.path
            };
        }
    }
}
