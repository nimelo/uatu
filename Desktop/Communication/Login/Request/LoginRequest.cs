﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Login.Request
{
    public class LoginRequest
    {
        public string login { get; set; }
        public string appKey { get; set; }
        public string password { get; set; }

        public LoginRequest(string login, string appKey, string password)
        {
            this.login = login;
            this.appKey = appKey;
            this.password = password;
        }
    }
}
