﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Login
{
    public class LoginResult
    {
        public bool IsSuccess { get; set; }
        public string Login { get; set; }
        public string AppPassword { get; set; }

        public static LoginResult FailResult
        {
            get
            {
                return new LoginResult()
                {
                    IsSuccess = false
                };
            }
        }
        
        public static LoginResult FakeSuccess(string login, string password)
        {
            return new LoginResult()
            {
                IsSuccess = true,
                Login = login,
                AppPassword = password
            };
        }
    }
}
