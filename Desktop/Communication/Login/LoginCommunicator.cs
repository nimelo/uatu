﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication.Resolving;
using Communication.Utils;

namespace Communication.Login
{
    public class LoginCommunicator : AbstractCommunicator
    {
        public LoginCommunicator(ServiceConfiguration configuration) : base(configuration)
        {
        }

        public LoginResult LoginCredentials(string login, string password)
        {
            var response = HTMLPostUtil.Post<Login.Response.LoginResponse>(this.Configuration.GetHttpUrl(), new Login.Request.LoginRequest(login, null, password));
            return new LoginResult()
            {
                AppPassword = response.appKey,
                Login = login,
                IsSuccess = response.success
            };
        }

        public LoginResult LoginByAppKey(string login, string AppPassword)
        {
            var response = HTMLPostUtil.Post<Login.Response.LoginResponse>(this.Configuration.GetHttpUrl(), new Login.Request.LoginRequest(login, AppPassword, null));
            return new LoginResult()
            {
                AppPassword = response.appKey,
                Login = login,
                IsSuccess = response.success
            };
        }
    }
}
