﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Login.Response
{
    public class LoginResponse
    {
        public bool success { get; set; }
        public string login { get; set; }
        public string appKey { get; set; }

        public LoginResponse(string login, string password, bool isSuccess)
        {
            this.login = login;
            this.appKey = password;
            this.success = isSuccess;
        }

        public LoginResponse()
        {

        }
    }
}
