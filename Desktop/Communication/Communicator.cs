﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication
{
    public class Communicator
    {
        public Configuration.ConfigurationCommunicator Configuration { get; protected set; }
        public Login.LoginCommunicator Login { get; protected set; }
        public Resolving.ResolvingCommunicator Resolve { get; protected set; }
        public Raporting.RaportingCommunicator Raport { get; protected set; }

        public Communicator(ServiceConfiguration reslveConfig)
        {
            this.Resolve = new Resolving.ResolvingCommunicator(reslveConfig);
            ServiceConfiguration lConf = this.Resolve.GetLoginConfiguration();
            ServiceConfiguration cConf = this.Resolve.GetConfigurationConfiguration();
            ServiceConfiguration rCong = this.Resolve.GetRaportConfiguration();

            this.Raport = new Raporting.RaportingCommunicator(rCong);
            this.Configuration = new Configuration.ConfigurationCommunicator(cConf);
            this.Login = new Login.LoginCommunicator(lConf);
        }
    }
}
