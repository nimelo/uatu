﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Raporting.Request
{
    public class Credentials
    {
        public string login { get; set; }
        public string appKey { get; set; }

        public Credentials(string login, string appKey)
        {
            this.login = login;
            this.appKey = appKey;
        }
    }
}
