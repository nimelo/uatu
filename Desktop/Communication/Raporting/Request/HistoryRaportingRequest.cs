﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Raporting.Request
{
    public class HistoryRaportingRequest
    {
        public Credentials credentials { get; set; }
        public object histories { get; set; }

        public HistoryRaportingRequest(Credentials credentials, object histories)
        {
            this.credentials = credentials;
            this.histories = histories;
        }
    }
}
