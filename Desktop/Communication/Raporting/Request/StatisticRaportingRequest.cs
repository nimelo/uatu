﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Raporting.Request
{
    public class StatisticRaportingRequest
    {
        public Credentials credentials { get; set; }
        public object statistics { get; set; }

        public StatisticRaportingRequest(Credentials credentials, object statistics)
        {
            this.credentials = credentials;
            this.statistics = statistics;
        }
    }
}
