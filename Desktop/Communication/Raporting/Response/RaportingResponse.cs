﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Raporting.Response
{
    public class RaportingResponse
    {
        public bool success { get; set; }
        public string errorMessage { get; set; }
    }
}
