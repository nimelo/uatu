﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication.Resolving;
using Communication.Utils;

namespace Communication.Raporting
{
    public class RaportingCommunicator : AbstractCommunicator
    {
        public RaportingCommunicator(ServiceConfiguration configuration) : base(configuration)
        {
        }

        public RaportingResult RaportStatistics(string login, string appKey, object statistics)
        {
            var request = new Raporting.Request.StatisticRaportingRequest(new Request.Credentials(login, appKey), statistics);
            var response = HTMLPostUtil.Post<Raporting.Response.RaportingResponse>(this.Configuration.GetHttpUrl() + "/statistics", request);
            return new RaportingResult()
            {
                ErrorMessage = response.errorMessage,
                IsSuccess = response.success
            };
        }

        public RaportingResult RaportHistory(string login, string appKey, object histories)
        {
            var request = new Raporting.Request.HistoryRaportingRequest(new Request.Credentials(login, appKey), histories);
            var response = HTMLPostUtil.Post<Raporting.Response.RaportingResponse>(this.Configuration.GetHttpUrl() + "/histories", request);
            return new RaportingResult()
            {
                ErrorMessage = response.errorMessage,
                IsSuccess = response.success
            };
        }
    }
}
