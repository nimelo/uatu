﻿namespace Communication.Raporting
{
    public class RaportingResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public static RaportingResult FailResult
        {
            get
            {
                return new RaportingResult()
                {
                    IsSuccess = false
                };
            }
        }

        public static RaportingResult FakeSuccess
        {
            get
            {
                return new RaportingResult()
                {
                    IsSuccess = true
                };
            }
        }
    }
}