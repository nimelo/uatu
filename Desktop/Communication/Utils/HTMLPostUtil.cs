﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Communication.Utils
{
    public static class HTMLPostUtil
    {
        public static T Post<T>(string url, object request)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = @"application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(JSONUtils.ToJSON(request));
                streamWriter.Flush();
            }

            var response = httpWebRequest.GetResponse();

            string responseStr = "";
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                responseStr = sr.ReadToEnd();
            }

            var objResponse = JSONUtils.FromJSON<T>(responseStr);

            return objResponse;
        }
    }
}
