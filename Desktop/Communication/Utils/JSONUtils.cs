﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Communication.Utils
{
    public static class JSONUtils
    {
        public static string ToJSON(object obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }

        public static T FromJSON<T>(string json)
        {
            return new JavaScriptSerializer().Deserialize<T>(json);
        }
    }
}
