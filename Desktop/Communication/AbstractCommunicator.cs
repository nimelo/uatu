﻿using Communication.Resolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication
{
    public abstract class AbstractCommunicator
    {
        public ServiceConfiguration Configuration { get; set; }

        public AbstractCommunicator(ServiceConfiguration configuration)
        {
            this.Configuration = configuration;
        }
    }
}
