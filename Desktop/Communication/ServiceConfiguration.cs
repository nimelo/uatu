﻿namespace Communication
{
    public class ServiceConfiguration
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Port { get; set; }
        public string Path { get; set; }

        public string GetHttpUrl()
        {
            return $@"http://{Address}:{Port}/{Path}";
        }

        public static ServiceConfiguration DefaultResolveConfiguration
        {
            get
            {
                return new ServiceConfiguration()
                {
                    Address = "127.0.0.1",
                    Port = "9000",
                    Path = "resolve"
                };
            }
        }
    }
}