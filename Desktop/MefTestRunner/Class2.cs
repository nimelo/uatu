﻿using Common.Observer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace MefTestRunner
{
    class Program
    {
        static void Main(string[] args)
        {
           // Construct and initialize settings for a second AppDomain.
            AppDomainSetup ads = new AppDomainSetup();
            ads.ApplicationBase = @"E:\BitBucket\UATU\Desktop\MefTestRunner\bin\x64\Debug\Plugins\C\ApplicationObserver.dll";
            ads.DisallowBindingRedirects = false;
            ads.DisallowCodeDownload = true;
            ads.ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            // create the second AppDomain.
            AppDomain ad2 = AppDomain.CreateDomain("AD #2", null, ads);
            Proxy marts = (Proxy)ad2.CreateInstanceAndUnwrap(typeof(Proxy).Assembly.FullName, typeof(Proxy).FullName);
            // Call a method on the object via the proxy, passing the // default AppDomain's friendly name in as a parameter.
            marts.GetAssembly(@"E:\BitBucket\UATU\Desktop\MefTestRunner\bin\x64\Debug\Plugins\C\ApplicationObserver.dll");
            // unload the second AppDomain. This deletes its object and // invalidates the proxy object.
            AppDomain.Unload(ad2);
        }
    }

    public class Proxy : MarshalByRefObject
    {
        public Assembly GetAssembly(string assemblyPath)
        {
            try
            {
                return Assembly.LoadFile(assemblyPath);
            }
            catch (Exception e)
            {
                return null;
                // throw new InvalidOperationException(ex);
            }
        }
    }
}
