﻿using Common.Observer;
using Common.Observer.NonPeriodical;
using Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MefTestRunner
{
    public class Class1
    {
        static void Main(string[] args)
        {
            var observersPath = Directory.GetDirectories(System.Environment.CurrentDirectory);
            var shadowPath = Path.Combine(System.Environment.CurrentDirectory, "Shadow");

            Directory.CreateDirectory(shadowPath);

            var setup = new AppDomainSetup()
            {
                CachePath = shadowPath,
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = Path.Combine(System.Environment.CurrentDirectory, "Plugins")
            };

            AppDomain domain = AppDomain.CreateDomain("Host_AppDomain", AppDomain.CurrentDomain.Evidence, setup);

            var runner = domain.CreateInstanceAndUnwrap(typeof(ObserverImporter).Assembly.FullName, typeof(ObserverImporter).FullName) as ObserverImporter;

            foreach (var item in Directory.EnumerateDirectories(Path.Combine(System.Environment.CurrentDirectory, "Plugins")))
            {
                runner.AddObserver(item);
            }
            //runner.AddObserver(Path.Combine(System.Environment.CurrentDirectory, "Plugins"));

            runner.Observers.ToList().Select(y => y.Value.Observer).ToList().ForEach(x =>
            {
                x.ErrorOccured += Plugin_ErrorOccured;
                x.QueryOccured += Plugin_QueryOccured;
                x.Start();               
            });

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
                Console.WriteLine(runner.Observers.Count);
                
            }

        }

        private static void Plugin_QueryOccured(object sender, Common.Observer.AbstractQueryResult e)
        {
            Console.WriteLine($"{(sender as AbstractObserver).Name}: {e.ToString()}");
        }

        private static void Plugin_ErrorOccured(object sender, Common.Observer.ObservationError e)
        {
            Console.WriteLine($"{(sender as AbstractObserver).Name + "\n"}{e.Exception} ->{e.Exception.Message}");
        }
    }
}
