﻿using Common;
using Common.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Plugins.TemperaturePlugin
{
    public class TemperaturePlugin : AbstractPlugin
    {
        #region IPlugin
        public override string GetName()
        {
            return nameof(TemperaturePlugin);
        }

        public override uint GetTimePeriod()
        {
            return 1000;
        }

        public override void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            
        }

        protected override void Handle(ElapsedEventArgs e)
        {
            
        }
        #endregion
    }
}
