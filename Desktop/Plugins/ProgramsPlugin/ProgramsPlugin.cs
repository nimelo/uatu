﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Common.EventArgs;
using Common.ProgramPlugin;

namespace Plugins.ProgramsPlugin
{
    public class ProgramsPlugin : AbstractProgramPlugin
    {
        #region Public 
        public override string GetName()
        {
            return nameof(ProgramsPlugin);
        }

        public override uint GetTimePeriod()
        {
            return 1000;
        }
        #endregion

        #region Protected Methdods
        protected override void Handle(ElapsedEventArgs e)
        {
            IntPtr handle = this.GetForegroundWindowHandle();
            uint pId = this.GetWindowThreadProcessId(handle);

            if (this.CurrentHandle.IsEmpty())
            {
                this.CurrentHandle.SetValue(pId, ProgramsPluginAdditionalDataExtractor.Extract(handle));
            }
            else
            {
                if (!this.CurrentHandle.Equals(pId))
                {
                    var lastSwap = this.CurrentHandle.SwapValue(pId, ProgramsPluginAdditionalDataExtractor.Extract(handle));
#if (DEBUG)
                    Console.WriteLine($"Was:{lastSwap.AdditionalData} from:{lastSwap.From} to:{lastSwap.To}");
#endif
                }
            }
        }


        public override void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (this.CanHandle)
            {
                base.OnTimedEvent(source, e);
            }
        }

        public override void OnMachineStateEvent(object source, DetectorEventArgs e)
        {
            if (e.State == MachineStateEnum.IDLE)
            {
                this.CanHandle = false;
                var lastSwap = this.CurrentHandle.ClearValue();
#if (DEBUG)
                Console.WriteLine($"Was:{lastSwap.AdditionalData} from:{lastSwap.From} to:{lastSwap.To}");
#endif
            }
            else if (e.State == MachineStateEnum.RESUME)
            {
                this.CanHandle = true;
            }
        }
        #endregion

        #region Properties
        protected ObjectWithSetMemory<uint, ProgramsPluginAdditionalData> CurrentHandle { get; set; }
        protected bool CanHandle { get; set; }
        #endregion

        #region Fields
        private uint[] systemPids = { 0, 4, 8 };
        #endregion

        #region Ctors
        public ProgramsPlugin()
        {
            this.CurrentHandle = new ObjectWithSetMemory<uint, ProgramsPluginAdditionalData>();
            this.CanHandle = true;
        }
        #endregion
    }
}
