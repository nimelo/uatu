﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.ProgramsPlugin
{
    public static class ForegroundWindowHelper
    {
        #region DLL imports
        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint processId);

        [DllImport("user32.dll")]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        #endregion

        #region Static Methods
        public static IntPtr GetForegroundWindowHandle()
        {
            return GetForegroundWindow();
        }

        public static uint GetWindowThreadProcessId(IntPtr windowHandle)
        {
            uint processId;

            GetWindowThreadProcessId(windowHandle, out processId);

            return processId;
        }
        #endregion
    }
}
