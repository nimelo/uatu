﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.ProgramsPlugin
{
    public static class ProgramsPluginAdditionalDataExtractor
    {
        public static ProgramsPluginAdditionalData Extract(uint pid)
        {
            var process = Process.GetProcessById((int)pid);

            return new ProgramsPluginAdditionalData()
            {
                ExecutionPath = process.Modules[0].FileName
            };
        }

        public static ProgramsPluginAdditionalData Extract(IntPtr handle)
        {
            return Extract(ForegroundWindowHelper.GetWindowThreadProcessId(handle));
        }
    }
}
