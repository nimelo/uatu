﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.ProgramsPlugin
{
    public class ObjectWithSetMemory<T, R>
    {
        #region Fields
        private R _additionalData;
        private T _value;
        private bool _hasValue;
        private DateTime _setDateTime;
        #endregion

        #region Properties
        public T Value
        {
            get { return _value; }
            protected set
            {
                this._hasValue = true;
                this._setDateTime = DateTime.Now;
                this._value = value;
            }
        }

        public R AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value; }
        }

        #endregion

        #region Ctors
        public ObjectWithSetMemory()
        {
            this._hasValue = false;
        }
        #endregion

        #region Public methods
        public bool IsEmpty()
        {
            return !this._hasValue;
        }

        public bool Equals(T value)
        {
            return this._value.Equals(value);
        }

        public void SetValue(T value, R AdditionalData)
        {
            this.Value = value;
            this.AdditionalData = AdditionalData;
        }

        public ObjectWithMemorySwapHistory<T, R> SwapValue(T value, R additionalData)
        {
            var currentTime = DateTime.Now;
            var obj = new ObjectWithMemorySwapHistory<T, R>() {
                Value = this.Value,
                AdditionalData = this.AdditionalData,
                From = this._setDateTime,
                To = currentTime
            };

            this.Value = value;
            this.AdditionalData = additionalData;

            return obj;
        }

        public ObjectWithMemorySwapHistory<T, R> ClearValue()
        {
            var currentTime = DateTime.Now;
            var obj = new ObjectWithMemorySwapHistory<T, R>()
            {
                Value = this.Value,
                AdditionalData = this.AdditionalData,
                From = this._setDateTime,
                To = currentTime
            };

            this._hasValue = false;
            return obj;
        }

        #endregion
    }

    public class ObjectWithMemorySwapHistory<T, R>
    {
        public T Value { get; set; }
        public R AdditionalData { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
