﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Plugins
{
    public class TestPlugin : AbstractPlugin
    {
        DateTime? first = null;

        public override string GetName()
        {
            return nameof(TestPlugin);
        }

        public override uint GetTimePeriod()
        {
            return 10000;
        }

        protected override void Handle(ElapsedEventArgs e)
        {
            if (this.first.HasValue)
            {
            //    Console.WriteLine($"{this.GetName()} Elapsed: {e.SignalTime - this.first}");
            }
            else
            {
                this.first = DateTime.Now;
            }
        }
    }
}
