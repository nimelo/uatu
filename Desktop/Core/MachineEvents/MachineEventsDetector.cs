﻿using Common.EventArgs;
using Core.MachineEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Core
{
    public class MachineEventsDetector : AbstractDetector
    {
        #region Properties
        public IEnumerable<AbstractDetector> Detectors { get; set; }
        #endregion

        #region Ctors
        public MachineEventsDetector()
        {
            this.Detectors = this.CreateDetecors(null);
            foreach (var detector in this.Detectors)
            {
                detector.EventOccured += DetectorEventOccured;
            }
        }
        public MachineEventsDetector(DetectorsConfiguration conf)
        {
            this.Detectors = this.CreateDetecors(conf);
            foreach (var detector in this.Detectors)
            {
                detector.EventOccured += DetectorEventOccured;
            }
        }
        #endregion

        #region Public methods
        public override void StartDetecting()
        {
            foreach (var detector in this.Detectors)
            {
                detector.StartDetecting();
            }
        }

        public override void StopDetecting()
        {
            foreach (var detector in this.Detectors)
            {
                detector.StopDetecting();
            }
        }
        #endregion

        #region Private methods
        private IEnumerable<AbstractDetector> CreateDetecors(DetectorsConfiguration conf)
        {
            var configuration = conf ?? DetectorsConfiguration.Default;

            var idleDetector = new IdleDetector(configuration.IdleDetectorTimerPeriod, configuration.IdleDetectorIdlePeriod);
            var powerDetector = new PowerModeDetector();
            var sessionDetector = new SessionDetector();

            return new List<AbstractDetector>()
            {
                idleDetector, powerDetector, sessionDetector
            };
        }

        private void DetectorEventOccured(object sender, DetectorEventArgs e)
        {
            this.OnEventOccured(e);
#if (DEBUG)
            Console.WriteLine(e.State);
#endif
        }
        #endregion
    }
}
