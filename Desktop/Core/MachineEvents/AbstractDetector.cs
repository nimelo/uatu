﻿using Common.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.MachineEvents
{
    public abstract class AbstractDetector : MarshalByRefObject
    {
        public event EventHandler<DetectorEventArgs> EventOccured;

        protected virtual void OnEventOccured(DetectorEventArgs e)
        {
            EventOccured?.Invoke(this, e);
        }

        public abstract void StartDetecting();

        public abstract void StopDetecting();
    }
}
