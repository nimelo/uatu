﻿using Common.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.MachineEvents
{
    public class SessionDetector : AbstractDetector
    {
        #region Public methods
        public override void StartDetecting()
        {
            Microsoft.Win32.SystemEvents.SessionEnded += SessionEnded;
        }

        public override void StopDetecting()
        {
            Microsoft.Win32.SystemEvents.SessionEnded -= SessionEnded;
        }
        #endregion

        #region Private methods
        private void SessionEnded(object sender, Microsoft.Win32.SessionEndedEventArgs e)
        {
            switch (e.Reason)
            {
                case Microsoft.Win32.SessionEndReasons.Logoff:
                    this.OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.SESSION_END_LOGOFF));
                    break;
                case Microsoft.Win32.SessionEndReasons.SystemShutdown:
                    this.OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.SESSION_END_SHUTDOWN));
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
