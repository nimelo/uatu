﻿using Common.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Core.MachineEvents
{
    public class IdleDetector : AbstractDetector
    {
        #region DLL imports
        [DllImport("user32.dll")]
        static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [StructLayout(LayoutKind.Sequential)]
        struct LASTINPUTINFO
        {
            public static readonly int SizeOf = Marshal.SizeOf(typeof(LASTINPUTINFO));

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 dwTime;
        }
        #endregion

        #region Ctors
        public IdleDetector(uint timerPeriod, uint idlePeriod)
        {
            this._timePeriod = idlePeriod;
            this._timer = new Timer(timerPeriod);
            this._timer.Elapsed += OnTimerElapsed;
            this._isIdle = false;
        }
        #endregion

        #region Fields
        private uint _timePeriod;
        Timer _timer;
        private bool _isIdle;
        #endregion

        #region Public methods
        public override void StartDetecting()
        {
            this._timer.Start();
        }

        public override void StopDetecting()
        {
            this._timer.Stop();
        }
        #endregion

        #region Protected methods
        protected virtual void OnTimerElapsed(object source, ElapsedEventArgs e)
        {
            if (this._isIdle)
            {
                if (!this.IsIdle())
                {
                    OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.RESUME));
                    this._isIdle = false;
                }
            }
            else
            {
                if (this.IsIdle())
                {
                    OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.IDLE));
                    this._isIdle = true;
                }
            }
            
        }
        #endregion

        #region Private methods
        private bool IsIdle()
        {
            return this.GetIdleTime() > this._timePeriod;
        }

        private uint GetIdleTime()
        {
            LASTINPUTINFO lastInPut = new LASTINPUTINFO();
            lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
            GetLastInputInfo(ref lastInPut);

            return ((uint)Environment.TickCount - lastInPut.dwTime);
        }

        private long GetEnvTickCount()
        {
            return Environment.TickCount;
        }

        private int GetLastInputTime()
        {
            int idleTime = 0;
            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = (uint)Marshal.SizeOf(lastInputInfo);
            lastInputInfo.dwTime = 0;

            int envTicks = Environment.TickCount;

            if (GetLastInputInfo(ref lastInputInfo))
            {
                int lastInputTick = (int)lastInputInfo.dwTime;

                idleTime = envTicks - lastInputTick;
            }

            return ((idleTime > 0) ? (idleTime / 1000) : 0);
        }
        #endregion
    }
}
