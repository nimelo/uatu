﻿namespace Core
{
    public class DetectorsConfiguration
    {
        public uint IdleDetectorIdlePeriod { get; set; }
        public uint IdleDetectorTimerPeriod { get; set; }

        public static DetectorsConfiguration Default
        {
            get
            {
                return new DetectorsConfiguration()
                {
                    IdleDetectorIdlePeriod = 10 * 60 * 1000,
                    IdleDetectorTimerPeriod = 1000
                };
            }
        }
    }
}