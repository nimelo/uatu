﻿using Common.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.MachineEvents
{
    public class PowerModeDetector : AbstractDetector
    {
        #region Public methods
        public override void StartDetecting()
        {
            Microsoft.Win32.SystemEvents.PowerModeChanged += PowerModeChanged;
        }

        public override void StopDetecting()
        {
            Microsoft.Win32.SystemEvents.PowerModeChanged -= PowerModeChanged;
        }
        #endregion

        #region Private methods
        private void PowerModeChanged(object sender, Microsoft.Win32.PowerModeChangedEventArgs e)
        {
            switch (e.Mode)
            {
                case Microsoft.Win32.PowerModes.Resume:
                    this.OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.RESUME));
                    break;
                case Microsoft.Win32.PowerModes.StatusChange:
                    this.OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.STATUS_CHANGE));
                    break;
                case Microsoft.Win32.PowerModes.Suspend:
                    this.OnEventOccured(new DetectorEventArgs(Common.MachineStateEnum.SUSPEND));
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
