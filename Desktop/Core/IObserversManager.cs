﻿using Common.Observer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IObserversManager
    {
        void Start();
        void Stop();
        IDictionary<string, AbstractObserver> GetObservers();
        void Reconfigure(Dictionary<string, string> browserPaths, List<string> appExceptions, List<string> webExceptions);

        AbstractObserver AddObserver(string path);
        void RemoveObserver(string path);

        bool Start(string path);
        bool Stop(string path);

        event EventHandler<AbstractQueryResult> ObserverQueryOccured;
        event EventHandler<ObservationError> ObserverErrorOccured;
    }
}
