﻿using Common.Observer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    class Program
    {
        public static string path = @"E:\BitBucket\UATU\Desktop\CPUTemperatureObserver\bin\Debug\CPUTemperatureObserver.dll";

        static void Main(string[] args)
        {
           
        }
    }

    public class Proxy : MarshalByRefObject
    {
        public Assembly GetAssembly(string assemblyPath)
        {
            try
            {
                return Assembly.LoadFile(assemblyPath);
            }
            catch (Exception e)
            {
                return null;
                // throw new InvalidOperationException(ex);
            }
        }
    }

}
