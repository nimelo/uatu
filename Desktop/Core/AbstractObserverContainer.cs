﻿using System;
using System.ComponentModel.Composition.Hosting;
using Common.Observer;

namespace Core
{
    public class AbstractObserverContainer : MarshalByRefObject
    {
        public CompositionContainer CompositionContainer { get; protected set; }
        public AbstractObserver Observer { get; protected set; }

        public AbstractObserverContainer(AbstractObserver observer, CompositionContainer compositionContainer)
        {
            this.Observer = observer;
            this.CompositionContainer = compositionContainer;
        }
    }
}