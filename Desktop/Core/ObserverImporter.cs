﻿using Common.Observer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.ComponentModel.Composition.Registration;
using Core.MachineEvents;
using Common.Observer.BrowserObserver;
using Common.Observer.ApplicationObserver;

namespace Core
{
    public class ObserverImporter : MarshalByRefObject, IObserversManager
    {
        [ImportMany(AllowRecomposition = true)]
        #region Propeties
        public IDictionary<string, AbstractObserverContainer> Observers { get; set; }
        public AggregateCatalog Catalog { get; protected set; }
        public CompositionContainer Container { get; protected set; }
        public AbstractDetector Detector { get; set; }
        public Dictionary<string, string> BrowsersPaths { get; private set; }
        public List<string> AppExceptions { get; private set; }
        public List<string> WebExceptions { get; private set; }
        #endregion

        #region Ctors 
        public ObserverImporter()
        {
            this.Observers = new Dictionary<string, AbstractObserverContainer>();
            this.Catalog = new AggregateCatalog();
            this.Container = new CompositionContainer(this.Catalog);
            this.AppExceptions = new List<string>();
            this.WebExceptions = new List<string>();
            this.BrowsersPaths = new Dictionary<string, string>();
        }
        #endregion

        #region Events
        public event EventHandler<AbstractQueryResult> ObserverQueryOccured;
        public event EventHandler<ObservationError> ObserverErrorOccured;
        #endregion

        #region On events
        public void OnObserverQueryOccured(object ob, AbstractQueryResult e)
        {
            this.ObserverQueryOccured?.Invoke(ob, e);
        }

        public void OnObserverErrorOccured(object ob, ObservationError e)
        {
            this.ObserverErrorOccured?.Invoke(ob, e);
        }
        #endregion

        #region Public methods        
        public void Recompose()
        {
            foreach (var container in this.Observers.Values)
            {
                foreach (var catalog in (container.CompositionContainer.Catalog as AggregateCatalog)?.Catalogs)
                {
                    (catalog as DirectoryCatalog)?.Refresh();
                }
            }

            throw new NotImplementedException();
        }

        public void Start()
        {
            this.Detector?.StartDetecting();
            foreach (var container in this.Observers.Values)
            {
                container.Observer.Start();
            }
        }

        public void Stop()
        {
            this.Detector?.StopDetecting();
            foreach (var container in this.Observers.Values)
            {
                container.Observer.Stop();
            }
        }

        public AbstractObserver AddObserver(string path)
        {
            try
            {
                var regBuilder = new RegistrationBuilder();
                regBuilder.ForTypesDerivedFrom<AbstractObserver>().Export<AbstractObserver>();

                var aggregateCatalog = new AggregateCatalog();
                var directory = new DirectoryCatalog(path, regBuilder);

                aggregateCatalog.Catalogs.Add(directory);
                var compositionContainer = new CompositionContainer(aggregateCatalog);

                var observer = compositionContainer.GetExport<AbstractObserver>();
                this.ConfigureObserver(observer.Value);

                this.Observers.Add(path, new AbstractObserverContainer(observer.Value, compositionContainer));
                this.WireUpObserver(observer.Value);           
                return observer.Value;
            }
            catch (Exception e)
            {

                return null;
            }
        }

        private void ConfigureObserver(AbstractObserver observer)
        {
            if (observer is AbstractBrowserObserver)
            {
                this.ConfigureBrowserObserver(observer as AbstractBrowserObserver);
            }
            else if (observer is AbstractApplicationObserver)
            {
                this.ConfigureApplicationObserver(observer as AbstractApplicationObserver);
            }
        }

        private void WireUpObserver(AbstractObserver observer)
        {

          /*  if (this.Detector != null)
                this.Detector.EventOccured += observer.OnMachineStateEvent;*/

            observer.QueryOccured += this.OnObserverQueryOccured;
            observer.ErrorOccured += this.OnObserverErrorOccured;
        }

        public void RemoveObserver(string path)
        {
            if (this.Observers.Keys.Contains(path))
            {
                this.Observers[path].Observer.Stop();
                this.Observers.Remove(path);
            }
        }

        public bool Start(string path)
        {
            if (this.Observers.Keys.Contains(path))
            {
                this.Observers[path].Observer.Start();
                return true;
            }
            return false;
        }

        public bool Stop(string path)
        {
            if (this.Observers.Keys.Contains(path))
            {
                this.Observers[path].Observer.Stop();
                return true;
            }
            return false;
        }

        public IDictionary<string, AbstractObserver> GetObservers()
        {
            return this.Observers.ToDictionary(x => x.Key, x => x.Value.Observer);
        }

        public void Reconfigure(Dictionary<string, string> browserPaths, List<string> appExceptions, List<string> webExceptions)
        {
            this.BrowsersPaths = browserPaths;
            this.AppExceptions = appExceptions;
            this.WebExceptions = webExceptions;

            foreach (var observer in this.Observers.Values)
            {
                if(observer.Observer is AbstractBrowserObserver)
                {
                    this.ConfigureBrowserObserver(observer.Observer as AbstractBrowserObserver);
                }
                else if(observer.Observer is AbstractApplicationObserver)
                {
                    this.ConfigureApplicationObserver(observer.Observer as AbstractApplicationObserver);
                }
            }
        }

        private void ConfigureApplicationObserver(AbstractApplicationObserver abstractApplicationObserver)
        {
            abstractApplicationObserver.IdleExceptions = this.AppExceptions;
        }

        private void ConfigureBrowserObserver(AbstractBrowserObserver abstractBrowserObserver)
        {
            abstractBrowserObserver.IdleExeceptionUrls = this.WebExceptions;
            var bName = abstractBrowserObserver.BrowserName;

            if (this.BrowsersPaths.Keys.Contains(bName))
            {
                abstractBrowserObserver.ExecutablePaths = new List<string>() { this.BrowsersPaths[bName] };
            }
        }
        #endregion
    }
}
